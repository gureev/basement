// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_EXCEPTIONS_HPP__
#define __AMPLE_SERIALIZATOR_EXCEPTIONS_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/serializator/api/sr_exceptions_visitor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

EX_DECLARE_BASE_EXCEPTION(
		BaseException
	,	ExceptionsVisitor
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_0_ARGUMENT(
		BaseException
	,	SaveFailure
)

EX_DECLARE_0_ARGUMENT(
		BaseException
	,	RestoreFailure
)

EX_DECLARE_0_ARGUMENT(
		BaseException
	,	VersionMismatch
)

EX_DECLARE_0_ARGUMENT(
		BaseException
	,	RootElementMismatch
)

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_EXCEPTIONS_HPP__
