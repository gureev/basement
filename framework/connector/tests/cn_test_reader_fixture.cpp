// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/sources/cn_plugins_info_restorer.hpp"

#include "framework/connector/tests/cn_test_reader_fixture.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/tools/common_headers/numeric_type_info.hpp"

#include <QtCore/qbuffer.h>

#include <ctime>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Tests {

/*---------------------------------------------------------------------------*/

RestorerFixture::RestorerFixture()
{
} // RestorerFixture::RestorerFixture

/*---------------------------------------------------------------------------*/

PluginIdentifier
RestorerFixture::generatePluginIdentifier() const
{
	return PluginIdentifier(
			std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
		,	std::rand()
	);

} // RestorerFixture::generatePluginIdentifier

/*---------------------------------------------------------------------------*/

const Implementation::PluginsContainer &
RestorerFixture::runTest()
{
	m_plugins.reset( new Implementation::PluginsContainer() );

	QByteArray data = m_body.toAscii();

	QBuffer buffer( & data );
	buffer.open( QIODevice::ReadOnly );

	Implementation::PluginsInfoRestorer restorer(
			* m_plugins
		,	buffer
	);
	restorer.run();

	return * m_plugins;

} // RestorerFixture::runTest

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Connector
} // namespace Ample
