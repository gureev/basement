// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/api/cn_plugin_identifier.hpp"
#include "framework/connector/api/cn_exceptions.hpp"

#include "framework/connector/sources/cn_plugins_info_restorer.hpp"
#include "framework/connector/sources/cn_plugins_container_impl.hpp"

#include "framework/connector/sources/resources/cn_resources.hpp"

#include "framework/connector/sources/utils/cn_utils.hpp"

#include "framework/tools/common_headers/version.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PluginsInfoRestorer::PluginsInfoRestorer(
		Implementation::PluginsContainer & _pluginsContainer
	,	QIODevice & _pluginsInfoFile
)
	:	m_pluginsContainer( _pluginsContainer )
	,	BaseRestorer( _pluginsInfoFile )
{
	resolvePluginsPath();

} // PluginsInfoRestorer::PluginsInfoRestorer

/*---------------------------------------------------------------------------*/

PluginsInfoRestorer::~PluginsInfoRestorer()
{
} // PluginsInfoRestorer::~PluginsInfoRestorer

/*---------------------------------------------------------------------------*/

void
PluginsInfoRestorer::resolvePluginsPath()
{
	const int pluginsPathMaxLength = 1024;
	char pluginsPath[ pluginsPathMaxLength ];

	int pluginsPathLength = Utils::getPluginsPath(
			pluginsPath
		,	pluginsPathMaxLength
	);

	if ( !pluginsPathLength )
		EX_THROW_0_ARGUMENT( Exceptions::CannotGetInformationAboutSystem )

	m_pluginsPath = pluginsPath;
	m_pluginsPath.replace( '\\', '/' );
	m_pluginsPath.replace(
			QRegExp( "/([^/\\\\:*?\\\"|<>]+).exe$" )
		,	"/%1.dll"
	);

} // PluginsInfoRestorer::resolvePluginsPath

/*---------------------------------------------------------------------------*/

PluginLoadPolicy::Enum
PluginsInfoRestorer::restoreLoadPolicy( const QDomElement & _tag )
{
	QString attribute = _tag.attribute( Resources::Xml::Attributes::PluginLazyLoad );

	if ( attribute.isEmpty() )
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_tag.lineNumber()
		);

	PluginLoadPolicy::Enum loadPolicy = PluginLoadPolicy::fromString( attribute );

	if ( loadPolicy == PluginLoadPolicy::Invalid )
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_tag.lineNumber()
		);

	return loadPolicy;

} // PluginsInfoRestorer::restoreLoadPolicy

/*---------------------------------------------------------------------------*/

const PluginIdentifier
PluginsInfoRestorer::restoreIdentifier( const QDomElement & _tag )
{
	QString attribute = _tag.attribute( Resources::Xml::Attributes::PluginIdentifier );

	if ( attribute.isEmpty() )
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_tag.lineNumber()
		)

	const PluginIdentifier identifier = PluginIdentifier::fromString( attribute );

	if ( ! identifier.isValid() )
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_tag.lineNumber()
		)

	return identifier;

} // PluginsInfoRestorer::restoreIdentifier

/*---------------------------------------------------------------------------*/

const QString
PluginsInfoRestorer::restoreName( const QDomElement & _tag )
{
	QString attribute = _tag.attribute( Resources::Xml::Attributes::PluginName );

	if ( attribute.isEmpty() )
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_tag.lineNumber()
		)

	return m_pluginsPath.arg( attribute );

} // PluginsInfoRestorer::restoreName

/*---------------------------------------------------------------------------*/

void
PluginsInfoRestorer::run()
{
	checkRootElement( Resources::Xml::Tags::Plugins );

	QDomElement pluginTag = m_rootElement.firstChildElement( Resources::Xml::Tags::Plugin );

	while ( !pluginTag.isNull() )
	{
		const QString name = restoreName( pluginTag );
		const PluginIdentifier identifier = restoreIdentifier( pluginTag );
		PluginLoadPolicy::Enum loadPolicy = restoreLoadPolicy( pluginTag );

		m_pluginsContainer.addPlugin(
				name
			,	identifier
			,	loadPolicy
		);

		pluginTag = pluginTag.nextSiblingElement( pluginTag.tagName() );
	}

} // PluginsInfoRestorer::run

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample
