// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/tests/con_test_fixture.hpp"

#include <boost/test/unit_test.hpp>

#include <QtCore/qcoreapplication.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		1) Messages

*/

/*---------------------------------------------------------------------------*/

//BOOST_FIXTURE_TEST_SUITE( Suite_Console, Fixture )

/*---------------------------------------------------------------------------*/
/*
BOOST_AUTO_TEST_CASE( Console_1_Messages )
{
	Accessor & accessor = getAccessor();

	while ( true )
	{
		accessor.print(
				MessageLevel::Success
			,	"TEST TEST TEST TEST TEST TEST TEST TEST TEST "
				"TEST TEST TEST TEST TEST TEST TEST TEST TEST "
				"TEST TEST TEST TEST TEST TEST TEST TEST TEST "
		);
	}

	int arc = 0;
	QCoreApplication app( arc, 0 );
	app.exec();

} // Console_1_Messages
*/
/*---------------------------------------------------------------------------*/

//BOOST_AUTO_TEST_SUITE_END() // Suite_Console

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Console
} // namespace Ample
