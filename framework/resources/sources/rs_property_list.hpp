// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_LIST_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_LIST_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_property.hpp"
#include "framework/resources/api/rs_access_type.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Property;

/*---------------------------------------------------------------------------*/

class PropertyList
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertyList();

	~PropertyList();

/*---------------------------------------------------------------------------*/

	void add( const std::string & _value );

/*---------------------------------------------------------------------------*/

	const Resources::Property & get( AccessType::Enum _accessType ) const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::Vector<
			boost::shared_ptr< Implementation::Property >
		>
		Properties;

	Properties m_properties;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_LIST_HPP__
