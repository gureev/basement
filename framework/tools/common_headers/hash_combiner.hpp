// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_HASH_COMBINER_HPP__
#define __AMPLE_COMMON_HEADERS_HASH_COMBINER_HPP__

/*----------------------------------------------------------------------------*/

#include <boost/functional/hash.hpp>

/*----------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {

/*----------------------------------------------------------------------------*/

class HashCombiner
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	typedef
		size_t
		HashResult;

/*----------------------------------------------------------------------------*/

	HashCombiner()
		:	m_result( 0 )
	{}

/*----------------------------------------------------------------------------*/

	HashCombiner & assign( const HashResult & _value );

	HashCombiner & operator += ( const HashResult & _value );

/*----------------------------------------------------------------------------*/

	HashResult getResult() const;

/*----------------------------------------------------------------------------*/

private:

/*----------------------------------------------------------------------------*/

	HashResult m_result;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

inline
HashCombiner &
HashCombiner::assign( const HashResult & _value )
{
	boost::hash_combine(
			m_result
		,	_value
	);

	return * this;
}

/*----------------------------------------------------------------------------*/

inline
HashCombiner &
HashCombiner::operator += ( const HashResult & _value )
{
	return assign( _value );
}

/*----------------------------------------------------------------------------*/

inline
HashCombiner::HashResult
HashCombiner::getResult() const
{
	return m_result;
}

/*----------------------------------------------------------------------------*/

} // namespace CommonHeaders
} // namespace Ample

/*----------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_HASH_COMBINER_HPP__
