// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_MAP_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_MAP_HPP__

/*---------------------------------------------------------------------------*/

#include "implementation/map.hpp"

#include <map>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {

/*---------------------------------------------------------------------------*/

template<
		typename _KeyType
	,	typename _ValueType
	,	typename _Comparator = std::less< _KeyType >
	,	typename _Allocator = std::allocator<
			std::pair<
					_KeyType
				,	_ValueType
			>
		>
>
class Map
	:	public Implementation::Map<
			std::map<
					_KeyType
				,	_ValueType
				,	_Comparator
				,	_Allocator
			>
		>
{

/*---------------------------------------------------------------------------*/

	typedef
		Implementation::Map<
			std::map<
					_KeyType
				,	_ValueType
				,	_Comparator
				,	_Allocator
			>
		>
		BaseContainer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Map()
		:	BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_MAP_HPP__
