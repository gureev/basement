// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_RESOURCES_HPP__
#define __AMPLE_CONSOLE_RESOURCES_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Constants {

/*---------------------------------------------------------------------------*/

namespace Console {

/*---------------------------------------------------------------------------*/

	extern const unsigned int TopLeftX;

	extern const unsigned int TopLeftY;

	extern const unsigned int Width;

	extern const unsigned int Height;

	extern const unsigned int LineLength;

	extern const unsigned int LinesCount;

/*---------------------------------------------------------------------------*/

} // namespace Console

/*---------------------------------------------------------------------------*/

namespace Command {

/*---------------------------------------------------------------------------*/

	extern const unsigned int MaxLength;

	extern const char * Prefix;

/*---------------------------------------------------------------------------*/

} // namespace Command

/*---------------------------------------------------------------------------*/

} // namespace Constants

/*---------------------------------------------------------------------------*/

namespace Strings {

/*---------------------------------------------------------------------------*/

	extern const char * ApplicationName;

/*---------------------------------------------------------------------------*/

} // namespace Strings

/*---------------------------------------------------------------------------*/

namespace Delays {

/*---------------------------------------------------------------------------*/

	extern const unsigned int ReadKey;

	extern const unsigned int ProcessEvents;

/*---------------------------------------------------------------------------*/

} // namespace Delays

/*---------------------------------------------------------------------------*/

namespace MessageColors {

/*---------------------------------------------------------------------------*/

	extern const unsigned int Default;

	extern const unsigned int Identifier;

	extern const unsigned int Error;

	extern const unsigned int Success;

	extern const unsigned int Warning;

/*---------------------------------------------------------------------------*/

} // namespace MessageColors

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_RESOURCES_HPP__
