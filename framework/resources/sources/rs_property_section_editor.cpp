// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_property_section_editor.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PropertySectionEditor::PropertySectionEditor(
		PropertySectionEditor & _parent
	,	const char * _sectionName
	,	int _sectionLevel
)
	:	m_name( _sectionName )
	,	m_parent( _parent )
{
	m_spaces.reserve( _sectionLevel );
	for ( int i = 0; i < _sectionLevel; ++i )
		m_spaces += '\t';

} // PropertySectionEditor::PropertySectionEditor

/*---------------------------------------------------------------------------*/

PropertySectionEditor &
PropertySectionEditor::property(
		const char * _name
	,	const char * _value
)
{
	m_body += Resources::Templates::Property.arg(
			m_spaces
		,	_name
		,	_value
	);

	return * this;

} // PropertySectionEditor::property

/*---------------------------------------------------------------------------*/

PropertySectionEditor &
PropertySectionEditor::section( const char * _name )
{
	m_activeNestedSection.reset(
		new PropertySectionEditor(
				* this
			,	_name
			,	m_spaces.length() + 1
		)
	);

	return * m_activeNestedSection;

} // PropertySectionEditor::section

/*---------------------------------------------------------------------------*/

PropertySectionEditor &
PropertySectionEditor::end()
{
	m_parent.m_body += Resources::Templates::PropertySection.arg(
			m_parent.m_spaces
		,	m_name
		,	m_body
	);

	return m_parent;

} // PropertySectionEditor::end

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
