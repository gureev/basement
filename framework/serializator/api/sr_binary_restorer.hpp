// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_BINARY_RESTORER_HPP__
#define __AMPLE_SERIALIZATOR_BINARY_RESTORER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/serializator/api/sr_export.hpp"

#include <QtCore/qdatastream.h>

/*---------------------------------------------------------------------------*/

class QDataStream;

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

class SERIALIZATOR_API BinaryRestorer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	BinaryRestorer( QDataStream & _inputStream );

	virtual ~BinaryRestorer();

/*---------------------------------------------------------------------------*/

	template< typename _RestoreType >
	_RestoreType restore();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	QDataStream & m_inputStream;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

template< typename _RestoreType >
inline
_RestoreType
BinaryRestorer::restore()
{
	_RestoreType result;
	m_inputStream >> result;
	return result;

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

template<>
inline
std::string
BinaryRestorer::restore< std::string >()
{
	int length = restore< int >();

	if ( !length )
		return "";

	std::string result;
	result.resize( length );

	m_inputStream.readRawData(
			&( *( result.begin() ) )
		,	length
	);

	return result;

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

template<>
inline
char
BinaryRestorer::restore< char >()
{
	return restore< unsigned char >();

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

template<>
inline
short
BinaryRestorer::restore< short >()
{
	return restore< unsigned short >();

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

template<>
inline
long
BinaryRestorer::restore< long >()
{
	return restore< unsigned long long >();

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

template<>
inline
unsigned long
BinaryRestorer::restore< unsigned long >()
{
	return restore< unsigned long long >();

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

template<>
inline
long long
BinaryRestorer::restore< long long >()
{
	return restore< unsigned long long >();

} // BinaryRestorer::restore

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_BINARY_RESTORER_HPP__
