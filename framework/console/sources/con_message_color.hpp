// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_MESSAGE_COLOR_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_MESSAGE_COLOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_message_level.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class MessageColor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	static int getColor( MessageLevel::Enum _messageLevel );

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_MESSAGE_COLOR_HPP__
