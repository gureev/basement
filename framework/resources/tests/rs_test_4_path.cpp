// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_path.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Path:
			1) Empty path
			2) One component
			3) Empty first component
			4) Empty second component
			5) Empty last component
			6) Empty first and second component
			7) Empty second and last component
			8) Empty first and last component
			9) Two components
			10) Three components

*/

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE( Suite_Path )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_1_EmptyPath )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_1_EmptyPath

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_2_OneComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "component1" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_2_OneComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_3_EmptyFirstComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "/component2" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_3_EmptyFirstComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_4_EmptySecondComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "component1//component3" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_4_EmptySecondComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_5_EmptyLastComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "component1/" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_5_EmptyLastComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_6_EmptyFirstAndSecondComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "//component3" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_6_EmptyFirstAndSecondComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_7_EmptySecondAndLastComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "component1//" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_7_EmptySecondAndLastComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_8_EmptyFirstAndLastComponent )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
		{
			Implementation::Path path( "/component2/" );
		}
		,	Exceptions::PathSyntaxError
	);

} // Path_8_EmptyFirstAndLastComponent

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_9_TwoComponents )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	try
	{
		Implementation::Path path( "component1/component2" );

		BOOST_REQUIRE_EQUAL( path.getComponentsCount(), 2 );

		BOOST_REQUIRE_EQUAL( path.getComponent( 0 ), "component1" );
		BOOST_REQUIRE_EQUAL( path.getComponent( 1 ), "component2" );

		BOOST_REQUIRE_EQUAL( path.getResource(), "component2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Path_9_TwoComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Path_10_ThreeComponents )
{
	/*---------- Setup ----------*/

	/*-------- Test actions -----*/

	try
	{
		Implementation::Path path( "component1/component2/component3" );

		BOOST_REQUIRE_EQUAL( path.getComponentsCount(), 3 );

		BOOST_REQUIRE_EQUAL( path.getComponent( 0 ), "component1" );
		BOOST_REQUIRE_EQUAL( path.getComponent( 1 ), "component2" );
		BOOST_REQUIRE_EQUAL( path.getComponent( 2 ), "component3" );

		BOOST_REQUIRE_EQUAL( path.getResource(), "component3" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Path_9_TwoComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Path

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
