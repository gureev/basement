// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/api/cn_exceptions.hpp"

#include "framework/connector/sources/cn_plugins_loader_impl.hpp"
#include "framework/connector/sources/cn_plugins_container_impl.hpp"
#include "framework/connector/sources/cn_plugins_info_restorer.hpp"

#include "framework/connector/sources/resources/cn_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

PluginsLoader &
PluginsLoader::getInstance()
{
	static Implementation::PluginsLoader loader;

	return loader;

} // PluginsLoader::getInstance

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PluginsLoader::PluginsLoader()
{
} // PluginsLoader::PluginsLoader

/*---------------------------------------------------------------------------*/

PluginsLoader::~PluginsLoader()
{
} // PluginsLoader::~PluginsLoader

/*---------------------------------------------------------------------------*/

void
PluginsLoader::restorePluginsInfo( QIODevice & _file ) const
{
	Implementation::PluginsContainer & pluginsContainer = Implementation::PluginsContainer::getInstance();

	PluginsInfoRestorer restorer(
			pluginsContainer
		,	_file
	);
	restorer.run();

	pluginsContainer.loadInstantPlugins();

} // PluginsLoader::restorePluginsInfo

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample
