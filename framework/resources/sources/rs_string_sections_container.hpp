// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTIONS_CONTAINER_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTIONS_CONTAINER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/tools/common_headers/containers/hash_map.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class StringSection;

/*---------------------------------------------------------------------------*/

class StringSectionsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	StringSectionsContainer();

	~StringSectionsContainer();

/*---------------------------------------------------------------------------*/

	StringSection & addSection(
			const std::string & _name
		,	const std::string & _shortName
	);

/*---------------------------------------------------------------------------*/

	const StringSection & getSection( const std::string & _name ) const;

	int getSectionsCount() const;

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::HashMap<
				std::string
			,	boost::shared_ptr< StringSection >
		>
		Sections;

	Sections m_sections;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTIONS_CONTAINER_HPP__
