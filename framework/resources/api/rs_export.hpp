// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_EXPORT_HPP__
#define __AMPLE_RESOURCES_EXPORT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {

/*---------------------------------------------------------------------------*/

#ifdef _PLUGIN_RESOURCES_
	#define RESOURCES_API __declspec( dllexport )
#else
	#define RESOURCES_API __declspec( dllimport )
#endif

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_EXPORT_HPP__
