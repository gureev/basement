// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PATH_FACTORY_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PATH_FACTORY_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/sources/rs_path_factory_comparator.hpp"

#include "framework/tools/common_headers/containers/hash_map.hpp"

#include <functional>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Path;

/*---------------------------------------------------------------------------*/

class PathFactory
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	const Path & createPath( const char * _path ) const;

/*---------------------------------------------------------------------------*/

	int getPathsCount() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::HashMap<
				const char *
			,	boost::shared_ptr< Path >
			,	std::hash< const char * >
			,	PathFactoryComparator
		>
		Paths;

	mutable Paths m_paths;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PATH_FACTORY_HPP__
