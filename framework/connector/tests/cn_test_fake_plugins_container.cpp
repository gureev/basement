// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/tests/cn_test_fake_plugins_container.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Tests {

/*---------------------------------------------------------------------------*/

void
FakePluginsContainer::loadInstantPlugins()
{
} // FakePluginsContainer::loadInstantPlugins

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Connector
} // namespace Ample
