// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTION_EDITOR_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTION_EDITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertySectionEditor
{

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	PropertySectionEditor(
			PropertySectionEditor & _parent
		,	const char * _sectionName
		,	int _sectionLevel
	);

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertySectionEditor & property(
			const char * _name
		,	const char * _value
	);

	PropertySectionEditor & section( const char * _name );

/*---------------------------------------------------------------------------*/

	PropertySectionEditor & end();

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	boost::scoped_ptr< PropertySectionEditor > m_activeNestedSection;

	PropertySectionEditor & m_parent;

	QString m_name;

	QString m_body;

	QString m_spaces;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTION_EDITOR_HPP__
