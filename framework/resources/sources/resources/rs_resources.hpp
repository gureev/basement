// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_RESOURCES_HPP__
#define __AMPLE_RESOURCES_RESOURCES_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/array.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Constants {

/*---------------------------------------------------------------------------*/

	extern const int MajorFormatVersion;

	extern const int MinorFormatVersion;

	extern const char * Plugins[ 5 ];

/*---------------------------------------------------------------------------*/

} // namespace Constants

/*---------------------------------------------------------------------------*/

namespace Paths {

/*---------------------------------------------------------------------------*/

	extern const QString PluginStrings;

/*---------------------------------------------------------------------------*/

} // namespace Paths

/*---------------------------------------------------------------------------*/

namespace Xml {

/*---------------------------------------------------------------------------*/

namespace Tags {

/*---------------------------------------------------------------------------*/

	extern const QString Section;

	extern const QString String;

	extern const QString Property;

/*---------------------------------------------------------------------------*/

} // namespace Tags

/*---------------------------------------------------------------------------*/

namespace Attributes {

/*---------------------------------------------------------------------------*/

	extern const QString Version;

	extern const QString SectionName;
	extern const QString SectionShortName;

	extern const QString StringIdentifier;
	extern const QString StringName;
	extern const QString StringValue;

	extern const QString PropertyName;
	extern const QString PropertyValue;

/*---------------------------------------------------------------------------*/

} // namespace Attributes

/*---------------------------------------------------------------------------*/

} // namespace Xml

/*---------------------------------------------------------------------------*/

namespace Templates {

/*---------------------------------------------------------------------------*/

	extern const QString Property;

	extern const QString PropertySection;

	extern const QString String;

	extern const QString StringSection;

/*---------------------------------------------------------------------------*/

} // namespace Templates

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_RESOURCES_HPP__
