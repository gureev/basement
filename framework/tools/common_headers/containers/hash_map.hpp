// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_HASH_MAP_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_HASH_MAP_HPP__

/*---------------------------------------------------------------------------*/

#include "implementation/map.hpp"

#include <boost/functional/hash.hpp>

#include <hash_map>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {

/*---------------------------------------------------------------------------*/

template<
		typename _KeyType
	,	typename _ValueType
	,	typename _Hasher = boost::hash< _KeyType >
	,	typename _Comparator = std::equal_to< _KeyType >
	,	typename _Allocator = std::allocator<
			std::pair<
					const _KeyType
				,	_ValueType
			>
		>
>
class HashMap
	:	public Implementation::Map<
			std::hash_map<
					_KeyType
				,	_ValueType
				,	_Hasher
				,	_Comparator
				,	_Allocator
			>
		>
{

/*---------------------------------------------------------------------------*/

	typedef
		Implementation::Map<
			std::hash_map<
					_KeyType
				,	_ValueType
				,	_Hasher
				,	_Comparator
				,	_Allocator
			>
		>
		BaseContainer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	HashMap()
		:	BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_HASH_MAP_HPP__
