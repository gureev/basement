// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_TEST_STRING_FIXTURE_HPP__
#define __AMPLE_RESOURCES_TEST_STRING_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/sources/rs_string_section.hpp"
#include "framework/resources/sources/rs_string_sections_container.hpp"
#include "framework/resources/sources/rs_string_section_editor.hpp"
#include "framework/resources/sources/rs_accessor_impl.hpp"

#include <QtCore/qbuffer.h>
#include <QtCore/qbytearray.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

class StringFixture
	:	public Implementation::StringSectionEditor

{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	StringFixture();

/*---------------------------------------------------------------------------*/

	const Implementation::StringSectionsContainer & runTest();

/*---------------------------------------------------------------------------*/

	void clear();

/*---------------------------------------------------------------------------*/

	Accessor & getAccessor();

	QIODevice & openStream();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	Implementation::StringSectionsContainer m_sections;

	Implementation::Accessor m_accessor;

	QBuffer m_buffer;

	QByteArray m_byteData;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_TEST_STRING_FIXTURE_HPP__
