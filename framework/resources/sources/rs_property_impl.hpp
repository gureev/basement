// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_property.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Property
	:	public Resources::Property
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Property( const std::string & _value );

	/*virtual*/ ~Property();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & asString() const;

	/*virtual*/ boost::optional< int > asInt() const;

	/*virtual*/ boost::optional< bool > asBool() const;

	/*virtual*/ boost::optional< double > asDouble() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::optional< double > m_asNumber;

	const std::string m_asString;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_HPP__
