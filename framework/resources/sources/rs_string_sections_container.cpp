// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_string_sections_container.hpp"
#include "framework/resources/sources/rs_string_section.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

StringSectionsContainer::StringSectionsContainer()
{
} // StringSectionsContainer::StringSectionsContainer

/*---------------------------------------------------------------------------*/

StringSectionsContainer::~StringSectionsContainer()
{
} // StringSectionsContainer::~StringSectionsContainer

/*---------------------------------------------------------------------------*/

StringSection &
StringSectionsContainer::addSection(
		const std::string & _name
	,	const std::string & _shortName
)
{
	if ( auto section = m_sections.getValue( _name ) )
		return ** section;

	boost::shared_ptr< StringSection > newSection( new StringSection( _shortName ) );
	m_sections[ _name ] = newSection;

	return * newSection;

} // StringSectionsContainer::addSection

/*---------------------------------------------------------------------------*/

const StringSection &
StringSectionsContainer::getSection( const std::string & _name ) const
{
	auto section = m_sections.getValue( _name );

	if ( ! section )
		EX_THROW_1_ARGUMENT(
				Exceptions::MissingSection
			,	_name
		);

	EX_DEBUG_ASSERT( * section );
	return ** section;

} // StringSectionsContainer::getSection

/*---------------------------------------------------------------------------*/

int
StringSectionsContainer::getSectionsCount() const
{
	return m_sections.size();

} // StringSectionsContainer::getSectionsCount

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
