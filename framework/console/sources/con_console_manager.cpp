// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/api/con_message_level.hpp"

#include "framework/console/sources/con_console_manager.hpp"

#include "framework/resources/api/rs_string.hpp"
#include "framework/exceptions/api/ex_exceptions.hpp"

#include <QtCore/qmetatype.h>

#include <boost/bind.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Q_DECLARE_METATYPE( std::string )
Q_DECLARE_METATYPE( MessageLevel::Enum )

/*---------------------------------------------------------------------------*/

ConsoleManager::ConsoleManager( const std::string & _applicationName )
	:	m_consoleReader(
				* this
			,	m_consoleWriter
		)
	,	m_consoleWriter( _applicationName )
{
	qRegisterMetaType< std::string >();
	qRegisterMetaType< MessageLevel::Enum >();

	QObject::connect(
			this
		,	SIGNAL( printEvent( MessageLevel::Enum, const std::string &, const std::string &, const std::string & ) )
		,	& m_consoleWriter
		,	SLOT( print( MessageLevel::Enum, const std::string &, const std::string &, const std::string & ) )
	);

	QObject::connect(
			this
		,	SIGNAL( printCommandEvent( const std::string & ) )
		,	& m_consoleWriter
		,	SLOT( printCommand( const std::string & ) )
	);

	m_consoleWriter.setInputControls(
			boost::bind(
					& ConsoleReader::disableInput
				,	& m_consoleReader
			)
		,	boost::bind(
					& ConsoleReader::enableInput
				,	& m_consoleReader
			)
	);

	m_consoleWriter.start();
	m_consoleReader.start();

} // ConsoleManager::ConsoleManager

/*---------------------------------------------------------------------------*/

ConsoleManager::~ConsoleManager()
{
	m_consoleReader.terminate();
	m_consoleReader.wait();

	m_consoleWriter.terminate();
	m_consoleWriter.wait();

} // ConsoleManager::~ConsoleManager

/*---------------------------------------------------------------------------*/

void
ConsoleManager::print(
		MessageLevel::Enum _messageLevel
	,	const Resources::String & _string
)
{
	emit printEvent(
			_messageLevel
		,	_string.getSectionName()
		,	_string.getIdentifier()
		,	_string.getValue()
	);

} // ConsoleManager::print

/*---------------------------------------------------------------------------*/

void
ConsoleManager::print(
		MessageLevel::Enum _messageLevel
	,	const std::string & _message
)
{
	emit printEvent(
			_messageLevel
		,	"TEST"
		,	"0000"
		,	_message
	);

} // ConsoleManager::print

/*---------------------------------------------------------------------------*/

void
ConsoleManager::onEnterKey( std::string _command )
{
	emit printCommandEvent( _command );

} // ConsoleManager::onEnterKey

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample
