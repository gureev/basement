// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_PLUGIN_INFO_HPP__
#define __AMPLE_CONNECTOR_PLUGIN_INFO_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_export.hpp"

#include "framework/connector/api/cn_plugin_load_policy.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

class Plugin;

/*---------------------------------------------------------------------------*/

class CONNECTOR_API PluginInfo
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~PluginInfo() {}

/*---------------------------------------------------------------------------*/

	virtual const Plugin & getPlugin() const = 0;

	virtual PluginLoadPolicy::Enum getLoadPolicy() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_PLUGIN_INFO_HPP__
