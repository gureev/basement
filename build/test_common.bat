@echo off

::  ---------------------------------------------------------------------------

color 07
call %~1 Test Build
if not errorlevel 0 goto TEST_RESULT_FAIL

cls

::  ---------------------------------------------------------------------------

set platform=%~2
set testFolder=%platform%\Test\bin
set testRunner=test_runner.exe

::  ---------------------------------------------------------------------------

echo.
call :TEST_SEPARATOR
echo ************************ TESTING ************************
call :TEST_SEPARATOR
echo.

::  ---------------------------------------------------------------------------

call :RUN_TESTS %testFolder% "*.test.exe" ""
if not errorlevel 0 goto TEST_RESULT_FAIL

call :RUN_TESTS %testFolder% "*.test.dll" %testFolder%\%testRunner%
if not errorlevel 0 goto TEST_RESULT_FAIL

goto TEST_RESULT_SUCCESS

::  ---------------------------------------------------------------------------

:TEST_RESULT_END_MESSAGE
echo.
call :TEST_SEPARATOR
echo ********************** END TESTING **********************
call :TEST_SEPARATOR
echo.
pause
exit /b 1

::  ---------------------------------------------------------------------------

:RUN_TESTS

set folder=%~1
set filter=%~2
set runner=%~3

for /r "%folder%" %%i in ( "%filter%" ) do (
	
	call :TEST_SEPARATOR
	echo Test: %%i

	call %runner% %%i "--detect_memory_leak=0"
	
	if not errorlevel 0 exit /b 1

	echo.
	echo.
)

exit /b 0

::  ---------------------------------------------------------------------------

:TEST_RESULT_SUCCESS
color 0A
goto TEST_RESULT_END_MESSAGE

::  ---------------------------------------------------------------------------

:TEST_RESULT_FAIL
color 0C
goto TEST_RESULT_END_MESSAGE

::  ---------------------------------------------------------------------------

:TEST_SEPARATOR
echo *********************************************************
exit /b 0

::  ---------------------------------------------------------------------------