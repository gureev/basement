// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTION_EDITOR_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTION_EDITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class StringSectionEditor
{

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	StringSectionEditor(
			StringSectionEditor & _parent
		,	const char * _sectionName
		,	const char * _sectionShortName
		,	int _sectionLevel
	);

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	StringSectionEditor & string(
			const char * _name
		,	const char * _identifier
		,	const char * _value
	);

	StringSectionEditor & section(
			const char * _name
		,	const char * _shortName
	);

/*---------------------------------------------------------------------------*/

	StringSectionEditor & end();

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	boost::scoped_ptr< StringSectionEditor > m_activeNestedSection;

	StringSectionEditor & m_parent;

	QString m_name;

	QString m_shortName;

	QString m_body;

	QString m_spaces;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTION_EDITOR_HPP__
