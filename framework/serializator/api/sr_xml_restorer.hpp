// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_XML_RESTORER_HPP__
#define __AMPLE_SERIALIZATOR_XML_RESTORER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/serializator/api/sr_export.hpp"

#include <QtXml/qdom.h>
#include <QtXml/qxml.h>

#include <QtCore/qstring.h>

/*---------------------------------------------------------------------------*/

class QIODevice;

namespace Ample
{
	namespace CommonHeaders
	{
		class Version;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

class SERIALIZATOR_API XmlRestorer
{

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	XmlRestorer( QIODevice & _inputStream );

	virtual ~XmlRestorer();

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	void checkRootElement(
			const QString & _requiredTagName
		//,	const QString & _requiredVersionAttributeName
		//,	const CommonHeaders::Version & _actualVersion
	) const;

/*---------------------------------------------------------------------------*/

	QDomElement m_rootElement;

	QDomDocument m_document;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_XML_SAVER_HPP__
