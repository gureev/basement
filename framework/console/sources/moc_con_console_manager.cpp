/****************************************************************************
** Meta object code from reading C++ file 'con_console_manager.hpp'
**
** Created: Tue 16. Apr 13:57:59 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "con_console_manager.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'con_console_manager.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Ample__Console__Implementation__ConsoleManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
     107,   48,   47,   47, 0x05,
     183,  174,   47,   47, 0x05,

 // slots: signature, parameters, type, tag, flags
     214,  174,   47,   47, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Ample__Console__Implementation__ConsoleManager[] = {
    "Ample::Console::Implementation::ConsoleManager\0"
    "\0_messageLevel,_moduleShortName,_messageIdentifier,_message\0"
    "printEvent(MessageLevel::Enum,std::string,std::string,std::string)\0"
    "_command\0printCommandEvent(std::string)\0"
    "onEnterKey(std::string)\0"
};

void Ample::Console::Implementation::ConsoleManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ConsoleManager *_t = static_cast<ConsoleManager *>(_o);
        switch (_id) {
        case 0: _t->printEvent((*reinterpret_cast< MessageLevel::Enum(*)>(_a[1])),(*reinterpret_cast< const std::string(*)>(_a[2])),(*reinterpret_cast< const std::string(*)>(_a[3])),(*reinterpret_cast< const std::string(*)>(_a[4]))); break;
        case 1: _t->printCommandEvent((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 2: _t->onEnterKey((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Ample::Console::Implementation::ConsoleManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall
};

const QMetaObject Ample::Console::Implementation::ConsoleManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Ample__Console__Implementation__ConsoleManager,
      qt_meta_data_Ample__Console__Implementation__ConsoleManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Ample::Console::Implementation::ConsoleManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Ample::Console::Implementation::ConsoleManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Ample::Console::Implementation::ConsoleManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Ample__Console__Implementation__ConsoleManager))
        return static_cast<void*>(const_cast< ConsoleManager*>(this));
    return QObject::qt_metacast(_clname);
}

int Ample::Console::Implementation::ConsoleManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void Ample::Console::Implementation::ConsoleManager::printEvent(MessageLevel::Enum _t1, const std::string & _t2, const std::string & _t3, const std::string & _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Ample::Console::Implementation::ConsoleManager::printCommandEvent(const std::string & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
