// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_EXCEPTIONS_HPP__
#define __AMPLE_EXCEPTIONS_EXCEPTIONS_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_base_exception.hpp"
#include "framework/exceptions/api/ex_internal_error.hpp"

#include "framework/exceptions/sources/ex_internal_macros.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

#ifdef EX_THROW_0_ARGUMENT
	#error
#else
#define EX_THROW_0_ARGUMENT( _EXCEPTION )									\
{																			\
	_EXCEPTION exception(													\
			__FILE__														\
		,	__LINE__														\
	);																		\
																			\
	boost::throw_exception( exception );									\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_THROW_1_ARGUMENT
	#error
#else
#define EX_THROW_1_ARGUMENT(												\
		_EXCEPTION															\
	,	_ARGUMENT1															\
)																			\
{																			\
	_EXCEPTION exception(													\
			__FILE__														\
		,	__LINE__														\
		,	_ARGUMENT1														\
	);																		\
																			\
	boost::throw_exception( exception );									\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_THROW_2_ARGUMENT
	#error
#else
#define EX_THROW_2_ARGUMENT(												\
		_EXCEPTION															\
	,	_ARGUMENT1															\
	,	_ARGUMENT2															\
)																			\
{																			\
	_EXCEPTION exception(													\
			__FILE__														\
		,	__LINE__														\
		,	_ARGUMENT1														\
		,	_ARGUMENT2														\
	);																		\
																			\
	boost::throw_exception( exception );									\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_THROW_3_ARGUMENT
	#error
#else
#define EX_THROW_3_ARGUMENT(												\
		_EXCEPTION															\
	,	_ARGUMENT1															\
	,	_ARGUMENT2															\
	,	_ARGUMENT3															\
)																			\
{																			\
	_EXCEPTION exception(													\
			__FILE__														\
		,	__LINE__														\
		,	_ARGUMENT1														\
		,	_ARGUMENT2														\
		,	_ARGUMENT3														\
	);																		\
																			\
	boost::throw_exception( exception );									\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_INTERNAL_ERROR
	#error
#else
#define EX_INTERNAL_ERROR													\
{																			\
	Ample::Exceptions::InternalError exception(					\
			__FILE__														\
		,	__LINE__														\
	);																		\
																			\
	boost::throw_exception( exception );									\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_ASSERT
	#error
#else
#define EX_ASSERT( _CONDITION )												\
{																			\
	if ( !( _CONDITION ) )													\
		EX_INTERNAL_ERROR													\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_DEBUG_ASSERT
	#error
#else
#define EX_DEBUG_ASSERT( _CONDITION )										\
{																			\
	BOOST_ASSERT_MSG(														\
			_CONDITION														\
		,	""																\
	);																		\
}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_DECLARE_BASE_EXCEPTION
	#error
#else
#define EX_DECLARE_BASE_EXCEPTION(											\
		_EXCEPTION															\
	,	_VISITOR															\
)																			\
	EX__INTERNAL_BASE_EXCEPTION_CLASS_TEMPLATE(								\
			_EXCEPTION														\
		,	_VISITOR														\
	)
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_DECLARE_0_ARGUMENT
	#error
#else
#define EX_DECLARE_0_ARGUMENT(												\
		_BASE_EXCEPTION														\
	,	_EXCEPTION															\
)																			\
	EX__INTERNAL_DECLARE_EXCEPTION(											\
			_BASE_EXCEPTION													\
		,	_EXCEPTION														\
		,																	\
	)
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_DECLARE_1_ARGUMENT
	#error
#else
#define EX_DECLARE_1_ARGUMENT(												\
		_BASE_EXCEPTION														\
	,	_EXCEPTION															\
	,	_ARGUMENT1															\
)																			\
	EX__INTERNAL_DECLARE_EXCEPTION(											\
			_BASE_EXCEPTION													\
		,	_EXCEPTION														\
		,	( _ARGUMENT1 )													\
	)
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_DECLARE_2_ARGUMENT
	#error
#else
#define EX_DECLARE_2_ARGUMENT(												\
		_BASE_EXCEPTION														\
	,	_EXCEPTION															\
	,	_ARGUMENT1															\
	,	_ARGUMENT2															\
)																			\
	EX__INTERNAL_DECLARE_EXCEPTION(											\
			_BASE_EXCEPTION													\
		,	_EXCEPTION														\
		,	( _ARGUMENT1 )													\
			( _ARGUMENT2 )													\
	)
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX_DECLARE_3_ARGUMENT
	#error
#else
#define EX_DECLARE_3_ARGUMENT(												\
		_BASE_EXCEPTION														\
	,	_EXCEPTION															\
	,	_ARGUMENT1															\
	,	_ARGUMENT2															\
	,	_ARGUMENT3															\
)																			\
	EX__INTERNAL_DECLARE_EXCEPTION(											\
			_BASE_EXCEPTION													\
		,	_EXCEPTION														\
		,	( _ARGUMENT1 )													\
			( _ARGUMENT2 )													\
			( _ARGUMENT3 )													\
	)
#endif

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_EXCEPTIONS_HPP__
