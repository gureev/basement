// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_EXPORT_HPP__
#define __AMPLE_SERIALIZATOR_EXPORT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

#ifdef _PLUGIN_SERIALIZATOR_
	#define SERIALIZATOR_API __declspec( dllexport )
#else
	#define SERIALIZATOR_API __declspec( dllimport )
#endif

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_EXPORT_HPP__
