// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

/*---------------------------------------------------------------------------*/

#define BOOST_TEST_MODULE FileSystem

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

TEST_RUNNER_ENTRY_POINT()

/*---------------------------------------------------------------------------*/
