// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTION_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTION_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_string.hpp"
#include "framework/resources/api/rs_access_type.hpp"

#include "framework/resources/sources/rs_string_sections_container.hpp"

#include "framework/tools/common_headers/containers/hash_map.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class StringList;

/*---------------------------------------------------------------------------*/

class StringSection
	:	public StringSectionsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	StringSection( const std::string & _shortName );

	~StringSection();

/*---------------------------------------------------------------------------*/

	void addResource(
			const std::string & _name
		,	const std::string & _identifier
		,	const std::string & _value
	);

/*---------------------------------------------------------------------------*/

	const Resources::String & getResource(
			const std::string & _name
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const;

	int getResourcesCount() const;

/*---------------------------------------------------------------------------*/

	StringSection & addSection(
			const std::string & _name
		,	const std::string & _shortName
	);

/*---------------------------------------------------------------------------*/

	const std::string & getShortName() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::HashMap<
				std::string
			,	boost::shared_ptr< Implementation::StringList >
		>
		Strings;

	Strings m_strings;

	const std::string m_shortName;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_STRING_SECTION_HPP__
