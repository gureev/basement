// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_PLUGINS_CONTAINER_HPP__
#define __AMPLE_CONNECTOR_PLUGINS_CONTAINER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_export.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

class PluginInfo;
class PluginIdentifier;

/*---------------------------------------------------------------------------*/

class CONNECTOR_API PluginsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~PluginsContainer() {}

/*---------------------------------------------------------------------------*/

	static PluginsContainer & getInstance();

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< PluginInfo >
		findPlugin( const PluginIdentifier & _pluginIdentifier ) const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_PLUGINS_CONTAINER_HPP__
