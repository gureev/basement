// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_MESSAGE_STATISTIC_HPP__
#define __AMPLE_CONSOLE_MESSAGE_STATISTIC_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {

/*---------------------------------------------------------------------------*/

class MessageStatistic
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~MessageStatistic() {}

/*---------------------------------------------------------------------------*/

	virtual int getErrorsCount() const = 0;

	virtual int getWarningsCount() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_MESSAGE_STATISTIC_HPP__
