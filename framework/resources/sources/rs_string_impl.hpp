// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_STRING_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_STRING_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_string.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class String
	:	public Resources::String
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	String(
			const std::string & _sectionName
		,	const std::string & _identifier
		,	const std::string & _value
	);

	/*virtual*/ ~String();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & getSectionName() const;

	/*virtual*/ const std::string & getIdentifier() const;

	/*virtual*/ const std::string & getValue() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	const std::string & m_sectionName;

	const std::string m_identifier;

	const std::string m_value;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_STRING_HPP__
