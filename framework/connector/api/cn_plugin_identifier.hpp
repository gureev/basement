// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_HPP__
#define __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_export.hpp"

#include <boost/array.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

class CONNECTOR_API PluginIdentifier
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	typedef
		unsigned int
		MajorType;

	typedef
		unsigned short
		MinorType;

	typedef
		unsigned char
		SubMinorType;

/*---------------------------------------------------------------------------*/

	PluginIdentifier(
			const MajorType _major
		,	const MinorType _minor0
		,	const MinorType _minor1
		,	const SubMinorType _subMinor0
		,	const SubMinorType _subMinor1
		,	const SubMinorType _subMinor2
		,	const SubMinorType _subMinor3
		,	const SubMinorType _subMinor4
		,	const SubMinorType _subMinor5
		,	const SubMinorType _subMinor6
		,	const SubMinorType _subMinor7
	);

	PluginIdentifier();

	~PluginIdentifier();

/*---------------------------------------------------------------------------*/

	bool equals( const PluginIdentifier & _pluginIdentifier ) const;

	bool isValid() const;

/*---------------------------------------------------------------------------*/

	MajorType getMajorValue() const;

	MinorType getMinorValue( int _index ) const;

	SubMinorType getSubMinorValue( int _index ) const;

/*---------------------------------------------------------------------------*/

	int getMinorValuesCount() const;

	int getSubMinorValuesCount() const;

/*---------------------------------------------------------------------------*/

	static PluginIdentifier fromString( const QString & _identifier );

/*---------------------------------------------------------------------------*/

	std::string toString() const;

	QString toQString() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	MajorType m_major;

	boost::array< MinorType, 2 > m_minor;

	boost::array< SubMinorType, 8 > m_subMinor;

/*---------------------------------------------------------------------------*/

	static const int ComponentsCount = 11;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_HPP__
