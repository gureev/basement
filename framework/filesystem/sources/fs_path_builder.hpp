// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_PATH_BUILDER_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_PATH_BUILDER_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace FileSystem
	{
		class Path;

	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PathBuilder
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	static boost::shared_ptr< FileSystem::Path > build( const std::string & _path );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	static void optimize( std::string & _path );

	static bool isAbsolute( const std::string & _path );

	static bool isSlash( const char _symbol );

	static const char extractDriveLetter( const std::string & _path );

	static void extractPathComponents(
			boost::shared_ptr< FileSystem::Path > & _path
		,	const std::string & _stringPath
		,	int _startPosition
	);

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_PATH_BUILDER_HPP__
