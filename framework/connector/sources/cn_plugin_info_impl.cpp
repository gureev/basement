// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/api/cn_plugin.hpp"
#include "framework/connector/api/cn_plugins_container.hpp"
#include "framework/connector/api/cn_exceptions.hpp"

#include "framework/connector/sources/cn_plugin_info_impl.hpp"

#include "framework/connector/sources/resources/cn_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PluginInfo::PluginInfo(
		const QString & _libraryName
	,	PluginLoadPolicy::Enum _loadPolicy
)
	:	m_plugin()
	,	m_library( _libraryName )
	,	m_loadPolicy( _loadPolicy )
{
#ifdef _TEST_
	m_library.setFileName(
		m_library.fileName().replace( ".dll", ".test.dll" )
	);
#endif

} // PluginInfo::PluginInfo

/*---------------------------------------------------------------------------*/

PluginInfo::~PluginInfo()
{
	if ( m_library.isLoaded() )
		m_library.unload();

} // PluginInfo::~PluginInfo

/*---------------------------------------------------------------------------*/

void
PluginInfo::initializeAccessor()
{
	if ( isInitialized() )
		return;

	initializePlugin();

	m_plugin->onPluginLoad();

} // PluginInfo::initializeAccessor

/*---------------------------------------------------------------------------*/

void
PluginInfo::initializePlugin()
{
	if ( m_library.isLoaded() )
		return;

	QString libraryName = m_library.fileName();

	if ( !QLibrary::isLibrary( libraryName ) )
		EX_THROW_1_ARGUMENT(
				Exceptions::PluginLibraryNotFound
			,	libraryName
		)

	m_library.load();
	if ( !m_library.isLoaded() )
		EX_THROW_1_ARGUMENT(
				Exceptions::CannotLoadPluginLibrary
			,	libraryName
		)

	PluginFactory pluginFactory = resolvePluginFactory();

	if ( !pluginFactory )
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenPlugin
			,	libraryName
		)

	m_plugin = pluginFactory();

	auto pluginInfo = PluginsContainer::getInstance().findPlugin( m_plugin->getIdentifier() );

	if ( !pluginInfo )
		EX_THROW_1_ARGUMENT(
				Exceptions::PluginInformationNotFound
			,	libraryName
		)

} // PluginInfo::initializePlugin

/*---------------------------------------------------------------------------*/

PluginInfo::PluginFactory
PluginInfo::resolvePluginFactory()
{
	m_library.setLoadHints( QLibrary::ExportExternalSymbolsHint );
	return static_cast< PluginFactory >(
		m_library.resolve( Resources::Constants::PluginFactoryFunction )
	);

} // PluginInfo::resolvePluginFactory

/*---------------------------------------------------------------------------*/

bool
PluginInfo::isInitialized() const
{
	return	m_plugin
		&&	m_plugin->getAccessor()
	;

} // PluginInfo::isInitialized

/*---------------------------------------------------------------------------*/

const Plugin &
PluginInfo::getPlugin() const
{
	const_cast< PluginInfo & >( * this ).initializeAccessor();

	EX_DEBUG_ASSERT( m_plugin );
	return * m_plugin;

} // PluginInfo::getPlugin

/*---------------------------------------------------------------------------*/

PluginLoadPolicy::Enum
PluginInfo::getLoadPolicy() const
{
	return m_loadPolicy;

} // PluginInfo::getLoadPolicy

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample
