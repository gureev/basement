// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_COMPARATOR_HPP__
#define __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_COMPARATOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Utils {

/*---------------------------------------------------------------------------*/

class PluginIdentifierComparator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	bool operator () (
			const PluginIdentifier & _left
		,	const PluginIdentifier & _right
	) const
	{
		return _left.equals( _right );

	} // operator ()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Utils
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_COMPARATOR_HPP__
