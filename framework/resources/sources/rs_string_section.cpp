// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_string_section.hpp"
#include "framework/resources/sources/rs_string_list.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

StringSection::StringSection( const std::string & _shortName )
	:	m_shortName( _shortName )
{
} // StringSection::StringSection

/*---------------------------------------------------------------------------*/

StringSection::~StringSection()
{
} // StringSection::~StringSection

/*---------------------------------------------------------------------------*/

const std::string &
StringSection::getShortName() const
{
	return m_shortName;

} // StringSection::getShortName

/*---------------------------------------------------------------------------*/

void
StringSection::addResource(
		const std::string & _name
	,	const std::string & _identifier
	,	const std::string & _value
)
{
	Strings::iterator it = m_strings.find( _name );

	if ( it == m_strings.end() )
	{
		boost::shared_ptr< Implementation::StringList > list( new StringList );
		m_strings[ _name ] = list;

		it = m_strings.find( _name );
	}

	it->second->add(
			m_shortName
		,	_identifier
		,	_value
	);

} // StringSection::addResource

/*---------------------------------------------------------------------------*/

StringSection &
StringSection::addSection(
		const std::string & _name
	,	const std::string & _shortName
)
{
	if ( m_strings.contains( _name ) )
		EX_THROW_1_ARGUMENT(
				Exceptions::DuplicateResource
			,	_name
		);

	return StringSectionsContainer::addSection(
			_name
		,	_shortName
	);

} // StringSection::addSection

/*---------------------------------------------------------------------------*/

const Resources::String &
StringSection::getResource(
		const std::string & _name
	,	AccessType::Enum _accessType
) const
{
	auto resource = m_strings.getValue( _name );

	if ( !resource )
		EX_THROW_1_ARGUMENT(
				Exceptions::MissingResource
			,	_name
		);

	EX_DEBUG_ASSERT( * resource );
	return ( * resource )->get( _accessType );

} // StringSection::getResource

/*---------------------------------------------------------------------------*/

int
StringSection::getResourcesCount() const
{
	return m_strings.size();

} // StringSection::getResourcesCount

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
