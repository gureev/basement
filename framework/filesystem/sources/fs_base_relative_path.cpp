// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_base_relative_path.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

BaseRelativePath::BaseRelativePath( int _componentsCount )
	:	BasePath( _componentsCount )
{
} // BaseRelativePath::BaseRelativePath

/*---------------------------------------------------------------------------*/

BaseRelativePath::BaseRelativePath( const BaseRelativePath & _other )
	:	BasePath( _other.m_components.size() )
{
	int nComponents = _other.m_components.size();
	for ( int i = 0; i < nComponents; ++i )
		m_components[ i ] = _other.m_components[ i ];

} // BaseRelativePath::BaseRelativePath

/*---------------------------------------------------------------------------*/

bool
BaseRelativePath::isAbsolute() const
{
	return false;

} // BaseRelativePath::isAbsolute

/*---------------------------------------------------------------------------*/

bool
BaseRelativePath::isRelative() const
{
	return true;

} // BaseRelativePath::isRelative

/*---------------------------------------------------------------------------*/

FileSystem::Path::Ptr
BaseRelativePath::clone() const
{
	boost::shared_ptr< Implementation::BaseRelativePath > newPath(
		new BaseRelativePath( * this )
	);

	return boost::static_pointer_cast< FileSystem::Path >( newPath );

} // BaseRelativePath::clone

/*---------------------------------------------------------------------------*/

char
BaseRelativePath::getDriveLetter() const
{
	EX_DEBUG_ASSERT( false );
	return 0;

} // BaseRelativePath::getDriveLetter

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
