// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_PROPERTY_VALIDATOR_HPP__
#define __AMPLE_RESOURCES_PROPERTY_VALIDATOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_property_type.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {

/*---------------------------------------------------------------------------*/

class PropertyValidator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~PropertyValidator() {}

/*---------------------------------------------------------------------------*/

	virtual PropertyValidator & expectType( PropertyType::Enum _type ) = 0;

/*---------------------------------------------------------------------------*/

	virtual PropertyValidator & expectValue(
			int _lowerBound
		,	int _upperBound
	) = 0;

	virtual PropertyValidator & expectValue(
			double _lowerBound
		,	double _upperBound
	) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_PROPERTY_VALIDATOR_HPP__
