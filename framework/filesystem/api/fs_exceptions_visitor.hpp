// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_EXCEPTIONS_VISITOR_HPP__
#define __AMPLE_FILESYSTEM_EXCEPTIONS_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class CannotOpenFileForReading;
class CannotOpenFileForWriting;

/*---------------------------------------------------------------------------*/

class ExceptionsVisitor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~ExceptionsVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const CannotOpenFileForReading & /*_exception*/ ) {}

	virtual void visit( const CannotOpenFileForWriting & /*_exception*/ ) {}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_EXCEPTIONS_VISITOR_HPP__
