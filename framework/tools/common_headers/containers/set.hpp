// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_SET_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_SET_HPP__

/*---------------------------------------------------------------------------*/

#include "implementation/set.hpp"

#include <set>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {

/*---------------------------------------------------------------------------*/

template<
		typename _KeyType
	,	typename _Comparator = std::less< _KeyType >
	,	typename _Allocator = std::allocator< _KeyType >
>
class Set
	:	public Implementation::Set<
			std::set<
					_KeyType
				,	_Comparator
				,	_Allocator
			>
		>
{

/*---------------------------------------------------------------------------*/

	typedef
		Implementation::Set<
			std::set<
					_KeyType
				,	_Comparator
				,	_Allocator
			>
		>
		BaseContainer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Set()
		:	BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_SET_HPP__
