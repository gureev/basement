// (C) 2013 Ample Exceptions

#include "framework/exceptions/sources/ph/ex_ph.hpp"

#include "framework/exceptions/api/ex_base_exception.hpp"
#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/exceptions/sources/resources/ex_resources.hpp"

#include <boost/format.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

BaseException::BaseException()
{
} // BaseException::BaseException

/*---------------------------------------------------------------------------*/

const char *
BaseException::what() const
{
	m_what = (
		boost::format( Resources::Messages::UnhandledException )
		%	throwFileName()
		%	throwLineNumber()
	).str();

	return m_what.data();

} // BaseException::what

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample
