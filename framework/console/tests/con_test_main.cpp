// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

/*---------------------------------------------------------------------------*/

#define BOOST_TEST_MODULE Console

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

TEST_RUNNER_ENTRY_POINT()

/*---------------------------------------------------------------------------*/
