// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_IMPLEMENTATION_MAP_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_IMPLEMENTATION_MAP_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/optional.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {
namespace Implementation {

/*---------------------------------------------------------------------------*/

template< typename _BaseContainer >
class Map
	:	public _BaseContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	typedef
		typename _BaseContainer::mapped_type
		ValueType;

	typedef
		typename _BaseContainer::key_type
		KeyType;

	typedef
		typename _BaseContainer::value_type
		PairType;

/*---------------------------------------------------------------------------*/

	Map()
		:	_BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

	int size() const
	{
		return static_cast< int >( _BaseContainer::size() );
	}

/*---------------------------------------------------------------------------*/

	bool contains( const KeyType & _key ) const
	{
		return this->find( _key ) != this->end();
	}

/*---------------------------------------------------------------------------*/

	boost::optional< const ValueType & > getValue( const KeyType & _key ) const
	{
		typedef
			boost::optional< const ValueType & >
			ReturnType;

		typename _BaseContainer::const_iterator it = this->find( _key );

		return	it != this->end()
			?	ReturnType( it->second )
			:	ReturnType()
		;
	}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_IMPLEMENTATION_MAP_HPP__
