// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_COMPARATOR_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_COMPARATOR_HPP__

/*---------------------------------------------------------------------------*/

#include <functional>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertyValidator;

/*---------------------------------------------------------------------------*/

class PropertyValidatorComparator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	bool operator() (
			const boost::shared_ptr< Implementation::PropertyValidator > & _left
		,	const boost::shared_ptr< Implementation::PropertyValidator > & _right
	) const
	{
		return ( * this )(
				_left.get()
			,	_right.get()
		);

	} // operator()

/*---------------------------------------------------------------------------*/

	bool operator() (
			const Ample::Resources::PropertyValidator * _left
		,	const boost::shared_ptr< Implementation::PropertyValidator > & _right
	) const
	{
		return ( * this )(
				_left
			,	_right.get()
		);

	} // operator()

/*---------------------------------------------------------------------------*/

	bool operator() (
			const boost::shared_ptr< Implementation::PropertyValidator > & _left
		,	const Ample::Resources::PropertyValidator * _right
	) const
	{
		return ( * this )(
				_left.get()
			,	_right
		);

	} // operator()

/*---------------------------------------------------------------------------*/

	bool operator() (
			const Ample::Resources::PropertyValidator * _left
		,	const Ample::Resources::PropertyValidator * _right
	) const
	{
		return std::equal_to< const Ample::Resources::PropertyValidator * >()(
				_left
			,	_right
		);

	} // operator()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_COMPARATOR_HPP__
