// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_PLUGIN_IDENTIFIER_HPP__
#define __AMPLE_RESOURCES_PLUGIN_IDENTIFIER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Plugin {

/*---------------------------------------------------------------------------*/

// {06146ED2-44A3-4C94-A80A-97F134856D0A}
const Connector::PluginIdentifier Identifier
( 0x6146ED2, 0x44A3, 0x4C94, 0xA8, 0xA, 0x97, 0xF1, 0x34, 0x85, 0x6D, 0xA );

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_PLUGIN_IDENTIFIER_HPP__
