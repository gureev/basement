// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_ACCESSOR_HPP__
#define __AMPLE_RESOURCES_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_accessor.hpp"

#include "framework/resources/api/rs_access_type.hpp"
#include "framework/resources/api/rs_string.hpp"

/*---------------------------------------------------------------------------*/

class QIODevice;

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {

/*---------------------------------------------------------------------------*/

class String;
class Property;
class PropertyValidator;

/*---------------------------------------------------------------------------*/

class Accessor
	:	public Connector::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual const String & getString(
			const char * _resourcePath
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const = 0;

	virtual const Property & getProperty(
			const char * _resourcePath
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const = 0;

/*---------------------------------------------------------------------------*/

	virtual void validateProperty(
			const char * _resourcePath
		,	const PropertyValidator & _validator
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const = 0;

/*---------------------------------------------------------------------------*/

	virtual PropertyValidator & createValidator() = 0;

/*---------------------------------------------------------------------------*/

	virtual void restoreStrings( QIODevice & _stream ) = 0;

	virtual void restoreProperties( QIODevice & _stream ) = 0;

/*---------------------------------------------------------------------------*/

	virtual void storeStrings(
			QIODevice & _stream
		,	const std::string & _sectionName
	) = 0;

	virtual void storeProperties(
			QIODevice & _stream
		,	const std::string & _sectionName
	) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_ACCESSOR_HPP__
