// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_HASH_SET_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_HASH_SET_HPP__

/*---------------------------------------------------------------------------*/

#include "implementation/set.hpp"

#include <boost/functional/hash.hpp>

#include <hash_set>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {

/*---------------------------------------------------------------------------*/

template<
		typename _KeyType
	,	typename _Hasher = boost::hash< _KeyType >
	,	typename _Comparator = std::equal_to< _KeyType >
	,	typename _Allocator = std::allocator< _KeyType >
>
class HashSet
	:	public Implementation::Set<
			std::hash_set<
					_KeyType
				,	_Hasher
				,	_Comparator
				,	_Allocator
			>
		>
{

/*---------------------------------------------------------------------------*/

	typedef
		Implementation::Set<
			std::hash_set<
					_KeyType
				,	_Hasher
				,	_Comparator
				,	_Allocator
			>
		>
		BaseContainer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	HashSet()
		:	BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_HASH_SET_HPP__
