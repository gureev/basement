// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_FILE_HANDLE_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_FILE_HANDLE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_file_handle.hpp"
#include "framework/filesystem/api/fs_open_mode.hpp"

#include <QtCore/qfile.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class FileHandle
	:	public FileSystem::FileHandle
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	FileHandle(
			boost::shared_ptr< FileSystem::Path > _path
		,	OpenMode::Enum _openMode
	);

	~FileHandle();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const FileSystem::Path & getPath() const;

	/*virtual*/ QIODevice & getDevice();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< FileSystem::Path > m_path;

	QFile m_file;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_FILE_HANDLE_HPP__
