/****************************************************************************
** Meta object code from reading C++ file 'con_console_reader.hpp'
**
** Created: Wed 17. Apr 10:27:33 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "con_console_reader.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'con_console_reader.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Ample__Console__Implementation__ConsoleReader[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      74,   47,   46,   46, 0x05,
     119,  110,   46,   46, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_Ample__Console__Implementation__ConsoleReader[] = {
    "Ample::Console::Implementation::ConsoleReader\0"
    "\0_input,_cursorLinePosition\0"
    "onInputChangeEvent(std::string,int)\0"
    "_command\0onEnterKeyEvent(std::string)\0"
};

void Ample::Console::Implementation::ConsoleReader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ConsoleReader *_t = static_cast<ConsoleReader *>(_o);
        switch (_id) {
        case 0: _t->onInputChangeEvent((*reinterpret_cast< const std::string(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->onEnterKeyEvent((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Ample::Console::Implementation::ConsoleReader::staticMetaObjectExtraData = {
    0,  qt_static_metacall
};

const QMetaObject Ample::Console::Implementation::ConsoleReader::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_Ample__Console__Implementation__ConsoleReader,
      qt_meta_data_Ample__Console__Implementation__ConsoleReader, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Ample::Console::Implementation::ConsoleReader::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Ample::Console::Implementation::ConsoleReader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Ample::Console::Implementation::ConsoleReader::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Ample__Console__Implementation__ConsoleReader))
        return static_cast<void*>(const_cast< ConsoleReader*>(this));
    return QThread::qt_metacast(_clname);
}

int Ample::Console::Implementation::ConsoleReader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void Ample::Console::Implementation::ConsoleReader::onInputChangeEvent(const std::string & _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Ample::Console::Implementation::ConsoleReader::onEnterKeyEvent(std::string _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
