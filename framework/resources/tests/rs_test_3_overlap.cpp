// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_string_sections_container.hpp"
#include "framework/resources/sources/rs_property_sections_container.hpp"

//#include "framework/resources/tests/rs_test_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Overlap:
			1) Strings
				1.1) Overlapped section
					1.1.1) Without strings
					1.1.2) One overlapped string
					1.1.3) Two overlapped strings
					1.1.4) One non overlapped string
					1.1.5) Two non overlapped strings
				1.2) Non overlapped section
					1.2.1) Without strings
					1.2.2) One string
					1.2.3) Two strings
			2) Properties
				2.1) Overlapped section
					2.1.1) Without properties
					2.1.2) One overlapped property
					2.1.3) Two overlapped properties
					2.1.4) One non overlapped property
					2.1.5) Two non overlapped properties
				2.2) Non overlapped section
					2.2.1) Without properties
					2.2.2) One property
					2.2.3) Two properties

*/

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
