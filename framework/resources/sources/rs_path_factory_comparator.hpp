// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PATH_FACTORY_COMPARATOR_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PATH_FACTORY_COMPARATOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PathFactoryComparator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	bool operator () (
			const char * _left
		,	const char * _right
	) const
	{
		return strcmp( _left, _right ) == 0;

	} // operator ()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PATH_FACTORY_COMPARATOR_HPP__
