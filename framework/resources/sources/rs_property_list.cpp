// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_property_list.hpp"
#include "framework/resources/sources/rs_property_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PropertyList::PropertyList()
{
} // PropertyList::PropertyList

/*---------------------------------------------------------------------------*/

PropertyList::~PropertyList()
{
} // PropertyList::~PropertyList

/*---------------------------------------------------------------------------*/

void
PropertyList::add( const std::string & _value )
{
	boost::shared_ptr< Implementation::Property > property(
		new Implementation::Property( _value )
	);

	m_properties.push_back( property );

} // PropertySection::add

/*---------------------------------------------------------------------------*/

const Resources::Property &
PropertyList::get( AccessType::Enum _accessType ) const
{
	EX_DEBUG_ASSERT( ! m_properties.empty() );

	return _accessType == AccessType::FirstAvailable
		?	* m_properties.back()
		:	* m_properties.front()
	;

} // PropertyList::get

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
