// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

/*---------------------------------------------------------------------------*/

#define BOOST_TEST_MODULE Resources

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

TEST_RUNNER_ENTRY_POINT()

/*---------------------------------------------------------------------------*/