// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGIN_EDITOR_HPP__
#define __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGIN_EDITOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_load_policy.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PluginEditor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PluginEditor();

/*---------------------------------------------------------------------------*/

	PluginEditor & plugin(
			const char * _name
		,	const char * _identifier
		,	const char * _loadPolicy
	);

/*---------------------------------------------------------------------------*/

	PluginEditor & begin();

	PluginEditor & end();

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	QString m_body;

	QString m_spaces;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGIN_EDITOR_HPP__
