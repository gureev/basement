// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/con_accessor_impl.hpp"

#include "framework/resources/api/rs_string.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Accessor::Accessor( const std::string & _applicationName )
	:	m_console( _applicationName )
{
} // Accessor::Accessor

/*---------------------------------------------------------------------------*/

Accessor::~Accessor()
{
} // Accessor::~Accessor

/*---------------------------------------------------------------------------*/

void
Accessor::print(
		MessageLevel::Enum _messageLevel
	,	const Resources::String & _string
)
{
	m_messageStatistic.updateMessagesCounter( _messageLevel );
	m_console.print(
			_messageLevel
		,	_string
	);

} // Accessor::print

/*---------------------------------------------------------------------------*/

void
Accessor::print(
		MessageLevel::Enum _messageLevel
	,	const std::string & _message
)
{
	m_messageStatistic.updateMessagesCounter( _messageLevel );
	m_console.print(
			_messageLevel
		,	_message
	);

} // Accessor::print

/*---------------------------------------------------------------------------*/

const MessageStatistic &
Accessor::getMessageStatistic() const
{
	return m_messageStatistic;

} // Accessor::getMessageStatistic

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample
