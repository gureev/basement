// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_PATH_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_PATH_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_path.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class BasePath
	:	public FileSystem::Path
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	BasePath( int _componentsCount );

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & getComponent( int _index ) const;

	/*virtual*/ int getComponentsCount() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ FileSystem::Path & stepUp();

/*---------------------------------------------------------------------------*/

	/*virtual*/ FileSystem::Path & operator /= ( const std::string & _component );

/*---------------------------------------------------------------------------*/

	/*virtual*/ std::string toString() const;

	/*virtual*/ QString toQString() const;

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	template< typename _String >
	_String buildString() const;

	virtual int calculatePathLength() const;

	template< typename _String >
	void fillPathString( _String & _string ) const;

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::Vector< std::string >
		Components;

	Components m_components;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_PATH_HPP__
