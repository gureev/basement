// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_PLUGIN_IDENTIFIER_HPP__
#define __AMPLE_CONSOLE_PLUGIN_IDENTIFIER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Plugin {

/*---------------------------------------------------------------------------*/

// {7A44A49C-DFBE-4D2E-810F-CFCEE430E8E2}
const Connector::PluginIdentifier Identifier
( 0x7A44A49C, 0xDFBE, 0x4D2E, 0x81, 0xF, 0xCF, 0xCE, 0xE4, 0x30, 0xE8, 0xE2 );

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_PLUGIN_IDENTIFIER_HPP__
