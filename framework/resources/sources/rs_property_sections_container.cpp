// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_property_sections_container.hpp"
#include "framework/resources/sources/rs_property_section.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PropertySectionsContainer::PropertySectionsContainer()
{
} // PropertySectionsContainer::PropertySectionsContainer

/*---------------------------------------------------------------------------*/

PropertySectionsContainer::~PropertySectionsContainer()
{
} // PropertySectionsContainer::~PropertySectionsContainer

/*---------------------------------------------------------------------------*/

PropertySection &
PropertySectionsContainer::addSection( const std::string & _name )
{
	if ( auto section = m_sections.getValue( _name ) )
		return ** section;

	boost::shared_ptr< PropertySection > newSection( new PropertySection() );
	m_sections[ _name ] = newSection;

	return * newSection;

} // PropertySectionsContainer::addSection

/*---------------------------------------------------------------------------*/

const PropertySection &
PropertySectionsContainer::getSection( const std::string & _name ) const
{
	auto section = m_sections.getValue( _name );

	if ( ! section )
		EX_THROW_1_ARGUMENT(
				Exceptions::MissingSection
			,	_name
		);

	EX_DEBUG_ASSERT( * section );
	return ** section;

} // PropertySectionsContainer::getSection

/*---------------------------------------------------------------------------*/

int
PropertySectionsContainer::getSectionsCount() const
{
	return m_sections.size();

} // PropertySectionsContainer::getSectionsCount

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
