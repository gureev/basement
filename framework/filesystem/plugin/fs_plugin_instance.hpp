// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_PLUGIN_INSTANCE_HPP__
#define __AMPLE_FILESYSTEM_PLUGIN_INSTANCE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_accessor.hpp"

#include "framework/filesystem/plugin/fs_plugin_identifier.hpp"
#include "framework/filesystem/plugin/fs_environment.hpp"

#include "framework/connector/api/cn_plugin.hpp"
#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Plugin {

/*---------------------------------------------------------------------------*/

class Instance
	:	public Connector::Plugin
	,	public Environment
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Instance();

	/*virtual*/ ~Instance();

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Connector::Accessor > getAccessor() const;

	/*virtual*/ const Connector::PluginIdentifier & getIdentifier() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	/*virtual*/ void onPluginLoad();

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< Accessor > m_accessor;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_PLUGIN_INSTANCE_HPP__
