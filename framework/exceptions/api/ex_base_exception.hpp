// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_BASE_EXCEPTION_HPP__
#define __AMPLE_EXCEPTIONS_BASE_EXCEPTION_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_export.hpp"

#include <exception>
#include <boost/exception/exception.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class EXCEPTIONS_API BaseException
	:	public virtual std::exception
	,	public virtual boost::exception
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	BaseException();

	virtual ~BaseException() {}

/*---------------------------------------------------------------------------*/

	virtual const std::string & throwFileName() const = 0;

	virtual const int & throwLineNumber() const = 0;

/*---------------------------------------------------------------------------*/

	/*virtual*/ const char * what() const;

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	mutable std::string m_what;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_BASE_EXCEPTION_HPP__
