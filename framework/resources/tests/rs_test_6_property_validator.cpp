// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"
#include "framework/resources/api/rs_property_validator.hpp"

#include "framework/resources/sources/rs_path.hpp"

#include "framework/resources/tests/rs_test_property_validator_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Property validator:
			1) Type
				1.1) Expected string
					1.1.1) Real string
					1.1.2) Real int
					1.1.3) Real double
					1.1.4) Real bool
				1.2) Expected int
					1.2.1) Real string
					1.2.2) Real int
					1.2.3) Real double
					1.2.4) Real bool
				1.3) Expected double
					1.3.1) Real string
					1.3.2) Real int
					1.3.3) Real double
					1.3.4) Real bool
				1.4) Expected bool
					1.4.1) Real string
					1.4.2) Real int
					1.4.3) Real double
					1.4.4) Real bool
			2) Value
				2.1) One range
					2.1.1) Value in range
					2.1.2) Value is lower than the lower bound
					2.1.3) Value higher than the upper bound
					2.1.4) Value is lower than the integer lower bound
					2.1.5) Value higher than the integer upper bound
					2.1.6) Value is equal with the lower bound
					2.1.7) Value is equal with the upper bound
				2.2) Two ranges
					2.2.1) Value in first range
					2.2.2) Value in second range
					2.2.3) Value is lower than the first range lower bound
					2.2.4) Value between ranges bounds
					2.2.5) Value higher than the second range upper bound
					2.2.6) Value is equal with the first range lower bound
					2.2.7) Value is equal with the first range upper bound
					2.2.8) Value is equal with the second range lower bound
					2.2.9) Value is equal with the second range upper bound

*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_PropertyValidator, PropertyValidatorFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_1_1_Type_ExpectedString_RealString )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "value" )
	.end();

	getValidator().expectType( PropertyType::String );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_1_1_Type_ExpectedString_RealString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_1_2_Type_ExpectedString_RealInt )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "65000" )
	.end();

	getValidator().expectType( PropertyType::String );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_1_2_Type_ExpectedString_RealInt

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_1_3_Type_ExpectedString_RealDouble )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "50.50" )
	.end();

	getValidator().expectType( PropertyType::String );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_1_3_Type_ExpectedString_RealDouble

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_1_4_Type_ExpectedString_RealBool )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "true" )
	.end();

	getValidator().expectType( PropertyType::String );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_1_4_Type_ExpectedString_RealBool

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_2_1_Type_ExpectedInt_RealString )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "value" )
	.end();

	getValidator().expectType( PropertyType::Int );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_1_2_1_Type_ExpectedInt_RealString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_2_2_Type_ExpectedInt_RealInt )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "100" )
	.end();

	getValidator().expectType( PropertyType::Int );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_2_2_Type_ExpectedInt_RealInt

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_2_3_Type_ExpectedInt_RealDouble )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "49.90" )
	.end();

	getValidator().expectType( PropertyType::Int );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_2_3_Type_ExpectedInt_RealDouble

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_2_4_Type_ExpectedInt_RealBool )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "false" )
	.end();

	getValidator().expectType( PropertyType::Int );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_2_4_Type_ExpectedInt_RealBool

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_3_1_Type_ExpectedDouble_RealString )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "value" )
	.end();

	getValidator().expectType( PropertyType::Double );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_1_3_1_Type_ExpectedDouble_RealString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_3_2_Type_ExpectedDouble_RealInt )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "100" )
	.end();

	getValidator().expectType( PropertyType::Double );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_3_2_Type_ExpectedDouble_RealInt

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_3_3_Type_ExpectedDouble_RealDouble )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "49.90" )
	.end();

	getValidator().expectType( PropertyType::Double );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_3_3_Type_ExpectedDouble_RealDouble

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_3_4_Type_ExpectedDouble_RealBool )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "false" )
	.end();

	getValidator().expectType( PropertyType::Double );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_3_4_Type_ExpectedDouble_RealBool

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_4_1_Type_ExpectedBool_RealString )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "value" )
	.end();

	getValidator().expectType( PropertyType::Bool );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_1_4_1_Type_ExpectedBool_RealString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_4_2_Type_ExpectedBool_RealInt )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "100" )
	.end();

	getValidator().expectType( PropertyType::Bool );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_4_2_Type_ExpectedBool_RealInt

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_4_3_Type_ExpectedBool_RealDouble )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "49.90" )
	.end();

	getValidator().expectType( PropertyType::Bool );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_4_3_Type_ExpectedBool_RealDouble

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_1_4_4_Type_ExpectedBool_RealBool )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "false" )
	.end();

	getValidator().expectType( PropertyType::Bool );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_1_4_4_Type_ExpectedBool_RealBool

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_1_Value_OneRange_ValueInRange )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "15" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_1_1_Value_OneRange_ValueInRange

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_2_Value_OneRange_ValueIsLowerThanTheLowerBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "5" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_2_1_2_Value_OneRange_ValueIsLowerThanTheLowerBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_3_Value_OneRange_ValueHigherThanTheUpperBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "25" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_2_1_3_Value_OneRange_ValueHigherThanTheUpperBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_4_Value_OneRange_ValueIsLowerThanTheIntegerLowerBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "-44000000000" )
	.end();

	getValidator()
		.expectType( PropertyType::Double )
		.expectValue( -45000000000.0, -43000000000.0 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_1_4_Value_OneRange_ValueIsLowerThanTheIntegerLowerBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_5_Value_OneRange_ValueHigherThanTheIntegerUpperBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "44000000000" )
	.end();

	getValidator()
		.expectType( PropertyType::Double )
		.expectValue( 43000000000.0, 45000000000.0 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_1_5_Value_OneRange_ValueHigherThanTheIntegerUpperBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_6_Value_OneRange_ValueIsEqualWithTheLowerBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "10" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_1_6_Value_OneRange_ValueIsEqualWithTheLowerBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_1_7_Value_OneRange_ValueIsEqualWithTheUpperBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "20" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_1_7_Value_OneRange_ValueIsEqualWithTheUpperBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_1_Value_TwoRanges_ValueInFirstRange )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "15" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_2_1_Value_TwoRanges_ValueInFirstRange

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_2_Value_TwoRanges_ValueInSecondRange )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "35" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_2_2_Value_TwoRanges_ValueInSecondRange

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_3_Value_TwoRanges_ValueIsLowerThanTheFirstRangeLowerBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "5" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_2_2_3_Value_TwoRanges_ValueIsLowerThanTheFirstRangeLowerBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_4_Value_TwoRanges_ValueBetweenRangesBounds )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "25" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_2_2_4_Value_TwoRanges_ValueBetweenRangesBounds

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_5_Value_TwoRanges_ValueHigherThanTheSecondRangeUpperBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "45" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );

	BOOST_REQUIRE_THROW(
			validateProperty()
		,	Exceptions::InvalidPropertyValue
	);

} // PropertyValidator_2_2_5_Value_TwoRanges_ValueHigherThanTheSecondRangeUpperBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_6_Value_TwoRanges_ValueIsEqualWithTheFirstRangeLowerBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "10" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_2_6_Value_TwoRanges_ValueIsEqualWithTheFirstRangeLowerBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_7_Value_TwoRanges_ValueIsEqualWithTheFirstRangeUpperBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "20" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_2_7_Value_TwoRanges_ValueIsEqualWithTheFirstRangeUpperBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_8_Value_TwoRanges_ValueIsEqualWithTheSecondRangeLowerBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "30" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_2_8_Value_TwoRanges_ValueIsEqualWithTheSecondRangeLowerBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PropertyValidator_2_2_9_Value_TwoRanges_ValueIsEqualWithTheSecondRangeUpperBound )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "40" )
	.end();

	getValidator()
		.expectType( PropertyType::Int )
		.expectValue( 10, 20 )
		.expectValue( 30, 40 )
	;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( runTest() );
	BOOST_REQUIRE_NO_THROW( validateProperty() );

} // PropertyValidator_2_2_9_Value_TwoRanges_ValueIsEqualWithTheSecondRangeUpperBound

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_PropertyValidator

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
