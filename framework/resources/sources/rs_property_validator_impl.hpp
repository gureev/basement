// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_property_validator.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace Resources
	{
		class Property;

		namespace Implementation
		{
			class Path;
		}
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertyValidator
	:	public Ample::Resources::PropertyValidator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertyValidator();

	/*virtual*/ ~PropertyValidator();

/*---------------------------------------------------------------------------*/

	void validate(
			const Path & _propertyPath
		,	const Ample::Resources::Property & _property
	) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ Ample::Resources::PropertyValidator & expectType( PropertyType::Enum _type );

/*---------------------------------------------------------------------------*/

	/*virtual*/ Ample::Resources::PropertyValidator & expectValue(
			int _lowerBound
		,	int _upperBound
	);

	/*virtual*/ Ample::Resources::PropertyValidator & expectValue(
			double _lowerBound
		,	double _upperBound
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	template< typename _PropertyType >
	void validatePropertyType(
			const Path & _propertyPath
		,	boost::optional< _PropertyType > _propertyValue
	) const;

/*---------------------------------------------------------------------------*/

	PropertyType::Enum m_propertyType;

	typedef
		CommonHeaders::Containers::Vector<
			std::pair< double, double >
		>
		Constraints;

	Constraints m_constraints;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_HPP__
