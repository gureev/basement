// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_path.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Path::Path( const std::string & _resourcePath )
{
	int nComponents = 1 + std::count(
			_resourcePath.begin()
		,	_resourcePath.end()
		,	'/'
	);

	if ( nComponents <= 1 )
		EX_THROW_1_ARGUMENT(
				Exceptions::PathSyntaxError
			,	_resourcePath
		);

	m_components.reserve( nComponents );

	int startPosition = 0;
	int endPosition = 0;
	int length = _resourcePath.length();

	while ( startPosition <= length )
	{
		endPosition = _resourcePath.find(
				'/'
			,	startPosition
		);
		if ( endPosition == std::string::npos )
			endPosition = length;

		if ( startPosition == endPosition )
			EX_THROW_1_ARGUMENT(
					Exceptions::PathSyntaxError
				,	_resourcePath
			);

		m_components.push_back(
			_resourcePath.substr(
					startPosition
				,	endPosition - startPosition
			)
		);
		startPosition = endPosition + 1;
	}

} // Path::Path

/*---------------------------------------------------------------------------*/

Path::~Path()
{
} // Path::~Path

/*---------------------------------------------------------------------------*/

int
Path::getComponentsCount() const
{
	return m_components.size();

} // Path::getComponentsCount

/*---------------------------------------------------------------------------*/

const std::string &
Path::getComponent( int _index ) const
{
	EX_DEBUG_ASSERT(
			_index >= 0
		&&	_index < getComponentsCount()
	);
	return m_components[ _index ];

} // Path::getComponent

/*---------------------------------------------------------------------------*/

const std::string &
Path::getResource() const
{
	EX_DEBUG_ASSERT( getComponentsCount() );
	return m_components.back();

} // Path::getResource

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
