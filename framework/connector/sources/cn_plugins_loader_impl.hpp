// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_LOADER_HPP__
#define __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_LOADER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugins_loader.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PluginsLoader
	:	public Connector::PluginsLoader
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PluginsLoader();

	/*virtual*/ ~PluginsLoader();

/*---------------------------------------------------------------------------*/

	/*virtual*/ void restorePluginsInfo( QIODevice & _file ) const;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_LOADER_HPP__
