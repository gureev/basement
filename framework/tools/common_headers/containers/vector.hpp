// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_VECTOR_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_VECTOR_HPP__

/*---------------------------------------------------------------------------*/

#include <vector>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {

/*---------------------------------------------------------------------------*/

template<
		typename _ValueType
	,	typename _Allocator = std::allocator< _ValueType >
>
class Vector
	:	public std::vector<
				_ValueType
			,	_Allocator
		>
{

/*---------------------------------------------------------------------------*/

	typedef
		std::vector<
				_ValueType
			,	_Allocator
		>
		BaseContainer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Vector()
		:	BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

	void reset()
	{
		BaseContainer::clear();
	}

/*---------------------------------------------------------------------------*/

	int size() const
	{
		return static_cast< int >( BaseContainer::size() );
	}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_VECTOR_HPP__
