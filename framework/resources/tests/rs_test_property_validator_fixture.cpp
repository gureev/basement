// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_path.hpp"

#include "framework/resources/tests/rs_test_property_validator_fixture.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

PropertyValidatorFixture::PropertyValidatorFixture()
	:	m_validator( new Implementation::PropertyValidator() )
{
} // PropertyValidatorFixture::PropertyValidatorFixture

/*---------------------------------------------------------------------------*/

Resources::PropertyValidator &
PropertyValidatorFixture::getValidator()
{
	EX_DEBUG_ASSERT( m_validator );
	return * m_validator;

} // PropertyValidatorFixture::getValidator

/*---------------------------------------------------------------------------*/

void
PropertyValidatorFixture::validateProperty()
{
	Resources::Implementation::Path propertyPath( "test/prop" );

	const Property & property = m_sections.getSection( "test" ).getResource( "prop" );

	m_validator->validate(
			propertyPath
		,	property
	);

} // PropertyValidatorFixture::validateProperty

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
