// (C) 2013 Ample Exceptions

#include "framework/exceptions/sources/ph/ex_ph.hpp"

#include "framework/exceptions/sources/resources/ex_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Messages {

/*---------------------------------------------------------------------------*/

	const std::string InternalError = "An unknown internal error occurred at: %1% (%2%)";

	const std::string UnhandledException = "Unhandled exception at: %1% (%2%)";

/*---------------------------------------------------------------------------*/

} // namespace Messages

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Exceptions
} // namespace Ample
