// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_strings_restorer.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

#include <QtXml/qdom.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

StringsRestorer::StringsRestorer(
		StringSectionsContainer & _sectionsContainer
	,	QIODevice & _stream
)
	:	BaseRestorer( _stream )
	,	m_sectionsContainer( _sectionsContainer )
{
} // StringsRestorer::StringsRestorer

/*---------------------------------------------------------------------------*/

StringsRestorer::~StringsRestorer()
{
} // StringsRestorer::~StringsRestorer

/*---------------------------------------------------------------------------*/

void
StringsRestorer::restoreString(
		StringSection & _section
	,	QDomElement & _stringTag
)
{
	QString restoredName = _stringTag.attribute( Resources::Xml::Attributes::StringName );
	QString value = _stringTag.attribute( Resources::Xml::Attributes::StringValue );
	QString identifier = _stringTag.attribute( Resources::Xml::Attributes::StringIdentifier );

	if (	restoredName.isEmpty()
		||	value.isEmpty()
		||	identifier.isNull()
	)
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_stringTag.lineNumber()
		);

	std::string stringName = restoredName.toAscii().data();

	if ( m_restoredStrings.getValue( stringName ) )
		EX_THROW_1_ARGUMENT(
				Exceptions::DuplicateResource
			,	stringName
		);

	m_restoredStrings.insert( stringName );

	_section.addResource(
			stringName
		,	identifier.toAscii().data()
		,	value.toAscii().data()
	);

} // StringsRestorer::restoreString

/*---------------------------------------------------------------------------*/

void
StringsRestorer::restoreSection(
		StringSection & _section
	,	QDomElement & _sectionTag
)
{
	m_restoredStrings.clear();

	QDomElement stringTag = _sectionTag.firstChildElement( Resources::Xml::Tags::String );

	while ( ! stringTag.isNull() )
	{
		restoreString(
				_section
			,	stringTag
		);

		stringTag = stringTag.nextSiblingElement( stringTag.tagName() );
	}

	restoreNestedSections(
			_section
		,	_sectionTag
	);

} // StringsRestorer::restoreSection

/*---------------------------------------------------------------------------*/

template<
		typename _SectionsContainer
	,	typename _SectionTag
>
void
StringsRestorer::restoreNestedSections(
		_SectionsContainer & _section
	,	_SectionTag & _sectionTag
)
{
	RestoredResources restoredSections;

	QDomElement sectionTag = _sectionTag.firstChildElement( Resources::Xml::Tags::Section );

	while ( ! sectionTag.isNull() )
	{
		QString restoredSectionName = sectionTag.attribute( Resources::Xml::Attributes::SectionName );
		QString restoredSectionShortName = sectionTag.attribute( Resources::Xml::Attributes::SectionShortName );

		if (	restoredSectionName.isEmpty()
			||	restoredSectionShortName.isNull()
		)
			EX_THROW_1_ARGUMENT(
					Exceptions::BrokenAttribute
				,	sectionTag.lineNumber()
			);

		std::string sectionName = restoredSectionName.toAscii().data();

		if ( restoredSections.getValue( sectionName ) )
			EX_THROW_1_ARGUMENT(
					Exceptions::DuplicateSection
				,	sectionName
			);

		restoredSections.insert( sectionName );

		StringSection & section = _section.addSection(
				sectionName
			,	restoredSectionShortName.toAscii().data()
		);

		restoreSection(
				section
			,	sectionTag
		);

		sectionTag = sectionTag.nextSiblingElement( sectionTag.tagName() );
	}

} // StringsRestorer::restoreNestedSections

/*---------------------------------------------------------------------------*/

void
StringsRestorer::run()
{
	restoreNestedSections(
			m_sectionsContainer
		,	m_document
	);

} // StringsRestorer::run

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
