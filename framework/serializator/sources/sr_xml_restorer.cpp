// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

#include "framework/serializator/api/sr_xml_restorer.hpp"
#include "framework/serializator/api/sr_exceptions.hpp"

#include "framework/serializator/sources/resources/sr_resources.hpp"

#include "framework/tools/common_headers/version.hpp"

#include <QtCore/qfile.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

XmlRestorer::XmlRestorer( QIODevice & _inputStream )
{
	if ( ! _inputStream.isOpen() )
		EX_THROW_0_ARGUMENT( Exceptions::RestoreFailure )

	if ( ! m_document.setContent( & _inputStream ) )
	{
		_inputStream.close();
		EX_THROW_0_ARGUMENT( Exceptions::RestoreFailure )
	}

	_inputStream.close();

	m_rootElement = m_document.documentElement();

	if ( m_rootElement.isNull() )
		EX_THROW_0_ARGUMENT( Exceptions::RestoreFailure )

} // XmlRestorer::XmlRestorer

/*---------------------------------------------------------------------------*/

XmlRestorer::~XmlRestorer()
{
} // XmlRestorer::~XmlRestorer

/*---------------------------------------------------------------------------*/

void
XmlRestorer::checkRootElement(
		const QString & _requiredTagName
	//,	const QString & _requiredVersionAttributeName
	//,	const CommonHeaders::Version & _actualVersion
) const
{
	if ( m_rootElement.tagName() != _requiredTagName )
		EX_THROW_0_ARGUMENT( Exceptions::RootElementMismatch )

	/*
	QString versionAttribute = m_rootElement.attribute( _requiredVersionAttributeName );

	CommonHeaders::Version restoredVersion = CommonHeaders::Version::fromString( versionAttribute );

	if ( !( restoredVersion.isValid() && _actualVersion.isSatisfies( restoredVersion ) ) )
		EX_THROW_0_ARGUMENT( Exceptions::VersionMismatch )
	*/

} // XmlRestorer::checkRootElement

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample
