// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_path.hpp"
#include "framework/resources/sources/rs_accessor_impl.hpp"
#include "framework/resources/sources/rs_strings_restorer.hpp"
#include "framework/resources/sources/rs_properties_restorer.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Accessor::Accessor()
{
} // Accessor::Accessor

/*---------------------------------------------------------------------------*/

Accessor::~Accessor()
{
} // Accessor::~Accessor

/*---------------------------------------------------------------------------*/

template<
		typename _Resource
	,	typename _Section
	,	typename _SectionContainer
>
const _Resource &
Accessor::getResource(
		const _SectionContainer & _sections
	,	const Path & _resourcePath
	,	AccessType::Enum _accessType
) const
{
	const _Section & section = getSection< _Section >(
			_sections
		,	_resourcePath
	);

	return section.getResource(
			_resourcePath.getResource()
		,	_accessType
	);

} // Accessor::getResource

/*---------------------------------------------------------------------------*/

template<
		typename _Section
	,	typename _SectionContainer
>
const _Section &
Accessor::getSection(
		const _SectionContainer & _sections
	,	const Path & _resourcePath
) const
{
	boost::optional< const _Section & > section =
		_sections.getSection( _resourcePath.getComponent( 0 ) );

	int nSections = _resourcePath.getComponentsCount() - 1;
	for ( int i = 1; i < nSections; ++i )
		section = section->getSection( _resourcePath.getComponent( i ) );

	EX_DEBUG_ASSERT( section );
	return * section;

} // Accessor::getSection

/*---------------------------------------------------------------------------*/

const Resources::String &
Accessor::getString(
		const char * _resourcePath
	,	AccessType::Enum _accessType
) const
{
	const Path & resourcePath = m_pathFactory.createPath( _resourcePath );

	return getResource<
			Resources::String
		,	StringSection
	>(
			m_strings
		,	resourcePath
		,	_accessType
	);

} // Accessor::getString

/*---------------------------------------------------------------------------*/

const Resources::Property &
Accessor::getProperty(
		const char * _resourcePath
	,	AccessType::Enum _accessType
) const
{
	const Path & resourcePath = m_pathFactory.createPath( _resourcePath );

	return getResource<
			Resources::Property
		,	PropertySection
	>(
			m_properties
		,	resourcePath
		,	_accessType
	);

} // Accessor::getProperty

/*---------------------------------------------------------------------------*/

void
Accessor::validateProperty(
		const char * _resourcePath
	,	const Resources::PropertyValidator & _validator
	,	AccessType::Enum _accessType
) const
{
	const Path & propertyPath = m_pathFactory.createPath( _resourcePath );

	const Resources::Property & property = getResource<
			Resources::Property
		,	PropertySection
	>(
			m_properties
		,	propertyPath
		,	_accessType
	);

	auto it = m_propertyValidators.find( & _validator );
	EX_DEBUG_ASSERT( it != m_propertyValidators.end() );

	( *it )->validate(
			propertyPath
		,	property
	);

} // Accessor::validateProperty

/*---------------------------------------------------------------------------*/

Resources::PropertyValidator &
Accessor::createValidator()
{
	boost::shared_ptr< Implementation::PropertyValidator > validator(
		new Implementation::PropertyValidator
	);

	m_propertyValidators.insert( validator );

	return * validator;

} // Accessor::createValidator

/*---------------------------------------------------------------------------*/

void
Accessor::restoreStrings( QIODevice & _stream )
{
	StringsRestorer restorer(
			m_strings
		,	_stream
	);
	restorer.run();

} // Accessor::restoreStrings

/*---------------------------------------------------------------------------*/

void
Accessor::restoreProperties( QIODevice & _stream )
{
	PropertiesRestorer restorer(
			m_properties
		,	_stream
	);
	restorer.run();

} // Accessor::restoreProperties

/*---------------------------------------------------------------------------*/

void
Accessor::storeStrings(
		QIODevice & _stream
	,	const std::string & _sectionName
)
{
} // Accessor::storeStrings

/*---------------------------------------------------------------------------*/

void
Accessor::storeProperties(
		QIODevice & _stream
	,	const std::string & _sectionName
)
{
} // Accessor::storeProperties

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
