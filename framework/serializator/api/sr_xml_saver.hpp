// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_XML_SAVER_HPP__
#define __AMPLE_SERIALIZATOR_XML_SAVER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/serializator/api/sr_export.hpp"

#include <QtCore/qfile.h>
#include <QtXml/qdom.h>

/*---------------------------------------------------------------------------*/

class QString;

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

class SERIALIZATOR_API XmlSaver
{

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	XmlSaver( QIODevice & _outputStream );

	virtual ~XmlSaver();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	QIODevice & m_outputStream;

	QDomDocument m_document;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_XML_SAVER_HPP__
