// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_TEST_FAKE_PLUGINS_CONTAINER_HPP__
#define __AMPLE_CONNECTOR_TEST_FAKE_PLUGINS_CONTAINER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

#include "framework/connector/sources/cn_plugins_container_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Tests {

/*---------------------------------------------------------------------------*/

class FakePluginsContainer
	:	public Connector::Implementation::PluginsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	/*virtual*/ void loadInstantPlugins();

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_TEST_FAKE_PLUGINS_CONTAINER_HPP__
