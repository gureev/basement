// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_accessor.hpp"

#include "framework/resources/sources/rs_default_strings_restorer.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

#include <boost/bind.hpp>

#include <QtCore/qfile.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

DefaultStringsRestorer::DefaultStringsRestorer( Accessor & _accessor )
	:	m_accessor( _accessor )
{
} // DefaultStringsRestorer::DefaultStringsRestorer

/*---------------------------------------------------------------------------*/

DefaultStringsRestorer::~DefaultStringsRestorer()
{
} // DefaultStringsRestorer::~DefaultStringsRestorer

/*---------------------------------------------------------------------------*/

void
DefaultStringsRestorer::run()
{
	const char ** begin = Resources::Constants::Plugins;
	const int size = sizeof( Resources::Constants::Plugins ) / sizeof( Resources::Constants::Plugins[ 0 ] );

	std::for_each(
			begin
		,	begin + size
		,	boost::bind(
					& DefaultStringsRestorer::restoreStrings
				,	this
				,	_1
			)
	);

} // DefaultStringsRestorer::run

/*---------------------------------------------------------------------------*/

void
DefaultStringsRestorer::restoreStrings( const char * _fileName )
{
	QString filePath = Resources::Paths::PluginStrings + _fileName;

	QFile file( filePath );
	file.open( QIODevice::ReadOnly );

	m_accessor.restoreStrings( file );

} // DefaultStringsRestorer::restoreStrings

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
