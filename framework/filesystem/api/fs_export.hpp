// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_EXPORT_HPP__
#define __AMPLE_FILESYSTEM_EXPORT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {

/*---------------------------------------------------------------------------*/

#ifdef _PLUGIN_FILESYSTEM_
	#define FILESYSTEM_API __declspec( dllexport )
#else
	#define FILESYSTEM_API __declspec( dllimport )
#endif

/*---------------------------------------------------------------------------*/

} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_EXPORT_HPP__
