// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_strings_restorer.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

#include "framework/resources/tests/rs_test_string_fixture.hpp"

#include <QtCore/qbuffer.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

StringFixture::StringFixture()
	:	Implementation::StringSectionEditor(
				* this
			,	"XML_TEST_FILE"
			,	"XML_TEST_FILE"
			,	0
		)
{
} // StringFixture::StringFixture

/*---------------------------------------------------------------------------*/

Accessor &
StringFixture::getAccessor()
{
	return m_accessor;

} // StringFixture::getAccessor

/*---------------------------------------------------------------------------*/

QIODevice &
StringFixture::openStream()
{
	m_byteData = m_body.toAscii();

	m_buffer.setBuffer( & m_byteData );
	m_buffer.open( QIODevice::ReadOnly );

	return m_buffer;

} // StringFixture::openStream

/*---------------------------------------------------------------------------*/

void
StringFixture::clear()
{
	m_body = "";

} // StringFixture::clear

/*---------------------------------------------------------------------------*/

const Implementation::StringSectionsContainer &
StringFixture::runTest()
{
	QByteArray data = m_body.toAscii();

	QBuffer buffer( & data );
	buffer.open( QIODevice::ReadOnly );

	Implementation::StringsRestorer restorer(
			m_sections
		,	buffer
	);
	restorer.run();

	return m_sections;

} // StringFixture::runTest

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
