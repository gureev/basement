// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_MESSAGE_STATISTIC_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_MESSAGE_STATISTIC_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_message_statistic.hpp"
#include "framework/console/api/con_message_level.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class MessageStatistic
	:	public Console::MessageStatistic
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	MessageStatistic();

	/*virtual*/ ~MessageStatistic();

/*---------------------------------------------------------------------------*/

	void reset();

/*---------------------------------------------------------------------------*/

	/*virtual*/ int getErrorsCount() const;

	/*virtual*/ int getWarningsCount() const;

/*---------------------------------------------------------------------------*/

	void updateMessagesCounter( MessageLevel::Enum _messageLevel );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	int m_errors;

	int m_warnings;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_MESSAGE_STATISTIC_HPP__
