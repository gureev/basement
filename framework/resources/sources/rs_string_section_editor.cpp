// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_string_section_editor.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

StringSectionEditor::StringSectionEditor(
		StringSectionEditor & _parent
	,	const char * _sectionName
	,	const char * _sectionShortName
	,	int _sectionLevel
)
	:	m_name( _sectionName )
	,	m_shortName( _sectionShortName )
	,	m_parent( _parent )
{
	m_spaces.reserve( _sectionLevel );
	for ( int i = 0; i < _sectionLevel; ++i )
		m_spaces += '\t';

} // StringSectionEditor::StringSectionEditor

/*---------------------------------------------------------------------------*/

StringSectionEditor &
StringSectionEditor::string(
		const char * _name
	,	const char * _identifier
	,	const char * _value
)
{
	m_body += Resources::Templates::String.arg(
			m_spaces
		,	_name
		,	_identifier
		,	_value
	);

	return * this;

} // StringSectionEditor::string

/*---------------------------------------------------------------------------*/

StringSectionEditor &
StringSectionEditor::section(
		const char * _name
	,	const char * _shortName
)
{
	m_activeNestedSection.reset(
		new StringSectionEditor(
				* this
			,	_name
			,	_shortName
			,	m_spaces.length() + 1
		)
	);

	return * m_activeNestedSection;

} // StringSectionEditor::section

/*---------------------------------------------------------------------------*/

StringSectionEditor &
StringSectionEditor::end()
{
	m_parent.m_body += Resources::Templates::StringSection.arg(
			m_parent.m_spaces
		,	m_name
		,	m_shortName
		,	m_body
	);

	return m_parent;

} // StringSectionEditor::end

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
