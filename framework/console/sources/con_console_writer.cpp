// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/con_console_writer.hpp"
#include "framework/console/sources/con_console_reader.hpp"
#include "framework/console/sources/con_message_color.hpp"

#include "framework/console/sources/resources/con_resources.hpp"

#include <QtCore/qcoreapplication.h>

#include <iostream>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

int __stdcall
consoleEventHandler( unsigned long _event )
{
	switch ( _event )
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		return false;

	};

	return false;

} // consoleEventHandler

/*---------------------------------------------------------------------------*/

ConsoleWriter::ConsoleWriter( const std::string & _applicationName )
{
	m_inputLocker = [](){};
	m_inputUnlocker = [](){};

	::SetConsoleCtrlHandler( consoleEventHandler, true );
	::SetConsoleTitle( _applicationName.data() );

	m_stdoutHandle = ::GetStdHandle( STD_OUTPUT_HANDLE );

	::COORD size;
	size.X = Resources::Constants::Console::LineLength;
	size.Y = Resources::Constants::Console::LinesCount;

	::SetConsoleScreenBufferSize(
			m_stdoutHandle
		,	size
	);
	::MoveWindow(
			::GetConsoleWindow()
		,	Resources::Constants::Console::TopLeftX
		,	Resources::Constants::Console::TopLeftY
		,	Resources::Constants::Console::Width
		,	Resources::Constants::Console::Height
		,	true
	);

	::CONSOLE_SCREEN_BUFFER_INFO screenBufferInfo;
	::GetConsoleScreenBufferInfo(
			m_stdoutHandle
		,	& screenBufferInfo
	);

	m_background = screenBufferInfo.wAttributes
		& ( BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY );

} // ConsoleWriter::ConsoleWriter

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::run()
{
	while ( isRunning() )
	{
		QCoreApplication::processEvents();

		msleep( Resources::Delays::ProcessEvents );
	}

} // ConsoleWriter::run

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::print(
		MessageLevel::Enum _messageLevel
	,	const std::string & _moduleShortName
	,	const std::string & _messageIdentifier
	,	const std::string & _message
)
{
	m_inputLocker();

	clearLine();

	printIdentifier(
			_moduleShortName
		,	_messageIdentifier
	);

	printMessage(
			_messageLevel
		,	_message
	);

	printSelectedCommand( m_inputHolder );

	m_inputUnlocker();

} // ConsoleWriter::print

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::printSelectedCommand( const std::string & _command )
{
	std::cout << std::flush;

	resetCursorLinePosition();
	setConsoleIdentifierTextColor();

	std::cout << Resources::Constants::Command::Prefix;

	setConsoleDefaultTextColor();

	std::cout << _command;

} // ConsoleWriter::printSelectedCommand

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::onInputChange(
		const std::string & _command
	,	int _cursorLinePosition
)
{
	m_inputHolder = _command;
	printSelectedCommand( m_inputHolder );

} // ConsoleWriter::onInputChange

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::printCommand( const std::string & _command )
{
	m_inputLocker();

	printSelectedCommand( _command );

	std::cout << '\n';

	m_inputUnlocker();

} // ConsoleWriter::printCommand

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::printIdentifier(
		const std::string & _moduleShortName
	,	const std::string & _messageIdentifier
)
{
	std::cout << std::flush;

	setConsoleIdentifierTextColor();

	std::cout
		<<	std::flush
		<<	" "
		<<	_moduleShortName
		<<	"-"
		<<	_messageIdentifier
		<<	" : "
		<<	std::flush
	;

	setConsoleDefaultTextColor();

	std::cout << std::flush;

} // ConsoleWriter::printIdentifier

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::printMessage(
		MessageLevel::Enum _messageLevel
	,	const std::string & _message
)
{
	std::cout << std::flush;

	setConsoleTextColor( _messageLevel );

	std::cout
		<<	std::flush
		<<	_message
		<<	'\n'
		<<	std::flush
	;

	setConsoleDefaultTextColor();

	std::cout << std::flush;

} // ConsoleWriter::printMessage

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::clearLine()
{
	static const std::string emptyLine(
			Resources::Constants::Console::LineLength - 1
		,	' '
	);

	resetCursorLinePosition();

	std::cout << emptyLine;

	resetCursorLinePosition();

} // ConsoleWriter::clearLine

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::resetCursorLinePosition()
{
	::CONSOLE_SCREEN_BUFFER_INFO screenBufferInfo;
	::GetConsoleScreenBufferInfo(
			m_stdoutHandle
		,	& screenBufferInfo
	);

	screenBufferInfo.dwCursorPosition.X = 0;

	::SetConsoleCursorPosition(
			m_stdoutHandle
		,	screenBufferInfo.dwCursorPosition
	);

} // ConsoleWriter::resetCursorLinePosition

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::setConsoleTextColor( MessageLevel::Enum _messageLevel )
{
	::SetConsoleTextAttribute(
			m_stdoutHandle
		,	m_background | MessageColor::getColor( _messageLevel )
	);

} // ConsoleWriter::setConsoleTextColor

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::setConsoleDefaultTextColor()
{
	::SetConsoleTextAttribute(
			m_stdoutHandle
		,	m_background | Resources::MessageColors::Default
	);

} // ConsoleWriter::setConsoleDefaultTextColor

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::setConsoleIdentifierTextColor()
{
	::SetConsoleTextAttribute(
			m_stdoutHandle
		,	m_background | Resources::MessageColors::Identifier
	);

} // ConsoleWriter::setConsoleIdentifierTextColor

/*---------------------------------------------------------------------------*/

void
ConsoleWriter::setInputControls(
		boost::function< void () > _inputLocker
	,	boost::function< void () > _inputUnlocker
)
{
	m_inputLocker = _inputLocker;
	m_inputUnlocker = _inputUnlocker;

} // ConsoleWriter::setInputControls

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample
