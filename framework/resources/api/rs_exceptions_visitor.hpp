// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_EXCEPTIONS_VISITOR_HPP__
#define __AMPLE_RESOURCES_EXCEPTIONS_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class MissingSection;
class MissingResource;

class DuplicateSection;
class DuplicateResource;

class InvalidPropertyValue;

class BrokenAttribute;
class PathSyntaxError;

/*---------------------------------------------------------------------------*/

class ExceptionsVisitor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~ExceptionsVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const MissingSection & /*_exception*/ ) {}

	virtual void visit( const MissingResource & /*_exception*/ ) {}

	virtual void visit( const DuplicateSection & /*_exception*/ ) {}

	virtual void visit( const DuplicateResource & /*_exception*/ ) {}

	virtual void visit( const BrokenAttribute & /*_exception*/ ) {}

	virtual void visit( const PathSyntaxError & /*_exception*/ ) {}

	virtual void visit( const InvalidPropertyValue & /*_exception*/ ) {}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_EXCEPTIONS_VISITOR_HPP__
