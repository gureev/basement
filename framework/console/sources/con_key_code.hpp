// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_KEY_CODE_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_KEY_CODE_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

#include <WinUser.h>

/*---------------------------------------------------------------------------*/

class KeyCode
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Enter				= VK_RETURN
		,	Backspace			= VK_BACK
		,	Tab					= VK_TAB
		,	ArrowUp				= VK_UP
		,	ArrowRight			= VK_RIGHT
		,	ArrowDown			= VK_DOWN
		,	ArrowLeft			= VK_LEFT
	};

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_KEY_CODE_HPP__
