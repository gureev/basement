// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_STRING_HPP__
#define __AMPLE_RESOURCES_STRING_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {

/*---------------------------------------------------------------------------*/

class String
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~String() {}

/*---------------------------------------------------------------------------*/

	virtual const std::string & getSectionName() const = 0;

	virtual const std::string & getIdentifier() const = 0;

	virtual const std::string & getValue() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_STRING_HPP__
