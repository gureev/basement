// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/api/fs_path.hpp"

#include "framework/filesystem/sources/fs_file_handle_impl.hpp"
#include "framework/filesystem/sources/fs_open_mode_converter.hpp"

#include "framework/filesystem/api/fs_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

FileHandle::FileHandle(
		boost::shared_ptr< FileSystem::Path > _path
	,	OpenMode::Enum _openMode
)
	:	m_path( _path )
	,	m_file( _path->toQString() )
{
	// TODO: Check SymLink

	bool result = m_file.open(
		OpenModeConverter::convert( _openMode )
	);

	if ( ! result )
	{
		if ( _openMode == OpenMode::Read )
			EX_THROW_1_ARGUMENT(
					Exceptions::CannotOpenFileForReading
				,	_path->toString()
			);

		EX_THROW_1_ARGUMENT(
				Exceptions::CannotOpenFileForWriting
			,	_path->toString()
		);
	}

} // FileHandle::FileHandle

/*---------------------------------------------------------------------------*/

FileHandle::~FileHandle()
{
	m_file.close();

} // FileHandle::FileHandle

/*---------------------------------------------------------------------------*/

const FileSystem::Path &
FileHandle::getPath() const
{
	EX_DEBUG_ASSERT( m_path );
	return * m_path;

} // FileHandle::getPath

/*---------------------------------------------------------------------------*/

QIODevice &
FileHandle::getDevice()
{
	return m_file;

} // FileHandle::getDevice

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
