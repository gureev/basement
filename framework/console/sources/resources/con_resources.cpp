// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/resources/con_resources.hpp"

#include <windows.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Constants {

/*---------------------------------------------------------------------------*/

namespace Console {

/*---------------------------------------------------------------------------*/

	const unsigned int TopLeftX = 1;

	const unsigned int TopLeftY = 1;

	const unsigned int Width = 1600;

	const unsigned int Height = 600;

	const unsigned int LineLength = 160;

	const unsigned int LinesCount = 10000;

/*---------------------------------------------------------------------------*/

} // namespace Console

/*---------------------------------------------------------------------------*/

namespace Command {

/*---------------------------------------------------------------------------*/

	extern const unsigned int MaxLength = 80;

	extern const char * Prefix = " > ";

/*---------------------------------------------------------------------------*/

} // namespace Command

/*---------------------------------------------------------------------------*/

} // namespace Constants

/*---------------------------------------------------------------------------*/

namespace Strings {

/*---------------------------------------------------------------------------*/

	const char * ApplicationName = "console/strings/application/name";

/*---------------------------------------------------------------------------*/

} // namespace Strings

/*---------------------------------------------------------------------------*/

namespace Delays {

/*---------------------------------------------------------------------------*/

	const unsigned int ReadKey = 50;

	const unsigned int ProcessEvents = 300;

/*---------------------------------------------------------------------------*/

} // namespace Delays

/*---------------------------------------------------------------------------*/

namespace MessageColors {

/*---------------------------------------------------------------------------*/

	const unsigned int Default = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;

	const unsigned int Identifier = Default | FOREGROUND_INTENSITY;

	const unsigned int Error = FOREGROUND_RED | FOREGROUND_INTENSITY;

	const unsigned int Success = FOREGROUND_GREEN | FOREGROUND_INTENSITY;

	const unsigned int Warning = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;

/*---------------------------------------------------------------------------*/

} // namespace MessageColors

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Console
} // namespace Ample
