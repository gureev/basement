// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_TEST_EXCEPTIONS_VISITOR_HPP__
#define __AMPLE_EXCEPTIONS_TEST_EXCEPTIONS_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {
namespace Tests {

/*---------------------------------------------------------------------------*/

class TestException;

/*---------------------------------------------------------------------------*/

class ExceptionsVisitor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~ExceptionsVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const TestException & /*_exception*/ ) {}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_TEST_EXCEPTIONS_VISITOR_HPP__
