// (C) 2013 Ample Test Runner

#ifndef __AMPLE_TOOLS_TEST_RUNNER_ENTRY_POINT_HPP__
#define __AMPLE_TOOLS_TEST_RUNNER_ENTRY_POINT_HPP__

/*---------------------------------------------------------------------------*/

#define TEST_RUNNER_ENTRY_POINT_FUNCTION testRunnerEntryPoint

/*---------------------------------------------------------------------------*/

#define TEST_RUNNER_ENTRY_POINT()											\
extern "C" __declspec( dllexport )											\
int																			\
TEST_RUNNER_ENTRY_POINT_FUNCTION( int _argc, char ** _argv )				\
{																			\
	return ::boost::unit_test::unit_test_main(								\
			& init_unit_test_suite											\
		,	_argc															\
		,	_argv															\
	);																		\
}

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_TOOLS_TEST_RUNNER_ENTRY_POINT_HPP__
