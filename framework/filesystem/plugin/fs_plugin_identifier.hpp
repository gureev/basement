// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_PLUGIN_IDENTIFIER_HPP__
#define __AMPLE_FILESYSTEM_PLUGIN_IDENTIFIER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Plugin {

/*---------------------------------------------------------------------------*/

// {1B83FFA2-3524-4025-A307-90C6A1E22D4C}
const Connector::PluginIdentifier Identifier
( 0x1B83FFA2, 0x3524, 0x4025, 0xA3, 0x7, 0x90, 0xc6, 0xA1, 0xE2, 0x2D, 0x4C );

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_PLUGIN_IDENTIFIER_HPP__
