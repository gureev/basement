// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_DEFAULT_STRINGS_RESTORER_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_DEFAULT_STRINGS_RESTORER_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {

	class Accessor;

namespace Implementation {

/*---------------------------------------------------------------------------*/

class DefaultStringsRestorer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	DefaultStringsRestorer( Accessor & _accessor );

	~DefaultStringsRestorer();

/*---------------------------------------------------------------------------*/

	void run();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void restoreStrings( const char * _fileName );

/*---------------------------------------------------------------------------*/

	Accessor & m_accessor;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_DEFAULT_STRINGS_RESTORER_HPP__
