// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_unix_path_builder.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

bool
UnixPathBuilder::isAbsolute( const std::string & _path )
{
	return isSlash( _path[ 0 ] );

} // UnixPathBuilder::isAbsolute

/*---------------------------------------------------------------------------*/

bool
UnixPathBuilder::isSlash( const char _symbol )
{
	return _symbol == '/';

} // UnixPathBuilder::isSlash

/*---------------------------------------------------------------------------*/

const char
UnixPathBuilder::extractDriveLetter( const std::string & _path )
{
	if ( ! isAbsolute( _path ) )
		return 0;

	return '/';

} // UnixPathBuilder::extractDriveLetter

/*---------------------------------------------------------------------------*/

void
UnixPathBuilder::optimize( std::string & _path )
{
} // UnixPathBuilder::optimize

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
