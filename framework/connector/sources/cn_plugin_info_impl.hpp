// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGIN_INFO_HPP__
#define __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGIN_INFO_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_info.hpp"
#include "framework/connector/api/cn_plugin_load_policy.hpp"

#include <QtCore/qlibrary.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PluginInfo
	:	public Connector::PluginInfo
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PluginInfo(
			const QString & _libraryName
		,	PluginLoadPolicy::Enum _loadPolicy
	);

	/*virtual*/ ~PluginInfo();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const Plugin & getPlugin() const;

	/*virtual*/ PluginLoadPolicy::Enum getLoadPolicy() const;

/*---------------------------------------------------------------------------*/

	void initializePlugin();

	void initializeAccessor();

/*---------------------------------------------------------------------------*/

	bool isInitialized() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	typedef
		Connector::Plugin & ( *PluginFactory )();

	PluginFactory resolvePluginFactory();

/*---------------------------------------------------------------------------*/

	boost::optional< Plugin & > m_plugin;

	QLibrary m_library;

	PluginLoadPolicy::Enum m_loadPolicy;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGIN_INFO_HPP__
