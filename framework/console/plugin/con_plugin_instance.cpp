// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/con_accessor_impl.hpp"

#include "framework/console/sources/resources/con_resources.hpp"

#include "framework/console/plugin/con_plugin_instance.hpp"

#include "framework/resources/api/rs_accessor.hpp"
#include "framework/resources/api/rs_property.hpp"
#include "framework/resources/plugin/rs_plugin_identifier.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Plugin {

/*---------------------------------------------------------------------------*/

CONNECTOR_DECLARE_PLUGIN_FACTORY()

/*---------------------------------------------------------------------------*/

Instance::Instance()
	:	m_accessor()
{
} // Instance::Instance

/*---------------------------------------------------------------------------*/

Instance::~Instance()
{
} // Instance::~Instance

/*---------------------------------------------------------------------------*/

void
Instance::onPluginLoad()
{
	boost::shared_ptr< Ample::Resources::Accessor > resources = getResources();

	m_accessor.reset(
		new Implementation::Accessor(
			resources->getProperty( Resources::Strings::ApplicationName ).asString()
		)
	);

} // Instance::onPluginLoad

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Ample::Resources::Accessor >
Instance::getResources() const
{
	return getPluginAccessor< Ample::Resources::Accessor >(
		Ample::Resources::Plugin::Identifier
	);

} // Instance::getResources

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Connector::Accessor >
Instance::getAccessor() const
{
	return m_accessor;

} // Instance::getAccessor

/*---------------------------------------------------------------------------*/

const Connector::PluginIdentifier &
Instance::getIdentifier() const
{
	return Identifier;

} // Instance::getIdentifier

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace Console
} // namespace Ample
