// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_UTILS_HPP__
#define __AMPLE_CONNECTOR_UTILS_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/lexical_cast.hpp>

#include <string>

#include <Windows.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Utils {

/*---------------------------------------------------------------------------*/

template< typename _Number >
std::string
convertNumber( const _Number & _number )
{
	return boost::lexical_cast< std::string >( _number );

} // convertNumber

/*---------------------------------------------------------------------------*/

int
getPluginsPath(
		char * _path
	,	int _maxLength
)
{
	return ::GetModuleFileName(
			0
		,	_path
		,	_maxLength
	);

} // getPluginsPath

/*---------------------------------------------------------------------------*/

} // namespace Utils
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_UTILS_HPP__
