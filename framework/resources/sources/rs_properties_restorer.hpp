// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTIES_RESTORER_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTIES_RESTORER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/sources/rs_property_sections_container.hpp"
#include "framework/resources/sources/rs_property_section.hpp"

#include "framework/tools/common_headers/containers/hash_set.hpp"

#include "framework/serializator/api/sr_xml_restorer.hpp"

/*---------------------------------------------------------------------------*/

class QIODevice;

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertiesRestorer
	:	public Serializator::XmlRestorer
{

/*---------------------------------------------------------------------------*/

	typedef
		Serializator::XmlRestorer
		BaseRestorer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertiesRestorer(
			PropertySectionsContainer & _sectionsContainer
		,	QIODevice & _stream
	);

	/*virtual*/ ~PropertiesRestorer();

/*---------------------------------------------------------------------------*/

	void run();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void restoreProperty(
			PropertySection & _section
		,	QDomElement & _propertyTag
	);

/*---------------------------------------------------------------------------*/

	void restoreSection(
			PropertySection & _section
		,	QDomElement & _sectionTag
	);

/*---------------------------------------------------------------------------*/

	template<
			typename _SectionsContainer
		,	typename _SectionTag
	>
	void restoreNestedSections(
			_SectionsContainer & _section
		,	_SectionTag & _sectionTag
	);

/*---------------------------------------------------------------------------*/

	PropertySectionsContainer & m_sectionsContainer;

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::HashSet< std::string >
		RestoredResources;

	RestoredResources m_restoredProperties;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTIES_RESTORER_HPP__
