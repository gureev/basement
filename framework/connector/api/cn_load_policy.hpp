// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_LOAD_POLICY_HPP__
#define __AMPLE_CONNECTOR_LOAD_POLICY_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

class LoadPolicy
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	enum Enum
	{
			Invalid

		,	Lazy
		,	Instant
	};

/*---------------------------------------------------------------------------*/

	static Enum fromString( const QString & _string )
	{
		if ( _string.length() != 1 )
			return Invalid;

		switch ( _string[ 0 ].toAscii() )
		{
		case '0':			return Instant;
		case '1':			return Lazy;

		default:			assert( !"Unknown value" );
							break;
		}

		return Invalid;
	}

/*---------------------------------------------------------------------------*/

	static const char * toString( Enum _enum )
	{
		switch ( _enum )
		{
		case Instant:		return "0";
		case Lazy:			return "1";

		default:			assert( !"Unknown value" );
		}

		return "";
	}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_LOAD_POLICY_HPP__
