// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_EXCEPTIONS_VISITOR_HPP__
#define __AMPLE_SERIALIZATOR_EXCEPTIONS_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class SaveFailure;
class RestoreFailure;
class VersionMismatch;
class RootElementMismatch;

/*---------------------------------------------------------------------------*/

class ExceptionsVisitor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~ExceptionsVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const SaveFailure & /*_exception*/ ) {}

	virtual void visit( const RestoreFailure & /*_exception*/ ) {}

	virtual void visit( const VersionMismatch & /*_exception*/ ) {}

	virtual void visit( const RootElementMismatch & /*_exception*/ ) {}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_EXCEPTIONS_VISITOR_HPP__
