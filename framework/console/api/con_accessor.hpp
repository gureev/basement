// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_ACCESSOR_HPP__
#define __AMPLE_CONSOLE_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_message_level.hpp"

#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace Resources
	{
		class String;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {

/*---------------------------------------------------------------------------*/

class MessageStatistic;

/*---------------------------------------------------------------------------*/

class Accessor
	:	public Connector::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual void print(
			MessageLevel::Enum _messageLevel
		,	const Resources::String & _string
	) = 0;

	virtual void print(
			MessageLevel::Enum _messageLevel
		,	const std::string & _message
	) = 0;

/*---------------------------------------------------------------------------*/

	virtual const MessageStatistic & getMessageStatistic() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_ACCESSOR_HPP__
