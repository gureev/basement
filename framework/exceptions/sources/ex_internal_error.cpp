// (C) 2013 Ample Exceptions

#include "framework/exceptions/sources/ph/ex_ph.hpp"

#include "framework/exceptions/api/ex_internal_error.hpp"

#include "framework/exceptions/sources/resources/ex_resources.hpp"

#include <boost/format.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

InternalError::InternalError(
		const std::string & _throwFileName
	,	int _throwLineNumber
)
	:	m_throwFileName( _throwFileName )
	,	m_throwLineNumber( _throwLineNumber )
{
} // InternalError::InternalError

/*---------------------------------------------------------------------------*/

const std::string &
InternalError::throwFileName() const
{
	return m_throwFileName;

} // InternalError::throwFileName

/*---------------------------------------------------------------------------*/

const int &
InternalError::throwLineNumber() const
{
	return m_throwLineNumber;

} // InternalError::throwLineNumber

/*---------------------------------------------------------------------------*/

const char *
InternalError::what() const
{
	m_what = (
		boost::format( Resources::Messages::InternalError )
		%	throwFileName()
		%	throwLineNumber()
	).str();

	return m_what.data();

} // InternalError::what

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample
