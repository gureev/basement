// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_TEST_FIXTURE_HPP__
#define __AMPLE_FILESYSTEM_TEST_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Tests {

/*---------------------------------------------------------------------------*/

class Fixture
{

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_TEST_FIXTURE_HPP__
