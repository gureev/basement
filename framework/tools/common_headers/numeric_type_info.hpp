// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_NUMERIC_TYPE_INFO_HPP__
#define __AMPLE_COMMON_HEADERS_NUMERIC_TYPE_INFO_HPP__

/*----------------------------------------------------------------------------*/

#include <stdint.h>

/*----------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {

/*----------------------------------------------------------------------------*/

template< typename _NumericType >
class NumericTypeInfo;

/*----------------------------------------------------------------------------*/

#define DECLARE_TYPE_INFO( _TYPE, _MAX, _MIN )									\
template<>																		\
class NumericTypeInfo< _TYPE >													\
{																				\
																				\
public:																			\
																				\
	typedef																		\
		_TYPE																	\
		Type;																	\
																				\
	static const Type MaximumValue = _MAX;										\
																				\
	static const Type MinimumValue = _MIN;										\
																				\
};

/*----------------------------------------------------------------------------*/

DECLARE_TYPE_INFO( int, INT32_MAX, INT32_MIN )
DECLARE_TYPE_INFO( unsigned int, UINT32_MAX, 0 )
DECLARE_TYPE_INFO( unsigned short, USHRT_MAX, 0 )
DECLARE_TYPE_INFO( unsigned char, UCHAR_MAX, 0 )

/*----------------------------------------------------------------------------*/

#undef DECLARE_TYPE_INFO

/*----------------------------------------------------------------------------*/

} // namespace CommonHeaders
} // namespace Ample

/*----------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_NUMERIC_TYPE_INFO_HPP__
