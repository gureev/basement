// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/api/cn_exceptions.hpp"

#include "framework/connector/sources/cn_plugin_info_impl.hpp"
#include "framework/connector/sources/cn_plugins_container_impl.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

PluginsContainer &
PluginsContainer::getInstance()
{
	return Implementation::PluginsContainer::getInstance();
}

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PluginsContainer &
PluginsContainer::getInstance()
{
	static boost::scoped_ptr< PluginsContainer > pluginsContainer( new PluginsContainer() );

	return * pluginsContainer;
}

/*---------------------------------------------------------------------------*/

void
PluginsContainer::addPlugin(
		const QString & _name
	,	const PluginIdentifier & _identifier
	,	PluginLoadPolicy::Enum _loadPolicy
)
{
	if ( m_plugins.find( _identifier ) != m_plugins.end() )
		EX_THROW_1_ARGUMENT(
				Exceptions::DuplicatePluginIdentifier
			,	_name
		)

	boost::shared_ptr< PluginInfo > pluginInfo(
		new PluginInfo(
				_name
			,	_loadPolicy
		)
	);

	m_plugins[ _identifier ] = pluginInfo;
}

/*---------------------------------------------------------------------------*/

void
PluginsContainer::loadInstantPlugins()
{
	Plugins::iterator it = m_plugins.begin();
	Plugins::iterator itEnd = m_plugins.end();

	CommonHeaders::Containers::Vector< Plugins::ValueType > instantPlugins;
	instantPlugins.reserve( m_plugins.size() );

	for ( ; it != itEnd; ++it )
	{
		Plugins::ValueType pluginInfo = it->second;

		if ( pluginInfo->getLoadPolicy() == PluginLoadPolicy::Instant )
		{
			instantPlugins.push_back( pluginInfo );
			pluginInfo->initializePlugin();
		}
	}

	int nInstantPlugins = instantPlugins.size();
	for ( int i = 0; i < nInstantPlugins; ++i )
		instantPlugins[ i ]->initializeAccessor();
}

/*---------------------------------------------------------------------------*/

int
PluginsContainer::getPluginsCount() const
{
	return m_plugins.size();

} // PluginsContainer::getPluginsCount

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Connector::PluginInfo >
PluginsContainer::findPlugin( const PluginIdentifier & _pluginIdentifier ) const
{
	Plugins::const_iterator it = m_plugins.find( _pluginIdentifier );

	return it != m_plugins.end()
		?	boost::static_pointer_cast< Connector::PluginInfo >( it->second )
		:	boost::shared_ptr< Connector::PluginInfo >()
	;

} // PluginsContainer::findPlugin

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample
