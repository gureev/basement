// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_PATH_HASHER_HPP__
#define __AMPLE_FILESYSTEM_PATH_HASHER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_path.hpp"

#include "framework/tools/common_headers/hash_combiner.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {

/*---------------------------------------------------------------------------*/

class PathHasher
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	CommonHeaders::HashCombiner::HashResult
	operator () ( const boost::shared_ptr< Path > & _path ) const
	{
		return ( *this )( *_path );

	} // operator ()

/*---------------------------------------------------------------------------*/

	CommonHeaders::HashCombiner::HashResult
	operator () ( const Path & _path ) const
	{
		CommonHeaders::HashCombiner hashCombiner;
		boost::hash< std::string > pathHasher;
		boost::hash< char > diskDriveHasher;

		int nComponents = _path.getComponentsCount();
		for ( int i = 0; i < nComponents; ++i )
			hashCombiner += pathHasher( _path.getComponent( i ) );

		if ( _path.isAbsolute() )
			hashCombiner += diskDriveHasher( _path.getDriveLetter() );

		return hashCombiner.getResult();

	} // operator ()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_PATH_HASHER_HPP__
