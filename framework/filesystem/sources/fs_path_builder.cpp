// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/filesystem/sources/fs_path_builder.hpp"
#include "framework/filesystem/sources/fs_windows_path_builder.hpp"
#include "framework/filesystem/sources/fs_unix_path_builder.hpp"
#include "framework/filesystem/sources/fs_base_relative_path.hpp"
#include "framework/filesystem/sources/fs_base_absolute_path.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

boost::shared_ptr< FileSystem::Path >
PathBuilder::build( const std::string & _path )
{
	EX_DEBUG_ASSERT( ! _path.empty() );

	std::string strPath = _path;

	optimize( strPath );

	const char driveLetter = extractDriveLetter( strPath );
	bool absolute = driveLetter;

	int nComponents = std::count(
			strPath.begin()
		,	strPath.end()
		,	'/'
	);

	boost::shared_ptr< FileSystem::Path > fsPath;

	if ( absolute )
		fsPath = boost::shared_ptr< FileSystem::Path >(
			new BaseAbsolutePath(
					driveLetter
				,	nComponents
			)
		);
	else
		fsPath = boost::shared_ptr< FileSystem::Path >(
			new BaseRelativePath( nComponents )
		);

	int startPosition = absolute
		?	strPath.find( '/' ) + 1
		:	0
	;

	extractPathComponents(
			fsPath
		,	strPath
		,	startPosition
	);

	return fsPath;

} // PathBuilder::build

/*---------------------------------------------------------------------------*/

void
PathBuilder::extractPathComponents(
		boost::shared_ptr< FileSystem::Path > & _path
	,	const std::string & _stringPath
	,	int _startPosition
)
{
	int length = static_cast< int >( _stringPath.length() );

	int componentStartPosition = _startPosition;
	int componentEndPosition = componentStartPosition;

	for ( ; componentEndPosition < length; ++componentEndPosition )
		if ( isSlash( _stringPath[ componentEndPosition ] ) )
		{
			std::string component = _stringPath.substr(
					componentStartPosition
				,	componentEndPosition - componentStartPosition
			);
			componentStartPosition = componentEndPosition + 1;
			* _path /= component;
		}

	if ( componentEndPosition > componentStartPosition )
		*_path /= _stringPath.substr( componentStartPosition );

} // PathBuilder::extractPathComponents

/*---------------------------------------------------------------------------*/

bool
PathBuilder::isAbsolute( const std::string & _path )
{
	return
#ifdef _WIN32
		WindowsPathBuilder::isAbsolute
#else
		UnixPathBuilder::isAbsolute
#endif
	( _path );

} // PathBuilder::isAbsolute

/*---------------------------------------------------------------------------*/

bool
PathBuilder::isSlash( const char _symbol )
{
	return
#ifdef _WIN32
		WindowsPathBuilder::isSlash
#else
		UnixPathBuilder::isSlash
#endif
	( _symbol );

} // PathBuilder::isAbsolute

/*---------------------------------------------------------------------------*/

const char
PathBuilder::extractDriveLetter( const std::string & _path )
{
	return
#ifdef _WIN32
		WindowsPathBuilder::extractDriveLetter
#else
		UnixPathBuilder::extractDriveLetter
#endif
	( _path );

} // PathBuilder::extractDriveLetter

/*---------------------------------------------------------------------------*/

void
PathBuilder::optimize( std::string & _path )
{
	return
#ifdef _WIN32
		WindowsPathBuilder::optimize
#else
		UnixPathBuilder::optimize
#endif
	( _path );

} // PathBuilder::optimize

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
