// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_STRINGS_RESTORER_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_STRINGS_RESTORER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/sources/rs_string_sections_container.hpp"
#include "framework/resources/sources/rs_string_section.hpp"

#include "framework/tools/common_headers/containers/hash_set.hpp"

#include "framework/serializator/api/sr_xml_restorer.hpp"

/*---------------------------------------------------------------------------*/

class QIODevice;

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class StringsRestorer
	:	public Serializator::XmlRestorer
{

/*---------------------------------------------------------------------------*/

	typedef
		Serializator::XmlRestorer
		BaseRestorer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	StringsRestorer(
			StringSectionsContainer & _sectionsContainer
		,	QIODevice & _stream
	);

	/*virtual*/ ~StringsRestorer();

/*---------------------------------------------------------------------------*/

	void run();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void restoreString(
			StringSection & _section
		,	QDomElement & _stringTag
	);

/*---------------------------------------------------------------------------*/

	void restoreSection(
			StringSection & _section
		,	QDomElement & _sectionTag
	);

/*---------------------------------------------------------------------------*/

	template<
			typename _SectionsContainer
		,	typename _SectionTag
	>
	void restoreNestedSections(
			_SectionsContainer & _section
		,	_SectionTag & _sectionTag
	);

/*---------------------------------------------------------------------------*/

	StringSectionsContainer & m_sectionsContainer;

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::HashSet< std::string >
		RestoredResources;

	RestoredResources m_restoredStrings;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_STRINGS_RESTORER_HPP__
