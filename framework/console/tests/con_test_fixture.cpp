// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/tests/con_test_fixture.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Tests {

/*---------------------------------------------------------------------------*/

Fixture::Fixture()
	:	m_accessor( "CONSOLE: TEST" )
{
} // Fixture::Fixture

/*---------------------------------------------------------------------------*/

Accessor &
Fixture::getAccessor()
{
	return m_accessor;

} // Fixture::getAccessor

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Console
} // namespace Ample
