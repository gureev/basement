// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"
#include "framework/resources/api/rs_property.hpp"

#include "framework/resources/sources/rs_property_validator_impl.hpp"
#include "framework/resources/sources/rs_path.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PropertyValidator::PropertyValidator()
	:	m_propertyType( PropertyType::Undefined )
{
} // PropertyValidator::PropertyValidator

/*---------------------------------------------------------------------------*/

PropertyValidator::~PropertyValidator()
{
} // PropertyValidator::~PropertyValidator

/*---------------------------------------------------------------------------*/

void
PropertyValidator::validate(
		const Path & _propertyPath
	,	const Resources::Property & _property
) const
{
	switch ( m_propertyType )
	{
	case PropertyType::String:
		return;

	case PropertyType::Bool:
		validatePropertyType(
				_propertyPath
			,	_property.asBool()
		);
		break;

	case PropertyType::Int:
		validatePropertyType(
				_propertyPath
			,	_property.asInt()
		);
		break;

	case PropertyType::Double:
		validatePropertyType(
				_propertyPath
			,	_property.asDouble()
		);
		break;

	case PropertyType::Undefined:
		if ( ! _property.asDouble() )
			return;

	}

	double propertyValue = * _property.asDouble();

	int nConstraints = m_constraints.size();

	bool valid = true;
	for ( int i = 0; i < nConstraints; ++i )
	{
		valid = propertyValue >= m_constraints[ i ].first
			&&	propertyValue <= m_constraints[ i ].second
		;

		if ( valid )
			return;
	}

	if ( ! valid )
		EX_THROW_1_ARGUMENT(
				Exceptions::InvalidPropertyValue
			,	_propertyPath.getResource()
		);

} // PropertyValidator::validate

/*---------------------------------------------------------------------------*/

template< typename _PropertyType >
void
PropertyValidator::validatePropertyType(
		const Path & _propertyPath
	,	boost::optional< _PropertyType > _propertyValue
) const
{
	if ( ! _propertyValue )
		EX_THROW_1_ARGUMENT(
				Exceptions::InvalidPropertyValue
			,	_propertyPath.getResource()
		);

} // PropertyValidator::validatePropertyType

/*---------------------------------------------------------------------------*/

Resources::PropertyValidator &
PropertyValidator::expectType( PropertyType::Enum _type )
{
	m_propertyType = _type;
	return * this;

} // PropertyValidator::expectType

/*---------------------------------------------------------------------------*/

Resources::PropertyValidator &
PropertyValidator::expectValue(
		int _lowerBound
	,	int _upperBound
)
{
	Constraints::value_type constraint(
			_lowerBound
		,	_upperBound
	);
	m_constraints.push_back( constraint );

	return * this;

} // PropertyValidator::expectValue

/*---------------------------------------------------------------------------*/

Resources::PropertyValidator &
PropertyValidator::expectValue(
		double _lowerBound
	,	double _upperBound
)
{
	Constraints::value_type constraint(
			_lowerBound
		,	_upperBound
	);
	m_constraints.push_back( constraint );

	return * this;

} // PropertyValidator::expectValue

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
