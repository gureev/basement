// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_EXCEPTIONS_VISITOR_HPP__
#define __AMPLE_CONNECTOR_EXCEPTIONS_VISITOR_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class BrokenPlugin;
class BrokenAttribute;
class DuplicatePluginIdentifier;
class DuplicatePluginName;
class PluginLibraryNotFound;
class PluginInformationNotFound;
class CannotLoadPluginLibrary;
class CannotGetInformationAboutSystem;

/*---------------------------------------------------------------------------*/

class ExceptionsVisitor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~ExceptionsVisitor() {}

/*---------------------------------------------------------------------------*/

	virtual void visit( const BrokenPlugin & /*_exception*/ ) {}

	virtual void visit( const BrokenAttribute & /*_exception*/ ) {}

	virtual void visit( const DuplicatePluginIdentifier & /*_exception*/ ) {}

	virtual void visit( const DuplicatePluginName & /*_exception*/ ) {}

	virtual void visit( const PluginLibraryNotFound & /*_exception*/ ) {}

	virtual void visit( const PluginInformationNotFound & /*_exception*/ ) {}

	virtual void visit( const CannotLoadPluginLibrary & /*_exception*/ ) {}

	virtual void visit( const CannotGetInformationAboutSystem & /*_exception*/ ) {}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_EXCEPTIONS_VISITOR_HPP__
