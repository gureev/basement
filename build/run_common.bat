@echo off

set configuration=%1
set action=%2
set platform=%3
set environment=%4
set logfolder=%platform%\%configuration%

MD %logfolder%

call "%VS100COMNTOOLS%..\..\vc\vcvarsall.bat" %environment%

msbuild ../framework.sln /p:Configuration=%configuration%;Platform=%platform% /t:%action% /verbosity:minimal /consoleloggerparameters:NoItemAndPropertyList /flp1:logfile=./%logfolder%/build_errors.log;errorsonly /flp2:logfile=./%logfolder%/build_warnings.log;warningsonly /flp3:logfile=./%logfolder%/build.log;verbosity:minimal;PerformanceSummary
