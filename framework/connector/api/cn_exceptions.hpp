// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_EXCEPTIONS_HPP__
#define __AMPLE_CONNECTOR_EXCEPTIONS_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/connector/api/cn_exceptions_visitor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

EX_DECLARE_BASE_EXCEPTION(
		BaseException
	,	ExceptionsVisitor
);

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	BrokenPlugin
	,	( QString )( pluginName )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	BrokenAttribute
	,	( int )( pluginLineNumber )
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	DuplicatePluginIdentifier
	,	( QString )( pluginName )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	DuplicatePluginName
	,	( QString )( pluginName )
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	PluginLibraryNotFound
	,	( QString )( pluginName )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	PluginInformationNotFound
	,	( QString )( pluginName )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	CannotLoadPluginLibrary
	,	( QString )( pluginName )
)

EX_DECLARE_0_ARGUMENT(
		BaseException
	,	CannotGetInformationAboutSystem
)

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_EXCEPTIONS_HPP__
