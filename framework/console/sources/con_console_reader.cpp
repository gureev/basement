// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/con_console_reader.hpp"
#include "framework/console/sources/con_console_manager.hpp"
#include "framework/console/sources/con_key_code.hpp"

#include "framework/console/sources/resources/con_resources.hpp"

#include <QtCore/qcoreapplication.h>

#include <conio.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

ConsoleReader::ConsoleReader(
		ConsoleManager & _consoleManager
	,	ConsoleWriter & _consoleWriter
)
	:	m_enabled( false )
{
	m_input.reserve( Resources::Constants::Console::LineLength );

	m_stdoutHandle = GetStdHandle( STD_OUTPUT_HANDLE );
	m_stdinHandle = GetStdHandle( STD_INPUT_HANDLE );

	QObject::connect(
			this
		,	SIGNAL( onEnterKeyEvent( std::string ) )
		,	& _consoleManager
		,	SLOT( onEnterKey( std::string ) )
	);

	QObject::connect(
			this
		,	SIGNAL( onInputChangeEvent( const std::string &, int ) )
		,	& _consoleWriter
		,	SLOT( onInputChange( const std::string &, int ) )
	);

	unsigned long consoleMode = 0;
	::GetConsoleMode(
			m_stdinHandle
		,	& consoleMode
	);

	consoleMode |= ENABLE_LINE_INPUT;
	consoleMode |= ENABLE_ECHO_INPUT;
	consoleMode |= ENABLE_PROCESSED_INPUT;

	consoleMode &= ( ~ENABLE_QUICK_EDIT_MODE );

	::SetConsoleMode(
			m_stdinHandle
		,	consoleMode
	);

} // ConsoleReader::ConsoleReader

/*---------------------------------------------------------------------------*/

void
ConsoleReader::run()
{
	while ( isRunning() )
	{
		QCoreApplication::processEvents();

		if ( !m_enabled )
		{
			msleep( Resources::Delays::ProcessEvents );
			continue;
		}

		readKey();
		msleep( Resources::Delays::ReadKey );
	}

} // ConsoleReader::run

/*---------------------------------------------------------------------------*/

void
ConsoleReader::readKey()
{
	int inputChar = _getch();

	if ( ! m_enabled )
		return;

	switch ( inputChar )
	{
	case KeyCode::Enter:
		onEnterKey();
		break;

	case KeyCode::Backspace:
		onBackspaceKey();
		break;

	case KeyCode::Tab:
		onTabKey();
		break;

	case KeyCode::ArrowUp:
		onArrowUpKey();
		break;

	case KeyCode::ArrowRight:
		onArrowRightKey();
		break;

	case KeyCode::ArrowDown:
		onArrowDownKey();
		break;

	case KeyCode::ArrowLeft:
		onArrowLeftKey();
		break;

	}

	if ( QChar( inputChar ).isPrint() )
		onPrintableChar( inputChar );

} // ConsoleReader::readKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onEnterKey()
{
	if ( m_input.empty() )
		return;

	disableInput();

	emit onEnterKeyEvent( m_input );

	m_input.clear();

	emit onInputChangeEvent(
			m_input
		,	0 // TODO: Cursor position ( [SPIKE]: Need? )
	);

} // ConsoleReader::onEnterKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onBackspaceKey()
{
	// TODO: Optimize: must be only one event

	if ( m_input.empty() )
		return;

	( * m_input.rbegin() ) = ' ';

	emit onInputChangeEvent(
			m_input
		,	0
	);

	m_input.pop_back();

	emit onInputChangeEvent(
			m_input
		,	0
	);

} // ConsoleReader::onBackspaceKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onTabKey()
{
} // ConsoleReader::onTabKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onArrowUpKey()
{
} // ConsoleReader::onArrowUpKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onArrowRightKey()
{
} // ConsoleReader::onArrowRightKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onArrowDownKey()
{
} // ConsoleReader::onArrowDownKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onArrowLeftKey()
{
} // ConsoleReader::onArrowLeftKey

/*---------------------------------------------------------------------------*/

void
ConsoleReader::onPrintableChar( unsigned char _char )
{
	if ( m_input.length() >= Resources::Constants::Command::MaxLength )
		return;

	m_input += _char;

	emit onInputChangeEvent(
			m_input
		,	0 // TODO: Cursor position ( [SPIKE]: Need? )
	);

} // ConsoleReader::onPrintableChar

/*---------------------------------------------------------------------------*/

void
ConsoleReader::disableInput()
{
	m_enabled = false;

} // ConsoleReader::disableInput

/*---------------------------------------------------------------------------*/

void
ConsoleReader::enableInput()
{
	m_enabled = true;

} // ConsoleReader::enableInput

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample
