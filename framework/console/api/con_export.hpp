// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_EXPORT_HPP__
#define __AMPLE_CONSOLE_EXPORT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {

/*---------------------------------------------------------------------------*/

#ifdef _PLUGIN_CONSOLE_
	#define CONSOLE_API __declspec( dllexport )
#else
	#define CONSOLE_API __declspec( dllimport )
#endif

/*---------------------------------------------------------------------------*/

} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_EXPORT_HPP__
