/****************************************************************************
** Meta object code from reading C++ file 'con_console_writer.hpp'
**
** Created: Mon 22. Apr 14:12:03 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "con_console_writer.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'con_console_writer.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Ample__Console__Implementation__ConsoleWriter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
     106,   47,   46,   46, 0x0a,
     177,  168,   46,   46, 0x0a,
     232,  203,   46,   46, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Ample__Console__Implementation__ConsoleWriter[] = {
    "Ample::Console::Implementation::ConsoleWriter\0"
    "\0_messageLevel,_moduleShortName,_messageIdentifier,_message\0"
    "print(MessageLevel::Enum,std::string,std::string,std::string)\0"
    "_command\0printCommand(std::string)\0"
    "_command,_cursorLinePosition\0"
    "onInputChange(std::string,int)\0"
};

void Ample::Console::Implementation::ConsoleWriter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ConsoleWriter *_t = static_cast<ConsoleWriter *>(_o);
        switch (_id) {
        case 0: _t->print((*reinterpret_cast< MessageLevel::Enum(*)>(_a[1])),(*reinterpret_cast< const std::string(*)>(_a[2])),(*reinterpret_cast< const std::string(*)>(_a[3])),(*reinterpret_cast< const std::string(*)>(_a[4]))); break;
        case 1: _t->printCommand((*reinterpret_cast< const std::string(*)>(_a[1]))); break;
        case 2: _t->onInputChange((*reinterpret_cast< const std::string(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Ample::Console::Implementation::ConsoleWriter::staticMetaObjectExtraData = {
    0,  qt_static_metacall
};

const QMetaObject Ample::Console::Implementation::ConsoleWriter::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_Ample__Console__Implementation__ConsoleWriter,
      qt_meta_data_Ample__Console__Implementation__ConsoleWriter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Ample::Console::Implementation::ConsoleWriter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Ample::Console::Implementation::ConsoleWriter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Ample::Console::Implementation::ConsoleWriter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Ample__Console__Implementation__ConsoleWriter))
        return static_cast<void*>(const_cast< ConsoleWriter*>(this));
    return QThread::qt_metacast(_clname);
}

int Ample::Console::Implementation::ConsoleWriter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
