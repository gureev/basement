// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTIONS_CONTAINER_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTIONS_CONTAINER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/tools/common_headers/containers/hash_map.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertySection;

/*---------------------------------------------------------------------------*/

class PropertySectionsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertySectionsContainer();

	~PropertySectionsContainer();

/*---------------------------------------------------------------------------*/

	PropertySection & addSection( const std::string & _name );

/*---------------------------------------------------------------------------*/

	const PropertySection & getSection( const std::string & _name ) const;

	int getSectionsCount() const;

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::HashMap<
				std::string
			,	boost::shared_ptr< PropertySection >
		>
		Sections;

	Sections m_sections;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTIONS_CONTAINER_HPP__
