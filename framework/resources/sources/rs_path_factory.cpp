// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_path.hpp"
#include "framework/resources/sources/rs_path_factory.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

const Path &
PathFactory::createPath( const char * _path ) const
{
	Paths::const_iterator it = m_paths.find( _path );

	if ( it != m_paths.end() )
		return * it->second;

	boost::shared_ptr< Path > resourcePath(
		new Path( _path )
	);
	m_paths[ _path ] = resourcePath;

	return * resourcePath;

} // PathFactory::createPath

/*---------------------------------------------------------------------------*/

int
PathFactory::getPathsCount() const
{
	return m_paths.size();

} // PathFactory::getPathsCount

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
