// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_property_impl.hpp"

#include "framework/tools/common_headers/numeric_type_info.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Property::Property( const std::string & _value )
	:	m_asString( _value )
{
	static boost::regex boolRegExp( "^(true|false)$" );
	static boost::regex numberRegExp( "^(-?)([0-9]+)(\\.([0-9]+))?$" );

	if ( boost::regex_match( m_asString, boolRegExp ) )
		m_asNumber = m_asString[ 0 ] == 't'
			?	1.0
			:	0.0
		;
	else if ( boost::regex_match( m_asString, numberRegExp ) )
		m_asNumber = boost::lexical_cast< double >( m_asString );

} // Property::Property

/*---------------------------------------------------------------------------*/

Property::~Property()
{
} // Property::~Property

/*---------------------------------------------------------------------------*/

const std::string &
Property::asString() const
{
	return m_asString;

} // Property::asString

/*---------------------------------------------------------------------------*/

boost::optional< int >
Property::asInt() const
{
	if ( ! m_asNumber )
		return boost::optional< int >();

	int asInt = static_cast< int >( * m_asNumber );
	if (	asInt == CommonHeaders::NumericTypeInfo< int >::MinimumValue
		&&	* m_asNumber > 0
	)
		asInt = CommonHeaders::NumericTypeInfo< int >::MaximumValue;

	return asInt;

} // Property::asInt

/*---------------------------------------------------------------------------*/

boost::optional< bool >
Property::asBool() const
{
	if ( m_asNumber )
		return static_cast< bool >( * m_asNumber );

	return boost::optional< bool >();

} // Property::asBool

/*---------------------------------------------------------------------------*/

boost::optional< double >
Property::asDouble() const
{
	return m_asNumber;

} // Property::asDouble

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
