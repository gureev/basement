// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/sources/cn_plugin_editor.hpp"

#include "framework/connector/sources/resources/cn_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PluginEditor::PluginEditor()
{
} // PluginEditor::PluginEditor

/*---------------------------------------------------------------------------*/

PluginEditor &
PluginEditor::begin()
{
	return * this;

} // PluginEditor::begin

/*---------------------------------------------------------------------------*/

PluginEditor &
PluginEditor::end()
{
	m_body = Resources::Templates::PluginsInfoFile.arg( m_body );

	return * this;

} // PluginEditor::end

/*---------------------------------------------------------------------------*/

PluginEditor &
PluginEditor::plugin(
		const char * _name
	,	const char * _identifier
	,	const char * _loadPolicy
)
{
	m_body += Resources::Templates::Plugin.arg(
			_name
		,	_identifier
		,	_loadPolicy
	);

	return * this;

} // PluginEditor::plugin

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample
