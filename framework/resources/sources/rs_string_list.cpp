// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_string_list.hpp"
#include "framework/resources/sources/rs_string_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

StringList::StringList()
{
} // StringList::StringList

/*---------------------------------------------------------------------------*/

StringList::~StringList()
{
} // StringList::~StringList

/*---------------------------------------------------------------------------*/

void
StringList::add(
		const std::string & _sectionName
	,	const std::string & _identifier
	,	const std::string & _value
)
{
	boost::shared_ptr< Implementation::String > string(
		new Implementation::String(
				_sectionName
			,	_identifier
			,	_value
		)
	);

	m_strings.push_back( string );

} // PropertySection::add

/*---------------------------------------------------------------------------*/

const Resources::String &
StringList::get( AccessType::Enum _accessType ) const
{
	EX_DEBUG_ASSERT( ! m_strings.empty() );

	return _accessType == AccessType::FirstAvailable
		?	* m_strings.back()
		:	* m_strings.front()
	;

} // StringList::get

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
