// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_path.hpp"
#include "framework/resources/sources/rs_path_factory.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Path Factory:
			1) Empty
			2) One path
			3) Two different paths
			4) Two equal paths

*/

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE( Suite_PathFactory )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PathFactory_1_Empty )
{
	/*---------- Setup ----------*/

	Implementation::PathFactory factory;

	/*-------- Test actions -----*/

	BOOST_REQUIRE_EQUAL(
			factory.getPathsCount()
		,	0
	);

} // PathFactory_1_Empty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PathFactory_2_OnePath )
{
	/*---------- Setup ----------*/

	Implementation::PathFactory factory;

	factory.createPath( "test/path" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_EQUAL(
			factory.getPathsCount()
		,	1
	);

} // PathFactory_2_OnePath

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PathFactory_3_TwoDifferentPaths )
{
	/*---------- Setup ----------*/

	Implementation::PathFactory factory;

	factory.createPath( "test/path/1" );
	factory.createPath( "test/path/2" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_EQUAL(
			factory.getPathsCount()
		,	2
	);

} // PathFactory_3_TwoDifferentPaths

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( PathFactory_4_TwoEqualPaths )
{
	/*---------- Setup ----------*/

	Implementation::PathFactory factory;

	factory.createPath( "test/path/1" );
	factory.createPath( "test/path/1" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE_EQUAL(
			factory.getPathsCount()
		,	1
	);

} // PathFactory_4_TwoEqualPaths

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_PathFactory

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
