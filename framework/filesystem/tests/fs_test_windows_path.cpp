// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/api/fs_path.hpp"

#include "framework/filesystem/tests/fs_test_path_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		1) Absolute
			1.1) Drive path
			1.2) Drive path case sensetive
			1.3) Path 1 level
			1.4) Path 2 levels
			1.5) Without drive letter
			1.6) Without drive letter 1 level
			1.7) Without drive letter 2 levels
			1.8) Build String
			1.9) Build QString
		2) Relative
			2.1) Path 1 level
			2.2) Path 2 levels
			2.3) Local reference path 1 level
			2.4) Local reference path 2 levels
			2.5) Upper 1 reference path 1 level
			2.6) Upper 2 reference path 1 level
			2.7) Upper 1 reference path 2 levels
			2.8) Upper 2 reference path 2 levels
			2.9) Build String
			2.10) Build QString
*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_WindowsPath, PathFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_1_Absolute_DrivePath )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "c:/" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), 'c' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 0 );

} // WindowsPath_1_1_Absolute_DrivePath

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_2_Absolute_DrivePathCaseSensetive )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "C:/" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), 'C' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 0 );

} // WindowsPath_1_2_Absolute_DrivePathCaseSensetive

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_3_Absolute_Path1Level )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "C:/Documents and Settings" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), 'C' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 1 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "Documents and Settings" );

} // WindowsPath_1_3_Absolute_Path1Level

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_4_Absolute_Path2Levels )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "C:/Documents and Settings/Administrator" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), 'C' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 2 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "Documents and Settings" );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "Administrator" );

} // WindowsPath_1_4_Absolute_Path2Levels

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_5_Absolute_WithoutDriveLetter )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "/" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), '/' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 0 );

} // WindowsPath_1_5_Absolute_WithoutDriveLetter

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_6_Absolute_WithoutDriveLetter1Level )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "/home" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), '/' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 1 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "home" );

} // WindowsPath_1_6_Absolute_WithoutDriveLetter1Level

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_7_Absolute_WithoutDriveLetter2Levels )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "/home/aldec" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isAbsolute() );
	BOOST_REQUIRE_EQUAL( path->getDriveLetter(), '/' );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 2 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "home" );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "aldec" );

} // WindowsPath_1_7_Absolute_WithoutDriveLetter2Levels

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_8_Absolute_BuildString )
{
	/*---------- Setup ----------*/

	std::string testPath = "C:/Documents and Settings/Administrator";
	boost::shared_ptr< Path > path = m_fileSystem.createPath( testPath );

	/*-------- Test actions -----*/

	std::string resultPath = path->toString();
	BOOST_REQUIRE_EQUAL( testPath, resultPath );

} // WindowsPath_1_8_Absolute_BuildString

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_1_9_Absolute_BuildQString )
{
	/*---------- Setup ----------*/

	QString testPath = "C:/Documents and Settings/Administrator";
	boost::shared_ptr< Path > path = m_fileSystem.createPath( testPath.toAscii().data() );

	/*-------- Test actions -----*/

	QString resultPath = path->toQString();
	BOOST_REQUIRE_EQUAL(
			testPath.toAscii().data()
		,	resultPath.toAscii().data()
	);

} // WindowsPath_1_9_Absolute_BuildQString

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_1_Relative_Path1Level )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "Windows" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 1 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "Windows" );

} // WindowsPath_2_1_Relative_Path1Level

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_2_Relative_Path2Levels )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "Windows/system32" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 2 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "Windows" );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "system32" );

} // WindowsPath_2_2_Relative_Path2Levels

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_3_Relative_LocalReferencePath1Level )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "./Windows" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 2 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "Windows" );

} // WindowsPath_2_3_Relative_LocalReferencePath1Level

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_4_Relative_LocalReferencePath2Levels )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "./Windows/system32" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 3 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), "." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "Windows" );
	BOOST_REQUIRE_EQUAL( path->getComponent( 2 ), "system32" );

} // WindowsPath_2_4_Relative_LocalReferencePath2Levels

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_5_Relative_Upper1ReferencePath1Level )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "../Windows" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 2 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), ".." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "Windows" );

} // WindowsPath_2_5_Relative_Upper1ReferencePath1Level

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_6_Relative_Upper2ReferencePath1Level )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "../../Windows" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 3 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), ".." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), ".." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 2 ), "Windows" );

} // WindowsPath_2_6_Relative_Upper2ReferencePath1Level

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_7_Relative_Upper1ReferencePath2Levels )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "../Windows/system32" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 3 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), ".." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), "Windows" );
	BOOST_REQUIRE_EQUAL( path->getComponent( 2 ), "system32" );

} // WindowsPath_2_7_Relative_Upper1ReferencePath2Levels

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_8_Relative_Upper2ReferencePath2Levels )
{
	/*---------- Setup ----------*/

	boost::shared_ptr< Path > path = m_fileSystem.createPath( "../../Windows/system32" );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( path->isRelative() );
	BOOST_REQUIRE_EQUAL( path->getComponentsCount(), 4 );
	BOOST_REQUIRE_EQUAL( path->getComponent( 0 ), ".." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 1 ), ".." );
	BOOST_REQUIRE_EQUAL( path->getComponent( 2 ), "Windows" );
	BOOST_REQUIRE_EQUAL( path->getComponent( 3 ), "system32" );

} // WindowsPath_2_8_Relative_Upper2ReferencePath2Levels

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_9_Relative_BuildString )
{
	/*---------- Setup ----------*/

	std::string testPath = "../../Windows/system32";
	boost::shared_ptr< Path > path = m_fileSystem.createPath( testPath );

	/*-------- Test actions -----*/

	std::string resultPath = path->toString();
	BOOST_REQUIRE_EQUAL( testPath, resultPath );

} // WindowsPath_2_9_Relative_BuildString

/*----------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( WindowsPath_2_10_Relative_BuildQString )
{
	/*---------- Setup ----------*/

	QString testPath = "../../Windows/system32";
	boost::shared_ptr< Path > path = m_fileSystem.createPath( testPath.toAscii().data() );

	/*-------- Test actions -----*/

	QString resultPath = path->toQString();
	BOOST_REQUIRE_EQUAL(
			testPath.toAscii().data()
		,	resultPath.toAscii().data()
	);

} // WindowsPath_2_10_Relative_BuildQString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_WindowsPath

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace FileSystem
} // namespace Ample
