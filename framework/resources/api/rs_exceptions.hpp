// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_EXCEPTIONS_HPP__
#define __AMPLE_RESOURCES_EXCEPTIONS_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/resources/api/rs_exceptions_visitor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

EX_DECLARE_BASE_EXCEPTION(
		BaseException
	,	ExceptionsVisitor
);

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	MissingSection
	,	( std::string )( sectionName )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	MissingResource
	,	( std::string )( resourceName )
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	DuplicateSection
	,	( std::string )( sectionName )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	DuplicateResource
	,	( std::string )( resourceName )
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	InvalidPropertyValue
	,	( std::string )( propertyName )
)

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	BrokenAttribute
	,	( int )( attributeLineNumber )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	PathSyntaxError
	,	( std::string )( resourcePath )
)

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_EXCEPTIONS_HPP__
