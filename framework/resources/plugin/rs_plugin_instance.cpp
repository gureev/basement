// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_default_strings_restorer.hpp"
#include "framework/resources/sources/rs_accessor_impl.hpp"

#include "framework/resources/plugin/rs_plugin_instance.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Plugin {

/*---------------------------------------------------------------------------*/

CONNECTOR_DECLARE_PLUGIN_FACTORY()

/*---------------------------------------------------------------------------*/

Instance::Instance()
	:	m_accessor()
{
} // Instance::Instance

/*---------------------------------------------------------------------------*/

Instance::~Instance()
{
} // Instance::~Instance

/*---------------------------------------------------------------------------*/

void
Instance::onPluginLoad()
{
	m_accessor.reset( new Implementation::Accessor() );

	Implementation::DefaultStringsRestorer restorer( * m_accessor );
	restorer.run();

} // Instance::onPluginLoad

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Connector::Accessor >
Instance::getAccessor() const
{
	return m_accessor;

} // Instance::getAccessor

/*---------------------------------------------------------------------------*/

const Connector::PluginIdentifier &
Instance::getIdentifier() const
{
	return Identifier;

} // Instance::getIdentifier

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace Resources
} // namespace Ample
