// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_TEST_PROPERTY_FIXTURE_HPP__
#define __AMPLE_RESOURCES_TEST_PROPERTY_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/sources/rs_property_section.hpp"
#include "framework/resources/sources/rs_property_sections_container.hpp"
#include "framework/resources/sources/rs_property_section_editor.hpp"
#include "framework/resources/sources/rs_accessor_impl.hpp"

#include <QtCore/qbuffer.h>
#include <QtCore/qbytearray.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

class PropertyFixture
	:	public Implementation::PropertySectionEditor

{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertyFixture();

/*---------------------------------------------------------------------------*/

	const Implementation::PropertySectionsContainer & runTest();

/*---------------------------------------------------------------------------*/

	bool compare(
			double _left
		,	double _right
	) const;

/*---------------------------------------------------------------------------*/

	void clear();

/*---------------------------------------------------------------------------*/

	Accessor & getAccessor();

	QIODevice & openStream();

/*---------------------------------------------------------------------------*/

protected:

/*---------------------------------------------------------------------------*/

	Implementation::PropertySectionsContainer m_sections;

	Implementation::Accessor m_accessor;

	QBuffer m_buffer;

	QByteArray m_byteData;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_TEST_PROPERTY_FIXTURE_HPP__
