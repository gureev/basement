// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_accessor_impl.hpp"
#include "framework/filesystem/sources/fs_path_builder.hpp"
#include "framework/filesystem/sources/fs_base_path.hpp"
#include "framework/filesystem/sources/fs_file_handle_impl.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

#include <QtCore/qdir.h>
#include <QtCore/qfileinfo.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

Accessor::Accessor()
{
} // Accessor::Accessor

/*---------------------------------------------------------------------------*/

Accessor::~Accessor()
{
} // Accessor::~Accessor

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Path >
Accessor::createPath( const std::string & _path ) const
{
	return PathBuilder::build( _path );

} // Accessor::createPath

/*---------------------------------------------------------------------------*/

void
Accessor::setCurrentPath( const Path & _path )
{
	EX_INTERNAL_ERROR

} // Accessor::setCurrentPath

/*---------------------------------------------------------------------------*/

const Path &
Accessor::getCurrentPath() const
{
	EX_INTERNAL_ERROR

} // Accessor::getCurrentPath

/*---------------------------------------------------------------------------*/

bool
Accessor::exists( const Path & _path ) const
{
	return QFileInfo( _path.toQString() ).exists();

} // Accessor::exists

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Path >
Accessor::getAbsolutePath( const Path & _path ) const
{
	return getAbsolutePath(
			_path
		,	getCurrentPath()
	);

} // Accessor::getAbsolutePath

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Path >
Accessor::getAbsolutePath(
		const Path & _path
	,	const Path & _pathFrom
) const
{
	EX_INTERNAL_ERROR

} // Accessor::getAbsolutePath

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Path >
Accessor::getRelativePath( const Path & _path ) const
{
	return getRelativePath(
			_path
		,	getCurrentPath()
	);

} // Accessor::getRelativePath

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Path >
Accessor::getRelativePath(
		const Path & _path
	,	const Path & _pathTo
) const
{
	EX_INTERNAL_ERROR

} // Accessor::getRelativePath

/*---------------------------------------------------------------------------*/

bool
Accessor::canConvertToRelativePath(
		const Path & _path
	,	const Path & _pathTo
) const
{
	// TODO: Need to count common components

	EX_INTERNAL_ERROR

} // Accessor::canConvertToRelativePath

/*---------------------------------------------------------------------------*/

bool
Accessor::canRead( const Path & _path ) const
{
	return QFileInfo( _path.toQString() ).isReadable();

} // Accessor::canRead

/*---------------------------------------------------------------------------*/

bool
Accessor::canWrite( const Path & _path ) const
{
	return QFileInfo( _path.toQString() ).isWritable();

} // Accessor::canWrite

/*---------------------------------------------------------------------------*/

bool
Accessor::canDelete( const Path & _path ) const
{
	QFileInfo info( _path.toQString() );

	if ( ! info.isFile() )
		return true;

	return info.isWritable();

} // Accessor::canDelete

/*---------------------------------------------------------------------------*/

bool
Accessor::isFile( const Path & _path ) const
{
	return QFileInfo( _path.toQString() ).isFile();

} // Accessor::isFile

/*---------------------------------------------------------------------------*/

bool
Accessor::isFolder( const Path & _path ) const
{
	return QFileInfo( _path.toQString() ).isDir();

} // Accessor::isFolder

/*---------------------------------------------------------------------------*/

bool
Accessor::isLink( const Path & _path ) const
{
	return QFileInfo( _path.toQString() ).isSymLink();

} // Accessor::isLink

/*---------------------------------------------------------------------------*/

void
Accessor::remove( const Path & _path )
{
	EX_INTERNAL_ERROR

} // Accessor::remove

/*---------------------------------------------------------------------------*/

void
Accessor::rename(
		const Path & _path
	,	const std::string & _newName
)
{
	EX_INTERNAL_ERROR

} // Accessor::rename

/*---------------------------------------------------------------------------*/

void
Accessor::move(
		const Path & _path
	,	const Path & _targetFolderPath
)
{
	EX_INTERNAL_ERROR

} // Accessor::move

/*---------------------------------------------------------------------------*/

void
Accessor::copy(
		const Path & _path
	,	const Path & _targetFolderPath
)
{
	EX_INTERNAL_ERROR

} // Accessor::copy

/*---------------------------------------------------------------------------*/

void
Accessor::makeReadable( const Path & _path )
{
	QString path = _path.toQString();

	setPermissions(
			path
		,	static_cast< QFile::Permissions >(
				QFile( path ).permissions() | readablePermissionMask()
			)
	);

} // Accessor::makeReadable

/*---------------------------------------------------------------------------*/

void
Accessor::makeUnreadable( const Path & _path )
{
	QString path = _path.toQString();

	setPermissions(
			path
		,	static_cast< QFile::Permissions >(
				QFile( path ).permissions() & ( ~ readablePermissionMask() )
			)
	);

} // Accessor::makeUnreadable

/*---------------------------------------------------------------------------*/

void
Accessor::makeWritable( const Path & _path )
{
	QString path = _path.toQString();

	setPermissions(
			path
		,	static_cast< QFile::Permissions >(
				QFile( path ).permissions() | writablePermissionMask()
			)
	);

} // Accessor::makeWritable

/*---------------------------------------------------------------------------*/

void
Accessor::makeUnwritable( const Path & _path )
{
	QString path = _path.toQString();

	setPermissions(
			path
		,	static_cast< QFile::Permissions >(
				QFile( path ).permissions() & ( ~ writablePermissionMask() )
			)
	);

} // Accessor::makeUnwritable

/*---------------------------------------------------------------------------*/

void
Accessor::setPermissions(
		const QString & _path
	,	QFile::Permissions _permissions
)
{
	// TODO: Need to check existence?

	/*bool result = */QFile::setPermissions(
			_path
		,	_permissions
	);

	// TODO: Need to check result?

} // Accessor::setPermissions

/*---------------------------------------------------------------------------*/

int
Accessor::readablePermissionMask() const
{
	int mask = 0;

	mask |= QFile::ReadOwner;
	mask |= QFile::ReadUser;
	mask |= QFile::ReadGroup;
	mask |= QFile::ReadOther;

	return mask;

} // Accessor::readablePermissionMask

/*---------------------------------------------------------------------------*/

int
Accessor::writablePermissionMask() const
{
	int mask = 0;

	mask |= QFile::WriteOwner;
	mask |= QFile::WriteUser;
	mask |= QFile::WriteGroup;
	mask |= QFile::WriteOther;

	return mask;

} // Accessor::writablePermissionMask

/*---------------------------------------------------------------------------*/

boost::shared_ptr< FileSystem::FileHandle >
Accessor::openFile(
		boost::shared_ptr< FileSystem::Path > _path
	,	OpenMode::Enum _openMode
)
{
	return boost::shared_ptr< FileSystem::FileHandle >(
		new Implementation::FileHandle(
				_path
			,	_openMode
		)
	);

} // Accessor::openFile

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
