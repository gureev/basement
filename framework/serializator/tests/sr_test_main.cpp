// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

/*---------------------------------------------------------------------------*/

#define BOOST_TEST_MODULE Serializer

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

TEST_RUNNER_ENTRY_POINT()

/*---------------------------------------------------------------------------*/
