// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_string_sections_container.hpp"

#include "framework/resources/tests/rs_test_string_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Strings:
			1) Section tag
				1.1) Missing name attribute (Not implemented)
				1.2) MIssing short name attribute (Not implemented)
				1.3) Empty name attribute
				1.4) Empty short name attribute
			2) String tag
				2.1) Missing name attribute (Not implemented)
				2.2) Missing value attribute (Not implemented)
				2.3) Missing identifier attribute (Not implemented)
				2.4) Empty name attribute
				2.5) Empty value attribute
				2.6) Empty identifier attribute
			3) One section
				3.1) Without strings
				3.2) One string
				3.3) Two strings
			4) One nested section
				4.1) Without strings
				4.2) One string
				4.3) Two strings
			5) Two nested section
				5.1) Without strings
				5.2) One string only in first section
				5.3) One string only in second section
				5.4) One string in each section
				5.5) Two strings only in first section
				5.6) Two strings only in second section
				5.7) Two strings in each section
			6) Mixed section
				6.1) One string and one nested section
					6.1.1) String before section
					6.1.2) String after section
				6.2) One string and two nested sections
					6.2.1) String before sections
					6.2.2) String after sections
					6.2.3) String between sections
				6.3) One string and one string in nested section
				6.4) One string and two strings in nested section
				6.5) Two strings and one nested section
					6.4.1) Strings before section
					6.4.2) Strings after section
				6.6) Two strings and two nested sections
					6.2.1) Strings before sections
					6.2.2) Strings after sections
					6.2.3) Strings between sections
				6.7) Two strings and one string in nested section
				6.8) Two strings and two strings in nested section
			7) Resolve
				7.1) Section and nested section has equal names
				7.2) String and nested string has equal names
				7.3) Two sections has equal names
				7.4) Two strings has equal names
				7.5) String and section has equal names
					7.5.1) First string
					7.5.2) First section
				7.6) Two sections has equal short names
				7.7) Section and nested section has equal short names
				7.8) Section name and short name are equal
				7.9) String name and identifier are equal
				7.10) String name and value are equal
				7.11) String identifier and value are equal
			8) String overlap
				8.1) One string
				8.2) Two strings in same section
				8.3) Two strings in different sections
				8.4) Two strings in different level sections
				8.5) New string
				8.6) New section
				8.7) New nested section

*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Strings, StringFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_1_3_SectionTag_EmptyNameAttribute )
{
	/*---------- Setup ----------*/

	section( "", "test_short" ).end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Strings_1_3_SectionTag_EmptyNameAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_1_4_SectionTag_EmptyShortNameAttribute )
{
	/*---------- Setup ----------*/

	section( "test_name", "" ).end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_name" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_1_4_SectionTag_EmptyShortNameAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_2_4_StringTag_EmptyNameAttribute )
{
	/*---------- Setup ----------*/

	section( "test_section", "test_short" )
		.string( "", "test_identifier", "test_value" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Strings_2_4_StringTag_EmptyNameAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_2_5_StringTag_EmptyValueAttribute )
{
	/*---------- Setup ----------*/

	section( "test_section", "test_short" )
		.string( "test_string", "test_identifier", "" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Strings_2_5_StringTag_EmptyValueAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_2_6_StringTag_EmptyIdentifierAttribute )
{
	/*---------- Setup ----------*/

	section( "test_section", "test_short" )
		.string( "test_string", "", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value = section.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_2_6_StringTag_EmptyIdentifierAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_3_2_OneSection_OneString )
{
	/*---------- Setup ----------*/

	section( "test_section", "test_short" )
		.string( "test_string", "test_identifier", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value = section.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_3_2_OneSection_OneString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_3_3_OneSection_TwoStrings )
{
	/*---------- Setup ----------*/

	section( "test_section", "test_short" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = section.getResource( "test_string_1" );
		const String & value2 = section.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_3_3_OneSection_TwoStrings

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_4_1_OneNestedSection_WithoutStrings )
{
	/*---------- Setup ----------*/

	section( "test_section_1", "test_short_1" )
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_4_1_OneNestedSection_WithoutStrings

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_4_2_OneNestedSection_OneString )
{
	/*---------- Setup ----------*/

	section( "test_section_1", "test_short_1" )
		.section( "test_section_2", "test_short_2" )
			.string( "test_string", "test_identifier", "test_value" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value = section2.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_4_2_OneNestedSection_OneString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_4_3_OneNestedSection_TwoStrings )
{
	/*---------- Setup ----------*/

	section( "test_section_1", "test_short_1" )
		.section( "test_section_2", "test_short_2" )
			.string( "test_string_1", "test_identifier_1", "test_value_1" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = section2.getResource( "test_string_1" );
		const String & value2 = section2.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short_2" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_4_3_OneNestedSection_TwoStrings

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_1_TwoNestedSection_WithoutStrings )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_1_TwoNestedSection_WithoutStrings

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_2_TwoNestedSection_OneStringOnlyInFirstSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" )
			.string( "test_string", "test_identifier", "test_value" )
		.end()
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value = section1.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "test_short_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_2_TwoNestedSection_OneStringOnlyInFirstSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_3_TwoNestedSection_OneStringOnlyInSecondSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" )
			.string( "test_string", "test_identifier", "test_value" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value = section2.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_3_TwoNestedSection_OneStringOnlyInSecondSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_4_TwoNestedSection_OneStringInEachSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" )
			.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.end()
		.section( "test_section_2", "test_short_2" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = section1.getResource( "test_string_1" );
		const String & value2 = section2.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_4_TwoNestedSection_OneStringInEachSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_5_TwoNestedSection_TwoStringsOnlyInFirstSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" )
			.string( "test_string_1", "test_identifier_1", "test_value_1" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.end()
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = section1.getResource( "test_string_1" );
		const String & value2 = section1.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_5_TwoNestedSection_TwoStringsOnlyInFirstSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_6_TwoNestedSection_TwoStringsOnlyInSecondSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" )
			.string( "test_string_1", "test_identifier_1", "test_value_1" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = section2.getResource( "test_string_1" );
		const String & value2 = section2.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short_2" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_6_TwoNestedSection_TwoStringsOnlyInSecondSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_5_7_TwoNestedSection_TwoStringsInEachSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" )
			.string( "test_string_1", "test_identifier_1", "test_value_1" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.end()
		.section( "test_section_2", "test_short_2" )
			.string( "test_string_3", "test_identifier_3", "test_value_3" )
			.string( "test_string_4", "test_identifier_4", "test_value_4" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = section1.getResource( "test_string_1" );
		const String & value2 = section1.getResource( "test_string_2" );
		const String & value3 = section2.getResource( "test_string_3" );
		const String & value4 = section2.getResource( "test_string_4" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "test_value_3" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "test_identifier_3" );
		BOOST_REQUIRE_EQUAL( value3.getSectionName(), "test_short_2" );

		BOOST_REQUIRE_EQUAL( value4.getValue(), "test_value_4" );
		BOOST_REQUIRE_EQUAL( value4.getIdentifier(), "test_identifier_4" );
		BOOST_REQUIRE_EQUAL( value4.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_5_7_TwoNestedSection_TwoStringsInEachSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_1_1_MixedSection_OneStringAndOneNestedSection_StringBeforeSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_identifier", "test_value" )
		.section( "test_section", "test_short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value = parent.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_1_1_MixedSection_OneStringAndOneNestedSection_StringBeforeSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_1_2_MixedSection_OneStringAndOneNestedSection_StringAfterSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section", "test_short" ).end()
		.string( "test_string", "test_identifier", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value = parent.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_1_2_MixedSection_OneStringAndOneNestedSection_StringAfterSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_2_1_MixedSection_OneStringAndTwoNestedSections_StringBeforeSections )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_identifier", "test_value" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value = parent.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_2_1_MixedSection_OneStringAndTwoNestedSections_StringBeforeSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_2_2_MixedSection_OneStringAndTwoNestedSections_StringAfterSections )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" ).end()
		.string( "test_string", "test_identifier", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value = parent.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_2_2_MixedSection_OneStringAndTwoNestedSections_StringAfterSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_2_3_MixedSection_OneStringAndTwoNestedSections_StringBetweenSections )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.string( "test_string", "test_identifier", "test_value" )
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value = parent.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_2_3_MixedSection_OneStringAndTwoNestedSections_StringBetweenSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_3_MixedSection_OneStringAndOneStringInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.section( "test_section", "test_short" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = section.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_3_MixedSection_OneStringAndOneStringInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_4_MixedSection_OneStringAndTwoStringsInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.section( "test_section", "test_short" )
			.string( "test_string_2", "test_identifier_2", "test_value_2" )
			.string( "test_string_3", "test_identifier_3", "test_value_3" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = section.getResource( "test_string_2" );
		const String & value3 = section.getResource( "test_string_3" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "test_value_3" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "test_identifier_3" );
		BOOST_REQUIRE_EQUAL( value3.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_4_MixedSection_OneStringAndTwoStringsInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_5_1_MixedSection_TwoStringsAndOneNestedSection_StringsBeforeSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.section( "test_section", "test_short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_5_1_MixedSection_TwoStringsAndOneNestedSection_StringsBeforeSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_5_2_MixedSection_TwoStringsAndOneNestedSection_StringsAfterSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section", "test_short" ).end()
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_5_2_MixedSection_TwoStringsAndOneNestedSection_StringsAfterSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_6_1_MixedSection_TwoStringsAndTwoNestedSections_StringsBeforeSections )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_6_1_MixedSection_TwoStringsAndTwoNestedSections_StringsBeforeSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_6_2_MixedSection_TwoStringsAndTwoNestedSections_StringsAfterSections )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.section( "test_section_2", "test_short_2" ).end()
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_6_2_MixedSection_TwoStringsAndTwoNestedSections_StringsAfterSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_6_3_MixedSection_TwoStringsAndTwoNestedSections_StringsBetweenSections )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short_1" ).end()
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.section( "test_section_2", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_6_3_MixedSection_TwoStringsAndTwoNestedSections_StringsBetweenSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_7_MixedSection_TwoStringsAndOneStringInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.section( "test_section", "test_short" )
			.string( "test_string_3", "test_identifier_3", "test_value_3" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );
		const String & value3 = section.getResource( "test_string_3" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "test_value_3" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "test_identifier_3" );
		BOOST_REQUIRE_EQUAL( value3.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_7_MixedSection_TwoStringsAndOneStringInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_6_8_MixedSection_TwoStringsAndTwoStringsInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier_1", "test_value_1" )
		.string( "test_string_2", "test_identifier_2", "test_value_2" )
		.section( "test_section", "test_short" )
			.string( "test_string_3", "test_identifier_3", "test_value_3" )
			.string( "test_string_4", "test_identifier_4", "test_value_4" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test_short" );

		const String & value1 = parent.getResource( "test_string_1" );
		const String & value2 = parent.getResource( "test_string_2" );
		const String & value3 = section.getResource( "test_string_3" );
		const String & value4 = section.getResource( "test_string_4" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "test_value_3" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "test_identifier_3" );
		BOOST_REQUIRE_EQUAL( value3.getSectionName(), "test_short" );

		BOOST_REQUIRE_EQUAL( value4.getValue(), "test_value_4" );
		BOOST_REQUIRE_EQUAL( value4.getIdentifier(), "test_identifier_4" );
		BOOST_REQUIRE_EQUAL( value4.getSectionName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_6_8_MixedSection_TwoStringsAndTwoStringsInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_1_Resolve_SectionAndNestedSectionHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "test_section", "test_short_1" )
		.section( "test_section", "test_short_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		auto & section2 = section1.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_1_Resolve_SectionAndNestedSectionHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_2_Resolve_StringAndNestedStringHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "test_section_1", "test_short_1" )
		.string( "test_string", "test_identifier_1", "test_value_1" )
		.section( "test_section_2", "test_short_2" )
			.string( "test_string", "test_identifier_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short_1" );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short_2" );

		const String & value1 = section1.getResource( "test_string" );
		const String & value2 = section2.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier_1" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "test_short_1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier_2" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "test_short_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_2_Resolve_StringAndNestedStringHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_3_Resolve_TwoSectionsHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section", "test_short_1" ).end()
		.section( "test_section", "test_short_2" ).end()
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateSection
	);

} // Strings_7_3_Resolve_TwoSectionsHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_4_Resolve_TwoStringsHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_identifier_1", "test_value_1" )
		.string( "test_string", "test_identifier_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateResource
	);

} // Strings_7_4_Resolve_TwoStringsHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_5_1_Resolve_StringAndSectionHasEqualNames_FirstString )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_resource", "test_identifier_1", "test_value_1" )
		.section( "test_resource", "test_short_resource" ).end()
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateResource
	);

} // Strings_7_5_1_Resolve_StringAndSectionHasEqualNames_FirstString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_5_2_Resolve_StringAndSectionHasEqualNames_FirstSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_resource", "test_short_resource" ).end()
		.string( "test_resource", "test_identifier_1", "test_value_1" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateResource
	);

} // Strings_7_5_2_Resolve_StringAndSectionHasEqualNames_FirstSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_6_Resolve_TwoSectionsHasEqualShortNames )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "test_short" ).end()
		.section( "test_section_2", "test_short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_6_Resolve_TwoSectionsHasEqualShortNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_7_Resolve_SectionAndNestedSectionHasEqualShortNames )
{
	/*---------- Setup ----------*/

	section( "test_section_1", "test_short" )
		.section( "test_section_2", "test_short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_short" );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test_short" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_7_Resolve_SectionAndNestedSectionHasEqualShortNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_8_Resolve_SectionNameAndShortNameAreEqual )
{
	/*---------- Setup ----------*/

	section( "test_section_resource", "test_section_resource" ).end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_resource" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test_section_resource" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_8_Resolve_SectionNameAndShortNameAreEqual

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_9_Resolve_StringNameAndIdentifierAreEqual )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_string", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value = section.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_value" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_string" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_9_Resolve_StringNameAndIdentifierAreEqual

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_10_Resolve_StringNameAndValueAreEqual )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_identifier", "test_string" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value = section.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_string" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_10_Resolve_StringNameAndValueAreEqual

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_7_10_Resolve_StringIdentifierAndValueAreEqual )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_identifier", "test_identifier" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value = section.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_7_10_Resolve_StringIdentifierAndValueAreEqual

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_1_StringOverlap_OneString )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string", "test_identifier", "test_identifier" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value = section.getResource( "test_string" );

		BOOST_REQUIRE_EQUAL( value.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_1_StringOverlap_OneString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_2_StringOverlap_TwoStringsInSameSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.string( "test_string_1", "test_identifier", "test_identifier" )
		.string( "test_string_2", "test_identifier", "test_identifier" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value1 = section.getResource( "test_string_1" );
		const String & value2 = section.getResource( "test_string_1" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_2_StringOverlap_TwoStringsInSameSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_3_StringOverlap_TwoStringsInDifferentSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "short_parent" )
			.string( "test_string_1", "test_identifier", "test_identifier" )
		.end()
		.section( "test_section_2", "short_parent" )
			.string( "test_string_2", "test_identifier", "test_identifier" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		const String & value1 = section1.getResource( "test_string_1" );
		const String & value2 = section2.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_3_StringOverlap_TwoStringsInDifferentSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_4_StringOverlap_TwoStringsInDifferentLevelSection )
{
	/*---------- Setup ----------*/

	section( "parent", "short_parent" )
		.section( "test_section_1", "short_parent" )
			.string( "test_string_1", "test_identifier", "test_identifier" )
			.section( "test_section_2", "short_parent" )
				.string( "test_string_2", "test_identifier", "test_identifier" )
			.end()
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "short_parent" );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "short_parent" );

		const String & value1 = section1.getResource( "test_string_1" );
		const String & value2 = section2.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_4_StringOverlap_TwoStringsInDifferentLevelSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_5_StringOverlap_NewString )
{
	/*---------- Setup ----------*/

	section( "section", "short_parent" )
		.string( "test_string_1", "test_identifier", "test_identifier" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value = section.getResource( "test_string_1" );

		BOOST_REQUIRE_EQUAL( value.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "section", "short_parent" )
		.string( "test_string_2", "test_identifier", "test_identifier" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "short_parent" );

		const String & value1 = section.getResource( "test_string_1" );
		const String & value2 = section.getResource( "test_string_2" );

		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value1.getSectionName(), "short_parent" );

		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getValue(), "test_identifier" );
		BOOST_REQUIRE_EQUAL( value2.getSectionName(), "short_parent" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_5_StringOverlap_NewString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_6_StringOverlap_NewSection )
{
	/*---------- Setup ----------*/

	section( "section", "short" )
		.section( "nested_1", "short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "section", "short" )
		.section( "nested_2", "short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );
		auto & nestedSection2 = section.getSection( "nested_2" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( nestedSection2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_6_StringOverlap_NewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Strings_8_7_StringOverlap_NewNestedSection )
{
	/*---------- Setup ----------*/

	section( "section", "short" )
		.section( "nested_1", "short" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "section", "short" )
		.section( "nested_1", "short" )
			.section( "nested_2", "short" ).end()
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );
		auto & nestedSection2 = nestedSection1.getSection( "nested_2" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( nestedSection2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Strings_8_7_StringOverlap_NewNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Strings

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
