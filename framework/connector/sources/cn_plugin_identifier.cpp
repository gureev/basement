// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/api/cn_plugin_identifier.hpp"

#include <QtCore/qregexp.h>

#include <numeric>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

PluginIdentifier::PluginIdentifier(
		const MajorType _major
	,	const MinorType _minor0
	,	const MinorType _minor1
	,	const SubMinorType _subMinor0
	,	const SubMinorType _subMinor1
	,	const SubMinorType _subMinor2
	,	const SubMinorType _subMinor3
	,	const SubMinorType _subMinor4
	,	const SubMinorType _subMinor5
	,	const SubMinorType _subMinor6
	,	const SubMinorType _subMinor7
)
	:	m_major( _major )
{
	m_minor[ 0 ] = _minor0;
	m_minor[ 1 ] = _minor1;

	m_subMinor[ 0 ] = _subMinor0;
	m_subMinor[ 1 ] = _subMinor1;
	m_subMinor[ 2 ] = _subMinor2;
	m_subMinor[ 3 ] = _subMinor3;
	m_subMinor[ 4 ] = _subMinor4;
	m_subMinor[ 5 ] = _subMinor5;
	m_subMinor[ 6 ] = _subMinor6;
	m_subMinor[ 7 ] = _subMinor7;

} // PluginIdentifier::PluginIdentifier

/*---------------------------------------------------------------------------*/

PluginIdentifier::PluginIdentifier()
	:	m_major( 0 )
{
	m_minor.fill( 0 );
	m_subMinor.fill( 0 );

} // PluginIdentifier::PluginIdentifier

/*---------------------------------------------------------------------------*/

PluginIdentifier::~PluginIdentifier()
{
} // PluginIdentifier::~PluginIdentifier

/*---------------------------------------------------------------------------*/

bool
PluginIdentifier::equals( const PluginIdentifier & _pluginIdentifier ) const
{
	return	m_major == _pluginIdentifier.m_major
		&&	m_minor == _pluginIdentifier.m_minor
		&&	m_subMinor == _pluginIdentifier.m_subMinor
	;

} // PluginIdentifier::equals

/*---------------------------------------------------------------------------*/

bool
PluginIdentifier::isValid() const
{
	return	m_major
		+	std::accumulate(
					m_minor.begin()
				,	m_minor.end()
				,	0
			)
		+	std::accumulate(
					m_subMinor.begin()
				,	m_subMinor.end()
				,	0
			)
	;

} // PluginIdentifier::isValid

/*---------------------------------------------------------------------------*/

PluginIdentifier::MajorType
PluginIdentifier::getMajorValue() const
{
	return m_major;

} // PluginIdentifier::getMajorValue

/*---------------------------------------------------------------------------*/

PluginIdentifier::MinorType
PluginIdentifier::getMinorValue( int _index ) const
{
	return m_minor[ _index ];

} // PluginIdentifier::getMinorValue

/*---------------------------------------------------------------------------*/

PluginIdentifier::SubMinorType
PluginIdentifier::getSubMinorValue( int _index ) const
{
	return m_subMinor[ _index ];

} // PluginIdentifier::getSubMinorValue

/*---------------------------------------------------------------------------*/

std::string
PluginIdentifier::toString() const
{
	return toQString().toAscii().data();

} // PluginIdentifier::toString

/*---------------------------------------------------------------------------*/

int
PluginIdentifier::getMinorValuesCount() const
{
	return static_cast< int >( m_minor.size() );

} // PluginIdentifier::getMinorValuesCount

/*---------------------------------------------------------------------------*/

int
PluginIdentifier::getSubMinorValuesCount() const
{
	return static_cast< int >( m_subMinor.size() );

} // PluginIdentifier::getSubMinorValuesCount

/*---------------------------------------------------------------------------*/

QString
PluginIdentifier::toQString() const
{
	QString identifierMask( "%1-%2-%3-%4%5-%6%7%8%9%10%11" );

	identifierMask = identifierMask.arg( getMajorValue(), 8, 16, QChar( '0' ) );

	identifierMask = identifierMask.arg( getMinorValue( 0 ), 4, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getMinorValue( 1 ), 4, 16, QChar( '0' ) );

	identifierMask = identifierMask.arg( getSubMinorValue( 0 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 1 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 2 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 3 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 4 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 5 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 6 ), 2, 16, QChar( '0' ) );
	identifierMask = identifierMask.arg( getSubMinorValue( 7 ), 2, 16, QChar( '0' ) );

	identifierMask = identifierMask.toUpper();

	return identifierMask;

} // PluginIdentifier::toQString

/*---------------------------------------------------------------------------*/

PluginIdentifier
PluginIdentifier::fromString( const QString & _identifier )
{
	static QRegExp rule(
		"^"
		"([0-9A-Fa-f]{8})"
		"-"
		"([0-9A-Fa-f]{4})"
		"-"
		"([0-9A-Fa-f]{4})"
		"-"
		"([0-9A-Fa-f]{2})"
		"([0-9A-Fa-f]{2})"
		"-"
		"([0-9A-Fa-f]{2})"
		"([0-9A-Fa-f]{2})"
		"([0-9A-Fa-f]{2})"
		"([0-9A-Fa-f]{2})"
		"([0-9A-Fa-f]{2})"
		"([0-9A-Fa-f]{2})"
		"$"
	);

	return		!_identifier.indexOf( rule )
			&&	rule.captureCount() == ComponentsCount
		?	PluginIdentifier(
					rule.cap( 1 ).toUInt( 0, 16 )
				,	rule.cap( 2 ).toUShort( 0, 16 )
				,	rule.cap( 3 ).toUShort( 0, 16 )
				,	rule.cap( 4 ).toUShort( 0, 16 )
				,	rule.cap( 5 ).toUShort( 0, 16 )
				,	rule.cap( 6 ).toUShort( 0, 16 )
				,	rule.cap( 7 ).toUShort( 0, 16 )
				,	rule.cap( 8 ).toUShort( 0, 16 )
				,	rule.cap( 9 ).toUShort( 0, 16 )
				,	rule.cap( 10 ).toUShort( 0, 16 )
				,	rule.cap( 11 ).toUShort( 0, 16 )
			)
		:	PluginIdentifier()
	;

} // PluginIdentifier::fromString

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample
