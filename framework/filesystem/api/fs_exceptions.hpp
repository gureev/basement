// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_EXCEPTIONS_HPP__
#define __AMPLE_FILESYSTEM_EXCEPTIONS_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_exceptions.hpp"

#include "framework/filesystem/api/fs_exceptions_visitor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

EX_DECLARE_BASE_EXCEPTION(
		BaseException
	,	ExceptionsVisitor
);

/*---------------------------------------------------------------------------*/

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	CannotOpenFileForReading
	,	( std::string )( filePath )
)

EX_DECLARE_1_ARGUMENT(
		BaseException
	,	CannotOpenFileForWriting
	,	( std::string )( filePath )
)

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_EXCEPTIONS_HPP__
