// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTION_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTION_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_property.hpp"
#include "framework/resources/api/rs_access_type.hpp"

#include "framework/resources/sources/rs_property_sections_container.hpp"

#include "framework/tools/common_headers/containers/hash_map.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertyList;

/*---------------------------------------------------------------------------*/

class PropertySection
	:	public PropertySectionsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertySection();

	~PropertySection();

/*---------------------------------------------------------------------------*/

	void addResource(
			const std::string & _name
		,	const std::string & _value
	);

/*---------------------------------------------------------------------------*/

	const Resources::Property & getResource(
			const std::string & _name
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const;

	int getResourcesCount() const;

/*---------------------------------------------------------------------------*/

	PropertySection & addSection( const std::string & _name );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::HashMap<
				std::string
			,	boost::shared_ptr< Implementation::PropertyList >
		>
		Properties;

	Properties m_properties;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_SECTION_HPP__
