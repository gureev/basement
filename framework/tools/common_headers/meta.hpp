// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_META_HPP__
#define __AMPLE_COMMON_HEADERS_META_HPP__

/*----------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Meta {

/*----------------------------------------------------------------------------*/

template<
		bool _condition
	,	typename _TrueType
	,	typename _FalseType
>
class Conditional;

/*----------------------------------------------------------------------------*/

template<
		typename _TrueType
	,	typename _FalseType
>
class Conditional< true >
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	typedef
		_TrueType
		Result;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template<
		typename _TrueType
	,	typename _FalseType
>
class Conditional< false >
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	typedef
		_FalseType
		Result;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

} // namespace Meta
} // namespace CommonHeaders
} // namespace Ample

/*----------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_META_HPP__
