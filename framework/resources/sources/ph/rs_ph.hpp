// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_PH_HPP__
#define __AMPLE_RESOURCES_PH_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>

#include <QtCore/qstring.h>

#include <assert.h>
#include <string>

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_PH_HPP__
