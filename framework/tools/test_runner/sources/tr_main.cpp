// (C) 2013 Ample Test Runner

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

#include <QtCore/qlibrary.h>
#include <QtCore/qtextstream.h>

#include <boost/preprocessor/stringize.hpp>

#include <string>

#include <Windows.h>

/*---------------------------------------------------------------------------*/

int main( int _argc, char ** _argv )
{
	QTextStream console( stdout );

	if ( _argc < 2 )
	{
		console
			<< "Usage: test_runner <library_path> [boost parameters]"
			<< endl
		;
		return 1;
	}

	QString libraryName = _argv[ 1 ];

	libraryName.replace( '\\', '/' );
	libraryName.replace( "//", "/" );

	QLibrary library( libraryName );

	if ( ! QLibrary::isLibrary( libraryName ) )
	{
		console
			<< libraryName
			<< " is not a library!"
			<< endl
		;
		return 1;
	}

	if ( ! library.load() )
	{
		console
			<< library.errorString()
			<< endl
			<< "Error code: "
			<< GetLastError()
			<< endl
		;
		return 1;
	}

	typedef
		int ( *TestRunner )( int, char ** );

	TestRunner testRunner = static_cast< TestRunner >(
		library.resolve(
			BOOST_PP_STRINGIZE( TEST_RUNNER_ENTRY_POINT_FUNCTION )
		)
	);

	if ( !testRunner )
	{
		console
			<< library.errorString()
			<< endl
		;
		return 1;
	}

	return testRunner(
			_argc
		,	_argv
	);

} // main

/*---------------------------------------------------------------------------*/
