// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_READER_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_READER_HPP__

/*---------------------------------------------------------------------------*/

#include <QtCore/qthread.h>

#include <Windows.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class ConsoleManager;
class ConsoleWriter;

/*---------------------------------------------------------------------------*/

class ConsoleReader
	:	public QThread
{

/*---------------------------------------------------------------------------*/

	Q_OBJECT

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	ConsoleReader(
			ConsoleManager & _consoleManager
		,	ConsoleWriter & _consoleWriter
	);

/*---------------------------------------------------------------------------*/

	void enableInput();

	void disableInput();

/*---------------------------------------------------------------------------*/

signals:

/*---------------------------------------------------------------------------*/

	void onInputChangeEvent(
			const std::string & _input
		,	int _cursorLinePosition
	);

	void onEnterKeyEvent( std::string _command );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void readKey();

/*---------------------------------------------------------------------------*/

	void onEnterKey();

	void onBackspaceKey();

	void onTabKey();

	void onArrowUpKey();

	void onArrowRightKey();

	void onArrowDownKey();

	void onArrowLeftKey();

	void onPrintableChar( unsigned char _char );

/*---------------------------------------------------------------------------*/

	/*virtual*/ void run();

/*---------------------------------------------------------------------------*/

	bool m_enabled;

	std::string m_input;

/*---------------------------------------------------------------------------*/

	::HANDLE m_stdinHandle;

	::HANDLE m_stdoutHandle;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_READER_HPP__
