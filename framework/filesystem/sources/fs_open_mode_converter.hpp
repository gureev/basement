// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_OPEN_MODE_CONVERTER_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_OPEN_MODE_CONVERTER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_open_mode.hpp"

#include <QtCore/qiodevice.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class OpenModeConverter
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	static QIODevice::OpenMode convert( OpenMode::Enum _openMode )
	{
		switch ( _openMode )
		{
		case OpenMode::Append:				return QIODevice::Append;
		case OpenMode::Read:				return QIODevice::ReadOnly;
		case OpenMode::Write:				return QIODevice::WriteOnly;
		case OpenMode::ReadWrite:			return QIODevice::ReadWrite;
		case OpenMode::Truncate:			return QIODevice::Truncate;
		}

		return QIODevice::NotOpen;
	};

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_OPEN_MODE_CONVERTER_HPP__
