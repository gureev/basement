// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_PLUGIN_INSTANCE_HPP__
#define __AMPLE_RESOURCES_PLUGIN_INSTANCE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_accessor.hpp"

#include "framework/resources/plugin/rs_plugin_identifier.hpp"
#include "framework/resources/plugin/rs_environment.hpp"

#include "framework/connector/api/cn_plugin.hpp"
#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Plugin {

/*---------------------------------------------------------------------------*/

class Instance
	:	public Connector::Plugin
	,	public Environment
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Instance();

	/*virtual*/ ~Instance();

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Connector::Accessor > getAccessor() const;

	/*virtual*/ const Connector::PluginIdentifier & getIdentifier() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	/*virtual*/ void onPluginLoad();

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< Accessor > m_accessor;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_PLUGIN_INSTANCE_HPP__
