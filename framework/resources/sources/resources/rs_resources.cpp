// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

#include <boost/assign/list_of.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Constants {

/*---------------------------------------------------------------------------*/

	const int MajorFormatVersion = 1;

	const int MinorFormatVersion = 0;

	const char * Plugins[] = {
			"connector"
		,	"console"
		,	"filesystem"
		,	"resources"
		,	"serizalizator"
	};

/*---------------------------------------------------------------------------*/

} // namespace Constants

/*---------------------------------------------------------------------------*/

namespace Paths {

/*---------------------------------------------------------------------------*/

	const QString PluginStrings = ":/resources/strings/";

/*---------------------------------------------------------------------------*/

} // namespace Paths

/*---------------------------------------------------------------------------*/

namespace Xml {

/*---------------------------------------------------------------------------*/

namespace Tags {

/*---------------------------------------------------------------------------*/

	const QString Section = "section";

	const QString String = "string";

	const QString Property = "property";

/*---------------------------------------------------------------------------*/

} // namespace Tags

/*---------------------------------------------------------------------------*/

namespace Attributes {

/*---------------------------------------------------------------------------*/

	const QString Version = "version";

	const QString SectionName = "name";
	const QString SectionShortName = "short";

	const QString StringIdentifier = "identifier";
	const QString StringName = "name";
	const QString StringValue = "value";

	const QString PropertyName = "name";
	const QString PropertyValue = "value";

/*---------------------------------------------------------------------------*/

} // namespace Attributes

/*---------------------------------------------------------------------------*/

} // namespace Xml

/*---------------------------------------------------------------------------*/

namespace Templates {

/*---------------------------------------------------------------------------*/

	const QString Property =
		QString( "%4<%1 %2=\"%5\" %3=\"%6\"/>\n" ).arg(
				Xml::Tags::Property
			,	Xml::Attributes::PropertyName
			,	Xml::Attributes::PropertyValue
		);

	const QString PropertySection =
		QString(	"%3<%1 %2=\"%4\">\n"
					"%5"
					"%3</%1>\n"
		).arg(
				Xml::Tags::Section
			,	Xml::Attributes::SectionName
		);

/*---------------------------------------------------------------------------*/

	const QString String =
		QString( "%5<%1 %2=\"%6\" %3=\"%7\" %4=\"%8\"/>\n" ).arg(
				Xml::Tags::String
			,	Xml::Attributes::StringName
			,	Xml::Attributes::StringIdentifier
			,	Xml::Attributes::StringValue
		);

	const QString StringSection =
		QString(	"%4<%1 %2=\"%5\" %3=\"%6\">\n"
					"%7"
					"%4</%1>\n"
		).arg(
				Xml::Tags::Section
			,	Xml::Attributes::SectionName
			,	Xml::Attributes::SectionShortName
		);

/*---------------------------------------------------------------------------*/

} // namespace Templates

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Resources
} // namespace Ample
