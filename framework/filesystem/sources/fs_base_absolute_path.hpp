// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_ABSOLUTE_PATH_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_ABSOLUTE_PATH_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/sources/fs_base_path.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class BaseAbsolutePath
	:	public BasePath
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	BaseAbsolutePath(
			const char _driveLetter
		,	int _componentsCount
	);

	BaseAbsolutePath( const BaseAbsolutePath & _other );

/*---------------------------------------------------------------------------*/

	/*virtual*/ bool isAbsolute() const;

	/*virtual*/ bool isRelative() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ Ptr clone() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ char getDriveLetter() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	/*virtual*/ int calculatePathLength() const;

/*---------------------------------------------------------------------------*/

	const char m_driveLetter;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_ABSOLUTE_PATH_HPP__
