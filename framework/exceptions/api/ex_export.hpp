// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_EXPORT_HPP__
#define __AMPLE_EXCEPTIONS_EXPORT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

#ifdef _PLUGIN_EXCEPTIONS_
	#define EXCEPTIONS_API __declspec( dllexport )
#else
	#define EXCEPTIONS_API __declspec( dllimport )
#endif

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_EXPORT_HPP__
