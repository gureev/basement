// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_TEST_FIXTURE_HPP__
#define __AMPLE_EXCEPTIONS_TEST_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/tests/ex_test_exceptions_visitor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {
namespace Tests {

/*---------------------------------------------------------------------------*/

class Fixture
{

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_TEST_FIXTURE_HPP__
