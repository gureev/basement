// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_UNIX_PATH_BUILDER_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_UNIX_PATH_BUILDER_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class UnixPathBuilder
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	static void optimize( std::string & _path );

	static bool isAbsolute( const std::string & _path );

	static bool isSlash( const char _symbol );

	static const char extractDriveLetter( const std::string & _path );

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_UNIX_PATH_BUILDER_HPP__
