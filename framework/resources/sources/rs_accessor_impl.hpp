// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_ACCESSOR_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_accessor.hpp"

#include "framework/resources/sources/rs_path_factory.hpp"
#include "framework/resources/sources/rs_property_sections_container.hpp"
#include "framework/resources/sources/rs_string_sections_container.hpp"
#include "framework/resources/sources/rs_property_validator_impl.hpp"
#include "framework/resources/sources/rs_property_validator_hasher.hpp"
#include "framework/resources/sources/rs_property_validator_comparator.hpp"

#include "framework/tools/common_headers/containers/hash_set.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Accessor
	:	public Ample::Resources::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Accessor();

	/*virtual*/ ~Accessor();

/*---------------------------------------------------------------------------*/

	/*virtual*/ const Ample::Resources::String & getString(
			const char * _resourcePath
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const;

	/*virtual*/ const Ample::Resources::Property & getProperty(
			const char * _resourcePath
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void validateProperty(
			const char * _resourcePath
		,	const Ample::Resources::PropertyValidator & _validator
		,	AccessType::Enum _accessType = AccessType::FirstAvailable
	) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ Ample::Resources::PropertyValidator & createValidator();

/*---------------------------------------------------------------------------*/

	/*virtual*/ void restoreStrings( QIODevice & _stream );

	/*virtual*/ void restoreProperties( QIODevice & _stream );

/*---------------------------------------------------------------------------*/

	/*virtual*/ void storeStrings(
			QIODevice & _stream
		,	const std::string & _sectionName
	);

	/*virtual*/ void storeProperties(
			QIODevice & _stream
		,	const std::string & _sectionName
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	template<
			typename _Resource
		,	typename _Section
		,	typename _SectionContainer
	>
	const _Resource & getResource(
			const _SectionContainer & _sections
		,	const Path & _resourcePath
		,	AccessType::Enum _accessType
	) const;

/*---------------------------------------------------------------------------*/

	template<
			typename _Section
		,	typename _SectionContainer
	>
	const _Section & getSection(
			const _SectionContainer & _sections
		,	const Path & _resourcePath
	) const;

/*---------------------------------------------------------------------------*/

	PathFactory m_pathFactory;

/*---------------------------------------------------------------------------*/

	StringSectionsContainer m_strings;

	PropertySectionsContainer m_properties;

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::HashSet<
				boost::shared_ptr< Implementation::PropertyValidator >
			,	PropertyValidatorHasher
			,	PropertyValidatorComparator
		>
		PropertyValidators;

	PropertyValidators m_propertyValidators;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_ACCESSOR_HPP__
