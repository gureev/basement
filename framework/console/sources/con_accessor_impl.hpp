// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_ACCESSOR_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_accessor.hpp"

#include "framework/console/sources/con_message_statistic_impl.hpp"
#include "framework/console/sources/con_console_manager.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace Resources
	{
		class Accessor;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Accessor
	:	public Console::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Accessor( const std::string & _applicationName );

	/*virtual*/ ~Accessor();

/*---------------------------------------------------------------------------*/

	/*virtual*/ void print(
			MessageLevel::Enum _messageLevel
		,	const Resources::String & _string
	);

	/*virtual*/ void print(
			MessageLevel::Enum _messageLevel
		,	const std::string & _message
	);

/*---------------------------------------------------------------------------*/

	/*virtual*/ const MessageStatistic & getMessageStatistic() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	Implementation::ConsoleManager m_console;

	Implementation::MessageStatistic m_messageStatistic;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_ACCESSOR_HPP__
