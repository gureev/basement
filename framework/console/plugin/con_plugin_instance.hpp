// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_PLUGIN_INSTANCE_HPP__
#define __AMPLE_CONSOLE_PLUGIN_INSTANCE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_accessor.hpp"

#include "framework/console/plugin/con_plugin_identifier.hpp"
#include "framework/console/plugin/con_environment.hpp"

#include "framework/connector/api/cn_plugin.hpp"
#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Plugin {

/*---------------------------------------------------------------------------*/

class Instance
	:	public Connector::Plugin
	,	public Environment
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Instance();

	/*virtual*/ ~Instance();

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Connector::Accessor > getAccessor() const;

	/*virtual*/ const Connector::PluginIdentifier & getIdentifier() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< Ample::Resources::Accessor > getResources() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void onPluginLoad();

/*---------------------------------------------------------------------------*/

	boost::shared_ptr< Accessor > m_accessor;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_PLUGIN_INSTANCE_HPP__
