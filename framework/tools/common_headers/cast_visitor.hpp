// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CAST_VISITOR_HPP__
#define __AMPLE_COMMON_HEADERS_CAST_VISITOR_HPP__

/*----------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {

/*----------------------------------------------------------------------------*/

template<
		typename _Target
	,	typename _DefaultVisitor
>
class Cast
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	template< typename _Source >
	boost::optional< const _Target & > cast( const _Source & _source );

/*----------------------------------------------------------------------------*/

private:

/*----------------------------------------------------------------------------*/

	/*virtual*/ void visit( const _Target & _target );

/*----------------------------------------------------------------------------*/

private:

/*----------------------------------------------------------------------------*/

	boost::optional< const _Target & > m_result;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template<
		typename _Target
	,	typename _DefaultVisitor
>
template< typename _Source >
boost::optional< const _Target & >
Cast< _Target, _DefaultVisitor >::cast( const _Source & _source )
{
	m_result = boost::optional< const _Target & >();
	_source.accept( * this );
	return m_result;
}

/*----------------------------------------------------------------------------*/

template<
		typename _Target
	,	typename _DefaultVisitor
>
void
Cast< _Target, _DefaultVisitor >::visit( const _Target & _target )
{
	m_result = _target;
}

/*----------------------------------------------------------------------------*/

} // namespace CommonHeaders
} // namespace Ample

/*----------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CAST_VISITOR_HPP__
