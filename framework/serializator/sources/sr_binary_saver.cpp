// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

#include "framework/serializator/api/sr_binary_saver.hpp"

#include <QtCore/qdatastream.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

BinarySaver::BinarySaver( QDataStream & _outputStream )
	:	m_outputStream( _outputStream )
{
} // BinarySaver::BinarySaver

/*---------------------------------------------------------------------------*/

BinarySaver::~BinarySaver()
{
} // BinarySaver::~BinarySaver

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( bool _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( char _value )
{
	save( static_cast< unsigned char >( _value ) );

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( unsigned char _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( short _value )
{
	save( static_cast< unsigned short >( _value ) );

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( unsigned short _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( int _value )
{
	save( static_cast< unsigned int >( _value ) );

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( unsigned int _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( long _value )
{
	save( static_cast< unsigned long >( _value ) );

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( unsigned long _value )
{
	save( static_cast< unsigned long long >( _value ) );

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( long long _value )
{
	save( static_cast< unsigned long long >( _value ) );

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( unsigned long long _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( float _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( double _value )
{
	m_outputStream << _value;

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

void
BinarySaver::save( const std::string & _value )
{
	int length = static_cast< int >( _value.length() );
	save( length );

	if ( !length )
		return;

	m_outputStream.writeRawData(
			_value.data()
		,	length
	);

} // BinarySaver::save

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample
