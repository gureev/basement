// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_property_sections_container.hpp"

#include "framework/resources/tests/rs_test_property_fixture.hpp"

#include "framework/tools/common_headers/numeric_type_info.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Properties:
			1) Section tag
				1.1) Missing name attribute (Not implemented)
				1.2) Empty name attribute
			2) Property tag
				2.1) Missing name attribute (Not implemented)
				2.2) Missing value attribute (Not implemented)
				2.3) Empty name attribute
				2.4) Empty value attribute
			3) One section
				3.1) Without properties
				3.2) One property
				3.3) Two properties
			4) One nested section
				4.1) Without properties
				4.2) One property
				4.3) Two properties
			5) Two nested section
				5.1) Without properties
				5.2) One property only in first section
				5.3) One property only in second section
				5.4) One property in each section
				5.5) Two properties only in first section
				5.6) Two properties only in second section
				5.7) Two properties in each section
			6) Mixed section
				6.1) One property and one nested section
					6.1.1) Property before section
					6.1.2) Property after section
				6.2) One property and two nested sections
					6.2.1) Property before sections
					6.2.2) Property after sections
					6.2.3) Property between sections
				6.3) One property and one property in nested section
				6.4) One property and two properties in nested section
				6.5) Two properties and one nested section
					6.4.1) Properties before section
					6.4.2) Properties after section
				6.6) Two properties and two nested sections
					6.2.1) Properties before sections
					6.2.2) Properties after sections
					6.2.3) Properties between sections
				6.7) Two properties and one property in nested section
				6.8) Two properties and two properties in nested section
			7) Resolve
				7.1) Section and nested section has equal names
				7.2) Property and nested property has equal names
				7.3) Two sections has equal names
				7.4) Two properties has equal names
				7.5) Property and section has equal names
					7.5.1) First property
					7.5.2) First section
				7.6) Property name and value are equal
			8) Property types
				8.1) String type
				8.2) Zero value
					8.2.1) Integer
					8.2.2) Double
				8.3) One value
					8.3.1) Integer
					8.3.2) Double
				8.4) Negative value
					8.4.1) Integer
					8.4.2) Double
				8.5) Positive value
					8.5.1) Integer
					8.5.2) Double
				8.6) Too big negative value
					8.6.1) Integer
					8.6.2) Double
				8.7) Too big positive value
					8.7.1) Integer
					8.7.2) Double
				8.8) Bool true
				8.9) Bool false
			9) Property overlap
				9.1) One property
				9.2) Two properties in same section
				9.3) Two properties in different sections
				9.4) Two properties in different level sections
				9.5) New property
				9.6) New section
				9.7) New nested section

*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Properties, PropertyFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_1_2_SectionTag_EmptyNameAttribute )
{
	/*---------- Setup ----------*/

	section( "" ).end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Properties_1_2_SectionTag_EmptyNameAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_2_3_PropertyTag_EmptyNameAttribute )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "", "test_value" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Properties_2_3_PropertyTag_EmptyNameAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_2_4_PropertyTag_EmptyValueAttribute )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Properties_2_4_PropertyTag_EmptyValueAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_3_2_OneSection_OneProperty )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_3_2_OneSection_OneProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_3_3_OneSection_TwoProperties )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource( "test_property_1" );
		const Property & value2 = section.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_3_3_OneSection_TwoProperties

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_4_1_OneNestedSection_WithoutProperties )
{
	/*---------- Setup ----------*/

	section( "test_section_1" )
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_4_1_OneNestedSection_WithoutProperties

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_4_2_OneNestedSection_OneProperty )
{
	/*---------- Setup ----------*/

	section( "test_section_1" )
		.section( "test_section_2" )
			.property( "test_property", "test_value" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );

		const Property & value = section2.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_4_2_OneNestedSection_OneProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_4_3_OneNestedSection_TwoProperties )
{
	/*---------- Setup ----------*/

	section( "test_section_1" )
		.section( "test_section_2" )
			.property( "test_property_1", "test_value_1" )
			.property( "test_property_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 2 );

		const Property & value1 = section2.getResource( "test_property_1" );
		const Property & value2 = section2.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_4_3_OneNestedSection_TwoProperties

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_1_TwoNestedSection_WithoutProperties )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_1_TwoNestedSection_WithoutProperties

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_2_TwoNestedSection_OnePropertyOnlyInFirstSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" )
			.property( "test_property", "test_value" )
		.end()
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value = section1.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_2_TwoNestedSection_OnePropertyOnlyInFirstSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_3_TwoNestedSection_OnePropertyOnlyInSecondSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" )
			.property( "test_property", "test_value" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );

		const Property & value = section2.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_3_TwoNestedSection_OnePropertyOnlyInSecondSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_4_TwoNestedSection_OnePropertyInEachSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" )
			.property( "test_property_1", "test_value_1" )
		.end()
		.section( "test_section_2" )
			.property( "test_property_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );

		const Property & value1 = section1.getResource( "test_property_1" );
		const Property & value2 = section2.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_4_TwoNestedSection_OnePropertyInEachSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_5_TwoNestedSection_TwoPropertiesOnlyInFirstSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" )
			.property( "test_property_1", "test_value_1" )
			.property( "test_property_2", "test_value_2" )
		.end()
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 2 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value1 = section1.getResource( "test_property_1" );
		const Property & value2 = section1.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_5_TwoNestedSection_TwoPropertiesOnlyInFirstSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_6_TwoNestedSection_TwoPropertiesOnlyInSecondSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" )
			.property( "test_property_1", "test_value_1" )
			.property( "test_property_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 2 );

		const Property & value1 = section2.getResource( "test_property_1" );
		const Property & value2 = section2.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_6_TwoNestedSection_TwoPropertiesOnlyInSecondSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_5_7_TwoNestedSection_TwoPropertiesInEachSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" )
			.property( "test_property_1", "test_value_1" )
			.property( "test_property_2", "test_value_2" )
		.end()
		.section( "test_section_2" )
			.property( "test_property_3", "test_value_3" )
			.property( "test_property_4", "test_value_4" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 2 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 2 );

		const Property & value1 = section1.getResource( "test_property_1" );
		const Property & value2 = section1.getResource( "test_property_2" );
		const Property & value3 = section2.getResource( "test_property_3" );
		const Property & value4 = section2.getResource( "test_property_4" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "test_value_3" );
		BOOST_REQUIRE_EQUAL( value4.asString(), "test_value_4" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_5_7_TwoNestedSection_TwoPropertiesInEachSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_1_1_MixedSection_OnePropertyAndOneNestedSection_PropertyBeforeSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property", "test_value" )
		.section( "test_section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		const Property & value = parent.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_1_1_MixedSection_OnePropertyAndOneNestedSection_PropertyBeforeSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_1_2_MixedSection_OnePropertyAndOneNestedSection_PropertyAfterSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section" ).end()
		.property( "test_property", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		const Property & value = parent.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_1_2_MixedSection_OnePropertyAndOneNestedSection_PropertyAfterSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_2_1_MixedSection_OnePropertyAndTwoNestedSections_PropertyBeforeSections )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property", "test_value" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value = parent.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_2_1_MixedSection_OnePropertyAndTwoNestedSections_PropertyBeforeSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_2_2_MixedSection_OnePropertyAndTwoNestedSections_PropertyAfterSections )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" ).end()
		.property( "test_property", "test_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value = parent.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_2_2_MixedSection_OnePropertyAndTwoNestedSections_PropertyAfterSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_2_3_MixedSection_OnePropertyAndTwoNestedSections_PropertyBetweenSections )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.property( "test_property", "test_value" )
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value = parent.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_2_3_MixedSection_OnePropertyAndTwoNestedSections_PropertyBetweenSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_3_MixedSection_OnePropertyAndOnePropertyInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property_1", "test_value_1" )
		.section( "test_section" )
			.property( "test_property_2", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = section.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_3_MixedSection_OnePropertyAndOnePropertyInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_4_MixedSection_OnePropertyAndTwoPropertiesInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property_1", "test_value_1" )
		.section( "test_section" )
			.property( "test_property_2", "test_value_2" )
			.property( "test_property_3", "test_value_3" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = section.getResource( "test_property_2" );
		const Property & value3 = section.getResource( "test_property_3" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "test_value_3" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_4_MixedSection_OnePropertyAndTwoPropertiesInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_5_1_MixedSection_TwoPropertiesAndOneNestedSection_PropertiesBeforeSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
		.section( "test_section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_5_1_MixedSection_TwoPropertiesAndOneNestedSection_PropertiesBeforeSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_5_2_MixedSection_TwoPropertiesAndOneNestedSection_PropertiesAfterSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section" ).end()
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_5_2_MixedSection_TwoPropertiesAndOneNestedSection_PropertiesAfterSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_6_1_MixedSection_TwoPropertiesAndTwoNestedSections_PropertiesBeforeSections )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_6_1_MixedSection_TwoPropertiesAndTwoNestedSections_PropertiesBeforeSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_6_2_MixedSection_TwoPropertiesAndTwoNestedSections_PropertiesAfterSections )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.section( "test_section_2" ).end()
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_6_2_MixedSection_TwoPropertiesAndTwoNestedSections_PropertiesAfterSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_6_3_MixedSection_TwoPropertiesAndTwoNestedSections_PropertiesBetweenSections )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" ).end()
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
		.section( "test_section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_6_3_MixedSection_TwoPropertiesAndTwoNestedSections_PropertiesBetweenSections

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_7_MixedSection_TwoPropertiesAndOnePropertyInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
		.section( "test_section" )
			.property( "test_property_3", "test_value_3" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );
		const Property & value3 = section.getResource( "test_property_3" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "test_value_3" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_7_MixedSection_TwoPropertiesAndOnePropertyInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_6_8_MixedSection_TwoPropertiesAndTwoPropertiesInNestedSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property_1", "test_value_1" )
		.property( "test_property_2", "test_value_2" )
		.section( "test_section" )
			.property( "test_property_3", "test_value_3" )
			.property( "test_property_4", "test_value_4" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 2 );

		auto & section = parent.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = parent.getResource( "test_property_1" );
		const Property & value2 = parent.getResource( "test_property_2" );
		const Property & value3 = section.getResource( "test_property_3" );
		const Property & value4 = section.getResource( "test_property_4" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "test_value_3" );
		BOOST_REQUIRE_EQUAL( value4.asString(), "test_value_4" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_6_8_MixedSection_TwoPropertiesAndTwoPropertiesInNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_1_Resolve_SectionAndNestedSectionHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.section( "test_section" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		auto & section2 = section1.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_7_1_Resolve_SectionAndNestedSectionHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_2_Resolve_PropertyAndNestedPropertyHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "test_section_1" )
		.property( "test_property", "test_value_1" )
		.section( "test_section_2" )
			.property( "test_property", "test_value_2" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section1 = sections.getSection( "test_section_1" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );

		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );

		const Property & value1 = section1.getResource( "test_property" );
		const Property & value2 = section2.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "test_value_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "test_value_2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_7_2_Resolve_PropertyAndNestedPropertyHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_3_Resolve_TwoSectionsHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section" ).end()
		.section( "test_section" ).end()
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateSection
	);

} // Properties_7_3_Resolve_TwoSectionsHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_4_Resolve_TwoPropertiesHasEqualNames )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property", "test_value_1" )
		.property( "test_property", "test_value_2" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateResource
	);

} // Properties_7_4_Resolve_TwoPropertiesHasEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_5_1_Resolve_PropertyAndSectionHasEqualNames_FirstProperty )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_resource", "test_value_1" )
		.section( "test_resource" ).end()
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateResource
	);

} // Properties_7_5_1_Resolve_PropertyAndSectionHasEqualNames_FirstProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_5_2_Resolve_PropertyAndSectionHasEqualNames_FirstSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_resource" ).end()
		.property( "test_resource", "test_value_1" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicateResource
	);

} // Properties_7_5_2_Resolve_PropertyAndSectionHasEqualNames_FirstSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_7_6_Resolve_PropertyNameAndValueAreEqual )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.property( "test_property", "test_property" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 1 );

		const Property & value = parent.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_property" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_7_6_Resolve_PropertyNameAndValueAreEqual

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_1_PropertyTypes_StringType )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "test_string_value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE_EQUAL( value.asString(), "test_string_value" );

		BOOST_REQUIRE( ! value.asBool() );
		BOOST_REQUIRE( ! value.asDouble() );
		BOOST_REQUIRE( ! value.asInt() );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_1_PropertyTypes_StringType

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_2_1_PropertyTypes_ZeroValue_Integer )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "0" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "0" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_2_1_PropertyTypes_ZeroValue_Integer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_2_2_PropertyTypes_ZeroValue_Double )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "0.0" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "0.0" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_2_2_PropertyTypes_ZeroValue_Double

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_3_1_PropertyTypes_OneValue_Integer )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "1" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 1.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 1 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_3_1_PropertyTypes_OneValue_Integer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_3_2_PropertyTypes_OneValue_Double )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "1.0" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "1.0" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 1.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 1 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_3_2_PropertyTypes_OneValue_Double

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_4_1_PropertyTypes_NegativeValue_Integer )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "-555555" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "-555555" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), -555555.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), -555555 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_4_1_PropertyTypes_NegativeValue_Integer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_4_2_PropertyTypes_NegativeValue_Double )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "-123.456" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "-123.456" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), -123.456 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), -123 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_4_2_PropertyTypes_NegativeValue_Double

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_5_1_PropertyTypes_PositiveValue_Integer )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "4444" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "4444" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 4444 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 4444 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_5_1_PropertyTypes_PositiveValue_Integer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_5_2_PropertyTypes_PositiveValue_Double )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "456.789" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "456.789" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 456.789 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 456 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_5_2_PropertyTypes_PositiveValue_Double

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_6_1_PropertyTypes_TooBigNegativeValue_Integer )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "-11122233344455566688887777.0" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "-11122233344455566688887777.0" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE( compare( * value.asDouble(), -11122233344455566688887777.0 ) );
		BOOST_REQUIRE_EQUAL( * value.asInt(), CommonHeaders::NumericTypeInfo< int >::MinimumValue );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_6_1_PropertyTypes_TooBigNegativeValue_Integer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_6_2_PropertyTypes_TooBigNegativeValue_Double )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "-1112223334445556668777.888999" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "-1112223334445556668777.888999" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE( compare( * value.asDouble(), -1112223334445556668777.888999 ) );
		BOOST_REQUIRE_EQUAL( * value.asInt(), CommonHeaders::NumericTypeInfo< int >::MinimumValue );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_6_2_PropertyTypes_TooBigNegativeValue_Double

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_7_1_PropertyTypes_TooBigPositiveValue_Integer )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "4444888899996666333" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "4444888899996666333" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE( compare( * value.asDouble(), 4444888899996666333.0 ) );
		BOOST_REQUIRE_EQUAL( * value.asInt(), CommonHeaders::NumericTypeInfo< int >::MaximumValue );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_7_1_PropertyTypes_TooBigPositiveValue_Integer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_7_2_PropertyTypes_TooBigPositiveValue_Double )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "555666777888222111444555.555" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "555666777888222111444555.555" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 555666777888222111444555.555 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), CommonHeaders::NumericTypeInfo< int >::MaximumValue );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_7_2_PropertyTypes_TooBigPositiveValue_Double

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_8_PropertyTypes_BoolTrue )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "true" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "true" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 1.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 1 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_8_PropertyTypes_BoolTrue

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_8_9_PropertyTypes_BoolFalse )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "false" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "false" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_8_9_PropertyTypes_BoolFalse

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_1_PropertyOverlap_OneProperty )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property", "false" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "test_property" );

		BOOST_REQUIRE( value.asBool() );
		BOOST_REQUIRE( value.asDouble() );
		BOOST_REQUIRE( value.asInt() );

		BOOST_REQUIRE_EQUAL( value.asString(), "false" );
		BOOST_REQUIRE_EQUAL( * value.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value.asInt(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_1_PropertyOverlap_OneProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_2_PropertyOverlap_TwoPropertiesInSameSection )
{
	/*---------- Setup ----------*/

	section( "test_section" )
		.property( "test_property_1", "false" )
		.property( "test_property_2", "true" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test_section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource( "test_property_1" );
		const Property & value2 = section.getResource( "test_property_2" );

		BOOST_REQUIRE( value1.asBool() );
		BOOST_REQUIRE( value1.asDouble() );
		BOOST_REQUIRE( value1.asInt() );

		BOOST_REQUIRE_EQUAL( value1.asString(), "false" );
		BOOST_REQUIRE_EQUAL( * value1.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value1.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value1.asInt(), 0 );

		BOOST_REQUIRE( value2.asBool() );
		BOOST_REQUIRE( value2.asDouble() );
		BOOST_REQUIRE( value2.asInt() );

		BOOST_REQUIRE_EQUAL( value2.asString(), "true" );
		BOOST_REQUIRE_EQUAL( * value2.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value2.asDouble(), 1.0 );
		BOOST_REQUIRE_EQUAL( * value2.asInt(), 1 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_2_PropertyOverlap_TwoPropertiesInSameSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_3_PropertyOverlap_TwoPropertiesInDifferentSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" )
			.property( "test_property_1", "false" )
		.end()
		.section( "test_section_2" )
			.property( "test_property_2", "true" )
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = parent.getSection( "test_section_2" );

		const Property & value1 = section1.getResource( "test_property_1" );
		const Property & value2 = section2.getResource( "test_property_2" );

		BOOST_REQUIRE( value1.asBool() );
		BOOST_REQUIRE( value1.asDouble() );
		BOOST_REQUIRE( value1.asInt() );

		BOOST_REQUIRE_EQUAL( value1.asString(), "false" );
		BOOST_REQUIRE_EQUAL( * value1.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value1.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value1.asInt(), 0 );

		BOOST_REQUIRE( value2.asBool() );
		BOOST_REQUIRE( value2.asDouble() );
		BOOST_REQUIRE( value2.asInt() );

		BOOST_REQUIRE_EQUAL( value2.asString(), "true" );
		BOOST_REQUIRE_EQUAL( * value2.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value2.asDouble(), 1.0 );
		BOOST_REQUIRE_EQUAL( * value2.asInt(), 1 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_3_PropertyOverlap_TwoPropertiesInDifferentSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_4_PropertyOverlap_TwoPropertiesInDifferentLevelSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "test_section_1" )
			.property( "test_property_1", "false" )
			.section( "test_section_2" )
				.property( "test_property_2", "true" )
			.end()
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		runTest();

		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "test_section_1" );
		auto & section2 = section1.getSection( "test_section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 1 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 1 );

		const Property & value1 = section1.getResource( "test_property_1" );
		const Property & value2 = section2.getResource( "test_property_2" );

		BOOST_REQUIRE( value1.asBool() );
		BOOST_REQUIRE( value1.asDouble() );
		BOOST_REQUIRE( value1.asInt() );

		BOOST_REQUIRE_EQUAL( value1.asString(), "false" );
		BOOST_REQUIRE_EQUAL( * value1.asBool(), false );
		BOOST_REQUIRE_EQUAL( * value1.asDouble(), 0.0 );
		BOOST_REQUIRE_EQUAL( * value1.asInt(), 0 );

		BOOST_REQUIRE( value2.asBool() );
		BOOST_REQUIRE( value2.asDouble() );
		BOOST_REQUIRE( value2.asInt() );

		BOOST_REQUIRE_EQUAL( value2.asString(), "true" );
		BOOST_REQUIRE_EQUAL( * value2.asBool(), true );
		BOOST_REQUIRE_EQUAL( * value2.asDouble(), 1.0 );
		BOOST_REQUIRE_EQUAL( * value2.asInt(), 1 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_4_PropertyOverlap_TwoPropertiesInDifferentLevelSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_5_PropertyOverlap_NewProperty )
{
	/*---------- Setup ----------*/

	section( "section" )
		.property( "property_1", "value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value = section.getResource( "property_1" );

		BOOST_REQUIRE_EQUAL( value.asString(), "value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "section" )
		.property( "property_2", "value" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource( "property_1" );
		const Property & value2 = section.getResource( "property_2" );

		BOOST_REQUIRE_EQUAL( value1.asString(), "value" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "value" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_5_PropertyOverlap_NewProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_6_PropertyOverlap_NewSection )
{
	/*---------- Setup ----------*/

	section( "section" )
		.section( "nested_1" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "section" )
		.section( "nested_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );
		auto & nestedSection2 = section.getSection( "nested_2" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( nestedSection2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_6_PropertyOverlap_NewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Properties_9_7_PropertyOverlap_NewNestedSection )
{
	/*---------- Setup ----------*/

	section( "section" )
		.section( "nested_1" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "section" )
		.section( "nested_1" )
			.section( "nested_2" ).end()
		.end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );

		auto & nestedSection1 = section.getSection( "nested_1" );
		auto & nestedSection2 = nestedSection1.getSection( "nested_2" );

		BOOST_REQUIRE_EQUAL( nestedSection1.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( nestedSection1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( nestedSection2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( nestedSection2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Properties_9_7_PropertyOverlap_NewNestedSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Properties

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
