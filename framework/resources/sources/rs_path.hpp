// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PATH_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PATH_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Path
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Path( const std::string & _resourcePath );

	~Path();

/*---------------------------------------------------------------------------*/

	int getComponentsCount() const;

	const std::string & getComponent( int _index ) const;

	const std::string & getResource() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::Vector< std::string >
		Components;

	Components m_components;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PATH_HPP__
