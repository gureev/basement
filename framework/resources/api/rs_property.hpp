// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_PROPERTY_HPP__
#define __AMPLE_RESOURCES_PROPERTY_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {

/*---------------------------------------------------------------------------*/

class Property
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~Property() {}

/*---------------------------------------------------------------------------*/

	virtual const std::string & asString() const = 0;

	virtual boost::optional< int > asInt() const = 0;

	virtual boost::optional< bool > asBool() const = 0;

	virtual boost::optional< double > asDouble() const = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_PROPERTY_HPP__
