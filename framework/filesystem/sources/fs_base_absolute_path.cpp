// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_base_absolute_path.hpp"

#include <boost/filesystem.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

BaseAbsolutePath::BaseAbsolutePath(
		const char _driveLetter
	,	int _componentsCount
)
	:	m_driveLetter( _driveLetter )
	,	BasePath( _componentsCount )
{
} // BaseAbsolutePath::BaseAbsolutePath

/*---------------------------------------------------------------------------*/

BaseAbsolutePath::BaseAbsolutePath( const BaseAbsolutePath & _other )
	:	m_driveLetter( _other.m_driveLetter )
	,	BasePath( _other.m_components.size() )
{
	int nComponents = _other.m_components.size();
	for ( int i = 0; i < nComponents; ++i )
		m_components[ i ] = _other.m_components[ i ];

} // BaseAbsolutePath::BaseAbsolutePath

/*---------------------------------------------------------------------------*/

bool
BaseAbsolutePath::isAbsolute() const
{
	return true;

} // BaseAbsolutePath::isAbsolute

/*---------------------------------------------------------------------------*/

bool
BaseAbsolutePath::isRelative() const
{
	return false;

} // BaseAbsolutePath::isRelative

/*---------------------------------------------------------------------------*/

FileSystem::Path::Ptr
BaseAbsolutePath::clone() const
{
	boost::shared_ptr< Implementation::BaseAbsolutePath > newPath(
		new BaseAbsolutePath( * this )
	);

	return boost::static_pointer_cast< FileSystem::Path >( newPath );

} // BaseAbsolutePath::clone

/*---------------------------------------------------------------------------*/

char
BaseAbsolutePath::getDriveLetter() const
{
	return m_driveLetter;

} // BaseAbsolutePath::getDriveLetter

/*---------------------------------------------------------------------------*/

int
BaseAbsolutePath::calculatePathLength() const
{
	int rootLength = 1;
	if ( m_driveLetter != '/' )
		rootLength += 2;

	return BasePath::calculatePathLength() + rootLength;

} // BaseAbsolutePath::calculatePathLength

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
