// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_string_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

String::String(
		const std::string & _sectionName
	,	const std::string & _identifier
	,	const std::string & _value
)
	:	m_identifier( _identifier )
	,	m_value( _value )
	,	m_sectionName( _sectionName )
{
} // String::String

/*---------------------------------------------------------------------------*/

String::~String()
{
} // String::~String

/*---------------------------------------------------------------------------*/

const std::string &
String::getSectionName() const
{
	return m_sectionName;

} // String::getSectionName

/*---------------------------------------------------------------------------*/

const std::string &
String::getIdentifier() const
{
	return m_identifier;

} // String::getIdentifier

/*---------------------------------------------------------------------------*/

const std::string &
String::getValue() const
{
	return m_value;

} // String::getValue

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
