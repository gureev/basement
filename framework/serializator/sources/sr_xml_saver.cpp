// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

#include "framework/serializator/api/sr_xml_saver.hpp"
#include "framework/serializator/api/sr_exceptions.hpp"

#include <QtCore/qstring.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

XmlSaver::XmlSaver( QIODevice & _outputStream )
	:	m_outputStream( _outputStream )
{
	if ( !_outputStream.isOpen() )
		EX_THROW_0_ARGUMENT( Exceptions::SaveFailure )

} // XmlSaver::XmlSaver

/*---------------------------------------------------------------------------*/

XmlSaver::~XmlSaver()
{
} // XmlSaver::~XmlSaver

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample
