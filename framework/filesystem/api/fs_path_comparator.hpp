// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_PATH_COMPARATOR_HPP__
#define __AMPLE_FILESYSTEM_PATH_COMPARATOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_path.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {

/*---------------------------------------------------------------------------*/

class PathComparator
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	bool operator () (
			const boost::shared_ptr< Path > & _firstPath
		,	const boost::shared_ptr< Path > & _secondPath
	) const
	{
		return ( *this )(
				* _firstPath
			,	* _secondPath
		);

	} // operator ()

/*---------------------------------------------------------------------------*/

	bool operator () (
			const Path & _firstPath
		,	const Path & _secondPath
	) const
	{
		int nComponents = _firstPath.getComponentsCount();
		if ( nComponents != _secondPath.getComponentsCount() )
			return false;

		bool absolute = _firstPath.isAbsolute();
		if ( absolute != _secondPath.isAbsolute() )
			return false;

		for ( int i = nComponents - 1; i >= 0; --i )
			if ( _firstPath.getComponent( i ) != _secondPath.getComponent( i ) )
				return false;

		if (	absolute
			&&	_firstPath.getDriveLetter() != _secondPath.getDriveLetter()
		)
			return false;

		return true;

	} // operator ()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_PATH_COMPARATOR_HPP__
