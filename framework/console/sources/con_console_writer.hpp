// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_WRITER_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_WRITER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_message_level.hpp"

#include <QtCore/qthread.h>

#include <boost/optional.hpp>
#include <boost/function.hpp>

#include <Windows.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class ConsoleWriter
	:	public QThread
{

/*---------------------------------------------------------------------------*/

	Q_OBJECT

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	ConsoleWriter( const std::string & _applicationName );

/*---------------------------------------------------------------------------*/

	void setInputControls(
			boost::function< void () > _inputLocker
		,	boost::function< void () > _inputUnlocker
	);

/*---------------------------------------------------------------------------*/

public slots:

/*---------------------------------------------------------------------------*/

	void print(
			MessageLevel::Enum _messageLevel
		,	const std::string & _moduleShortName
		,	const std::string & _messageIdentifier
		,	const std::string & _message
	);

	void printCommand( const std::string & _command );

	void onInputChange(
			const std::string & _command
		,	int _cursorLinePosition
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	/*virtual*/ void run();

/*---------------------------------------------------------------------------*/

	void printIdentifier(
			const std::string & _moduleShortName
		,	const std::string & _messageIdentifier
	);

	void printMessage(
			MessageLevel::Enum _messageLevel
		,	const std::string & _message
	);

	void printSelectedCommand( const std::string & _command );

/*---------------------------------------------------------------------------*/

	void setConsoleTextColor( MessageLevel::Enum _messageLevel );

	void setConsoleDefaultTextColor();

	void setConsoleIdentifierTextColor();

/*---------------------------------------------------------------------------*/

	void clearLine();

	void resetCursorLinePosition();

/*---------------------------------------------------------------------------*/

	unsigned short m_background;

	::HANDLE m_stdoutHandle;

	std::string m_inputHolder;

/*---------------------------------------------------------------------------*/

	boost::function< void () > m_inputLocker;

	boost::function< void () > m_inputUnlocker;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_WRITER_HPP__
