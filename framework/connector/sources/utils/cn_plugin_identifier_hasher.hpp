// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_HASHER_HPP__
#define __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_HASHER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

#include "framework/tools/common_headers/hash_combiner.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Utils {

/*---------------------------------------------------------------------------*/

class PluginIdentifierHasher
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	CommonHeaders::HashCombiner::HashResult
	operator () ( const PluginIdentifier & _pluginIdentifier ) const
	{
		boost::hash< PluginIdentifier::MajorType > majorHasher;
		boost::hash< PluginIdentifier::MinorType > minorHasher;
		boost::hash< PluginIdentifier::SubMinorType > subMinorHasher;

		CommonHeaders::HashCombiner hashCombiner;

		hashCombiner.assign(
			majorHasher(
				_pluginIdentifier.getMajorValue()
			)
		);

		int nMinorValues = _pluginIdentifier.getMinorValuesCount();
		for ( int i = 0; i < nMinorValues; ++i )
			hashCombiner.assign(
				minorHasher(
					_pluginIdentifier.getMinorValue( i )
				)
			);

		int nSubMinorValues = _pluginIdentifier.getSubMinorValuesCount();
		for ( int i = 0; i < nSubMinorValues; ++i )
			hashCombiner.assign(
				subMinorHasher(
					_pluginIdentifier.getSubMinorValue( i )
				)
			);

		return hashCombiner.getResult();

	} // operator ()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Utils
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_PLUGIN_IDENTIFIER_HASHER_HPP__
