// (C) 2013 Ample Serializator

#ifndef __AMPLE_SERIALIZATOR_BINARY_SAVER_HPP__
#define __AMPLE_SERIALIZATOR_BINARY_SAVER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/serializator/api/sr_export.hpp"

/*---------------------------------------------------------------------------*/

class QDataStream;

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

class SERIALIZATOR_API BinarySaver
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	BinarySaver( QDataStream & _outputStream );

	virtual ~BinarySaver();

/*---------------------------------------------------------------------------*/

	void save( bool _value );

	void save( char _value );

	void save( unsigned char _value );

	void save( short _value );

	void save( unsigned short _value );

	void save( int _value );

	void save( unsigned int _value );

	void save( long _value );

	void save( unsigned long _value );

	void save( long long _value );

	void save( unsigned long long _value );

	void save( float _value );

	void save( double _value );

	void save( const std::string & _value );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	QDataStream & m_outputStream;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_SERIALIZATOR_BINARY_SAVER_HPP__
