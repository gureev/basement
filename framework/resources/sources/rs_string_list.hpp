// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_STRING_LIST_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_STRING_LIST_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/api/rs_string.hpp"
#include "framework/resources/api/rs_access_type.hpp"

#include "framework/tools/common_headers/containers/vector.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class String;

/*---------------------------------------------------------------------------*/

class StringList
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	StringList();

	~StringList();

/*---------------------------------------------------------------------------*/

	void add(
			const std::string & _sectionName
		,	const std::string & _identifier
		,	const std::string & _value
	);

/*---------------------------------------------------------------------------*/

	const Resources::String & get( AccessType::Enum _accessType ) const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	// TODO: Memory optimization
	typedef
		CommonHeaders::Containers::Vector<
			boost::shared_ptr< Implementation::String >
		>
		Strings;

	Strings m_strings;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_STRING_LIST_HPP__
