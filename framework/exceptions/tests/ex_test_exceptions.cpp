// (C) 2013 Ample Exceptions

#include "framework/exceptions/sources/ph/ex_ph.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"
#include "framework/exceptions/api/ex_internal_error.hpp"

#include "framework/exceptions/tests/ex_test_fixture.hpp"

#include <boost/test/unit_test.hpp>

#include <QtCore/qfile.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		1) Internal error
			1.1) catch as is
			1.2) catch as is (thrown by macro)
			1.3) catch as BaseException
			1.4) catch as boost::exception
			1.5) catch as std::exception
		2) Exception
			2.1) catch as is
			2.2) catch as is (thrown by macro)
			2.3) catch as BaseException
			2.4) catch as InternalError
			2.5) catch as boost::exception
			2.6) catch as std::exception

*/

/*---------------------------------------------------------------------------*/

EX_DECLARE_BASE_EXCEPTION(
		BaseTestException
	,	ExceptionsVisitor
);

EX_DECLARE_0_ARGUMENT(
		BaseTestException
	,	TestException
);

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Exceptions, Fixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_1_1_InternalError_CatchAsIs )
{
	try
	{
		throw InternalError(
				__FILE__
			,	__LINE__
		);
	}
	catch ( const InternalError & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Internal error catch corrupted!"
		);
	}

} // Exceptions_InternalError_CatchAsIs

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_1_2_InternalError_CatchAsIsThrownByMacro )
{
	try
	{
		EX_INTERNAL_ERROR
	}
	catch ( const InternalError & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Internal error catch corrupted!"
		);
	}

} // Exceptions_InternalError_CatchAsIsThrownByMacro

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_1_3_InternalError_CatchAsBaseException )
{
	try
	{
		EX_INTERNAL_ERROR
	}
	catch ( const Exceptions::BaseException & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Internal error catch corrupted!"
		);
	}

} // Exceptions_1_3_InternalError_CatchAsBaseException

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_1_4_InternalError_CatchAsBoostException )
{
	try
	{
		EX_INTERNAL_ERROR
	}
	catch ( const boost::exception & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Internal error catch corrupted!"
		);
	}

} // Exceptions_1_4_InternalError_CatchAsBoostException

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_1_5_InternalError_CatchAsStdException )
{
	try
	{
		EX_INTERNAL_ERROR
	}
	catch ( const std::exception & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Internal error catch corrupted!"
		);
	}

} // Exceptions_1_5_InternalError_CatchAsStdException

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_2_1_Exception_CatchAsIs )
{
	try
	{
		throw TestException(
				__FILE__
			,	__LINE__
		);
	}
	catch ( const TestException & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}

} // Exceptions_2_1_Exception_CatchAsIs

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_2_2_Exception_CatchAsIsThrownByMacro )
{
	try
	{
		EX_THROW_0_ARGUMENT( TestException );
	}
	catch ( const TestException & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}

} // Exceptions_2_2_Exception_CatchAsIsThrownByMacro

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_2_3_Exception_CatchAsBaseException )
{
	try
	{
		EX_THROW_0_ARGUMENT( TestException );
	}
	catch ( const BaseException & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}

} // Exceptions_2_3_Exception_CatchAsBaseException

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_2_4_Exception_CatchAsInternalError )
{
	try
	{
		EX_THROW_0_ARGUMENT( TestException );
	}
	catch ( const InternalError & /*_e*/ )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}
	catch ( const BaseException & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}

} // Exceptions_2_4_Exception_CatchAsInternalError

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_2_5_Exception_CatchAsBoostException )
{
	try
	{
		EX_THROW_0_ARGUMENT( TestException );
	}
	catch ( const boost::exception & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}

} // Exceptions_2_5_Exception_CatchAsBoostException

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Exceptions_2_6_Exception_CatchAsStdException )
{
	try
	{
		EX_THROW_0_ARGUMENT( TestException );
	}
	catch ( const std::exception & /*_e*/ )
	{
		return;
	}
	catch ( ... )
	{
		BOOST_REQUIRE_MESSAGE(
				false
			,	"Exception catch corrupted!"
		);
	}

} // Exceptions_2_6_Exception_CatchAsStdException

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Exceptions

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Exceptions
} // namespace Ample
