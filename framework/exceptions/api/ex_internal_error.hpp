// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_INTERNAL_ERROR_HPP__
#define __AMPLE_EXCEPTIONS_INTERNAL_ERROR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/exceptions/api/ex_base_exception.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

class EXCEPTIONS_API InternalError
	:	public BaseException
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	InternalError(
			const std::string & _throwFileName
		,	int _throwLineNumber
	);

/*---------------------------------------------------------------------------*/

	/*virtual*/ const std::string & throwFileName() const;

	/*virtual*/ const int & throwLineNumber() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ const char * what() const;

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	std::string m_throwFileName;

	int m_throwLineNumber;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_INTERNAL_ERROR_HPP__
