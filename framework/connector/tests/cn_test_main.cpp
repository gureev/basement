// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

/*---------------------------------------------------------------------------*/

#define BOOST_TEST_MODULE Connector

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

TEST_RUNNER_ENTRY_POINT()

/*---------------------------------------------------------------------------*/
