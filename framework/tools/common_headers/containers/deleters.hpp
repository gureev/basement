// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_DELETERS_HPP__
#define __AMPLE_COMMON_HEADERS_DELETERS_HPP__

/*----------------------------------------------------------------------------*/

#include "framework/tools/common_headers/meta.hpp"

/*----------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {

/*----------------------------------------------------------------------------*/

template< typename _Object >
class DefaultDeleter
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	void operator() ( _Object * _object ) const
	{
		boost::checked_delete( _object );
	}

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template<
		typename _PairObject
	// TODO: Pair delete policy
>
class PairDeleter
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	void operator() ( _Object * _object ) const
	{

	}

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*----------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_DELETERS_HPP__
