// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_property_sections_container.hpp"

#include "framework/resources/tests/rs_test_property_fixture.hpp"
#include "framework/resources/tests/rs_test_string_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Access type:
			1) Properties
				1.1) Replace property
				1.2) New property
				1.3) Replace and new property
				1.4) Double replace property
				1.5) Replace new property
				1.6) Replace section
				1.7) New section
				1.8) Replace and new section
				1.9) Double replace section
				1.10) Replace new section
			2) Strings
				2.1) Replace string
				2.2) New string
				2.3) Replace and new string
				2.4) Double replace string
				2.5) Replace new string
				2.6) Replace section
				2.7) New section
				2.8) Replace and new section
				2.9) Double replace section
				2.10) Replace new section

*/

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE( Suite_AccessType )

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_AccessType_Properties, PropertyFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_1_OneProperty )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "top" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "top" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "top" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop", "first" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "top" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_1_OneProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_2_NewProperty )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop_1", "first" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first" );

		BOOST_REQUIRE_THROW(
				section.getResource( "prop_2" )
			,	Exceptions::MissingResource
		)
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop_2", "second" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		const Property & value3 = section.getResource(
				"prop_2"
			,	AccessType::FirstAvailable
		);

		const Property & value4 = section.getResource(
				"prop_2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "second" );
		BOOST_REQUIRE_EQUAL( value4.asString(), "second" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_2_NewProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_3_ReplaceAndNewProperty )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop_1", "first_1" )
	.end();

	/*-------- Test actions -----*/

try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first_1" );

		BOOST_REQUIRE_THROW(
				section.getResource( "prop_2" )
			,	Exceptions::MissingResource
		)
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop_1", "first_2" )
		.property( "prop_2", "second" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		const Property & value3 = section.getResource(
				"prop_2"
			,	AccessType::FirstAvailable
		);

		const Property & value4 = section.getResource(
				"prop_2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first_2" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first_1" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "second" );
		BOOST_REQUIRE_EQUAL( value4.asString(), "second" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_3_ReplaceAndNewProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_4_DoubleReplaceProperty )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop", "first_1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first_1" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop", "first_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first_2" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop", "first_3" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first_3" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_4_DoubleReplaceProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_5_ReplaceNewProperty )
{
	/*---------- Setup ----------*/

	section( "test" )
		.property( "prop_1", "first" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first" );

		BOOST_REQUIRE_THROW(
				section.getResource( "prop_2" )
			,	Exceptions::MissingResource
		)
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop_2", "second_1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		const Property & value3 = section.getResource(
				"prop_2"
			,	AccessType::FirstAvailable
		);

		const Property & value4 = section.getResource(
				"prop_2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "second_1" );
		BOOST_REQUIRE_EQUAL( value4.asString(), "second_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test" )
		.property( "prop_2", "second_2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );

		const Property & value1 = section.getResource(
				"prop_1"
			,	AccessType::FirstAvailable
		);

		const Property & value2 = section.getResource(
				"prop_1"
			,	AccessType::TopAvailable
		);

		const Property & value3 = section.getResource(
				"prop_2"
			,	AccessType::FirstAvailable
		);

		const Property & value4 = section.getResource(
				"prop_2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value2.asString(), "first" );
		BOOST_REQUIRE_EQUAL( value3.asString(), "second_2" );
		BOOST_REQUIRE_EQUAL( value4.asString(), "second_1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_5_ReplaceNewProperty

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_6_OneSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_6_OneSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_7_NewSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "section_1" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section_1" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "section_1" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_7_NewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_8_ReplaceAndNewSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "section_1" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section_1" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section_1" ).end()
		.section( "section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "section_1" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_8_ReplaceAndNewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_9_DoubleReplaceSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_9_DoubleReplaceSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Properties_1_10_ReplaceNewSection )
{
	/*---------- Setup ----------*/

	section( "parent" )
		.section( "section_1" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section = parent.getSection( "section_1" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "section_2" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent" )
		.section( "section_2" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );

		auto & section1 = parent.getSection( "section_2" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Properties_1_10_ReplaceNewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_AccessType_Properties

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_AccessType_Strings, StringFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_1_OneString )
{
	/*---------- Setup ----------*/

	section( "test", "test" )
		.string( "string", "id1", "value1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string", "id2", "value2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id2" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_1_OneString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_2_NewString )
{
	/*---------- Setup ----------*/

	section( "test", "test" )
		.string( "string", "id1", "value1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );

		BOOST_REQUIRE_THROW(
				section.getResource( "string2" )
			,	Exceptions::MissingResource
		)
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string2", "id2", "value2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string"
			,	AccessType::TopAvailable
		);

		const String & value3 = section.getResource(
				"string2"
			,	AccessType::FirstAvailable
		);

		const String & value4 = section.getResource(
				"string2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "id2" );

		BOOST_REQUIRE_EQUAL( value4.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value4.getIdentifier(), "id2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_2_NewString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_3_ReplaceAndNewString )
{
	/*---------- Setup ----------*/

	section( "test", "test" )
		.string( "string1", "id1", "value1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string1", "id1", "value11" )
		.string( "string2", "id2", "value2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		const String & value3 = section.getResource(
				"string2"
			,	AccessType::FirstAvailable
		);

		const String & value4 = section.getResource(
				"string2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value11" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "id2" );

		BOOST_REQUIRE_EQUAL( value4.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value4.getIdentifier(), "id2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_3_ReplaceAndNewString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_4_DoubleReplaceString )
{
	/*---------- Setup ----------*/

	section( "test", "test" )
		.string( "string1", "id1", "value1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string1", "id2", "value2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id2" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string1", "id3", "value3" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value3" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id3" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_4_DoubleReplaceString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_5_ReplaceNewString )
{
	/*---------- Setup ----------*/

	section( "test", "test" )
		.string( "string1", "id1", "value1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 1 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );

		BOOST_REQUIRE_THROW(
				section.getResource( "string2" )
			,	Exceptions::MissingResource
		)
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string2", "id2", "value2" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		const String & value3 = section.getResource(
				"string2"
			,	AccessType::FirstAvailable
		);

		const String & value4 = section.getResource(
				"string2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "id2" );

		BOOST_REQUIRE_EQUAL( value4.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value4.getIdentifier(), "id2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "test", "test" )
		.string( "string2", "id3", "value3" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & section = sections.getSection( "test" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 2 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );

		const String & value1 = section.getResource(
				"string1"
			,	AccessType::FirstAvailable
		);

		const String & value2 = section.getResource(
				"string1"
			,	AccessType::TopAvailable
		);

		const String & value3 = section.getResource(
				"string2"
			,	AccessType::FirstAvailable
		);

		const String & value4 = section.getResource(
				"string2"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( value1.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value1.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value2.getValue(), "value1" );
		BOOST_REQUIRE_EQUAL( value2.getIdentifier(), "id1" );

		BOOST_REQUIRE_EQUAL( value3.getValue(), "value3" );
		BOOST_REQUIRE_EQUAL( value3.getIdentifier(), "id3" );

		BOOST_REQUIRE_EQUAL( value4.getValue(), "value2" );
		BOOST_REQUIRE_EQUAL( value4.getIdentifier(), "id2" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_5_ReplaceNewString

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_6_OneSection )
{
	/*---------- Setup ----------*/

	section( "parent", "test" )
		.section( "section", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_6_OneSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_7_NewSection )
{
	/*---------- Setup ----------*/

	section( "parent", "test" )
		.section( "section_1", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section_1" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section_2", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section1 = parent.getSection( "section_1" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_7_NewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_8_ReplaceAndNewSection )
{
	/*---------- Setup ----------*/

	section( "parent", "test" )
		.section( "section_1", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section_1" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section_1", "test" ).end()
		.section( "section_2", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section1 = parent.getSection( "section_1" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_8_ReplaceAndNewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_9_DoubleReplaceSection )
{
	/*---------- Setup ----------*/

	section( "parent", "test" )
		.section( "section", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_9_DoubleReplaceSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( AccessType_Strings_1_10_ReplaceNewSection )
{
	/*---------- Setup ----------*/

	section( "parent", "test" )
		.section( "section_1", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 1 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section = parent.getSection( "section_1" );

		BOOST_REQUIRE_EQUAL( section.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section_2", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section1 = parent.getSection( "section_2" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

	/*---------- Setup ----------*/

	clear();

	section( "parent", "test" )
		.section( "section_2", "test" ).end()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & sections = runTest();

		BOOST_REQUIRE_EQUAL( sections.getSectionsCount(), 1 );

		auto & parent = sections.getSection( "parent" );

		BOOST_REQUIRE_EQUAL( parent.getSectionsCount(), 2 );
		BOOST_REQUIRE_EQUAL( parent.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( parent.getShortName(), "test" );

		auto & section1 = parent.getSection( "section_2" );
		auto & section2 = parent.getSection( "section_2" );

		BOOST_REQUIRE_EQUAL( section1.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section1.getShortName(), "test" );

		BOOST_REQUIRE_EQUAL( section2.getSectionsCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getResourcesCount(), 0 );
		BOOST_REQUIRE_EQUAL( section2.getShortName(), "test" );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // AccessType_Strings_1_10_ReplaceNewSection

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_AccessType_Strings

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_AccessType

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
