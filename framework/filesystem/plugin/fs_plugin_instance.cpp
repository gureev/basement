// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_accessor_impl.hpp"

#include "framework/filesystem/plugin/fs_plugin_instance.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Plugin {

/*---------------------------------------------------------------------------*/

CONNECTOR_DECLARE_PLUGIN_FACTORY()

/*---------------------------------------------------------------------------*/

Instance::Instance()
	:	m_accessor()
{
} // Instance::Instance

/*---------------------------------------------------------------------------*/

Instance::~Instance()
{
} // Instance::~Instance

/*---------------------------------------------------------------------------*/

void
Instance::onPluginLoad()
{
	m_accessor.reset( new Implementation::Accessor() );

} // Instance::onPluginLoad

/*---------------------------------------------------------------------------*/

boost::shared_ptr< Connector::Accessor >
Instance::getAccessor() const
{
	return m_accessor;

} // Instance::getAccessor

/*---------------------------------------------------------------------------*/

const Connector::PluginIdentifier &
Instance::getIdentifier() const
{
	return Identifier;

} // Instance::getIdentifier

/*---------------------------------------------------------------------------*/

} // namespace Plugin
} // namespace FileSystem
} // namespace Ample
