// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_RELATIVE_PATH_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_RELATIVE_PATH_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/sources/fs_base_path.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class BaseRelativePath
	:	public BasePath
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	BaseRelativePath( int _componentsCount );

	BaseRelativePath( const BaseRelativePath & _other );

/*---------------------------------------------------------------------------*/

	/*virtual*/ bool isAbsolute() const;

	/*virtual*/ bool isRelative() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ Ptr clone() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ char getDriveLetter() const;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_BASE_RELATIVE_PATH_HPP__
