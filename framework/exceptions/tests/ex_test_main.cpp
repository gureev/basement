// (C) 2013 Ample Exceptions

#include "framework/exceptions/sources/ph/ex_ph.hpp"

/*---------------------------------------------------------------------------*/

#define BOOST_TEST_MODULE Exceptions

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

#include "framework/tools/test_runner/api/tr_entry_point.hpp"

TEST_RUNNER_ENTRY_POINT()

/*---------------------------------------------------------------------------*/
