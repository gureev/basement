// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_RESOURCES_HPP__
#define __AMPLE_CONNECTOR_RESOURCES_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Constants {

/*---------------------------------------------------------------------------*/

	extern const int MajorFormatVersion;

	extern const int MinorFormatVersion;

	extern const char * PluginFactoryFunction;

/*---------------------------------------------------------------------------*/

} // namespace Constants

/*---------------------------------------------------------------------------*/

namespace Paths {

/*---------------------------------------------------------------------------*/

	extern const QString PluginsInfo;

/*---------------------------------------------------------------------------*/

} // namespace Paths

/*---------------------------------------------------------------------------*/

namespace Xml {

/*---------------------------------------------------------------------------*/

namespace Tags {

/*---------------------------------------------------------------------------*/

	extern const QString Plugins;
	extern const QString Plugin;

/*---------------------------------------------------------------------------*/

} // namespace Tags

/*---------------------------------------------------------------------------*/

namespace Attributes {

/*---------------------------------------------------------------------------*/

	extern const QString Version;

	extern const QString PluginName;
	extern const QString PluginIdentifier;
	extern const QString PluginLazyLoad;

/*---------------------------------------------------------------------------*/

} // namespace Attributes

/*---------------------------------------------------------------------------*/

} // namespace Xml

/*---------------------------------------------------------------------------*/

namespace Templates {

/*---------------------------------------------------------------------------*/

	extern const QString Plugin;

	extern const QString PluginsInfoFile;

/*---------------------------------------------------------------------------*/

} // namespace Templates

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_RESOURCES_HPP__
