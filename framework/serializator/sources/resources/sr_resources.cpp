// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

#include "framework/serializator/sources/resources/sr_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Xml {

/*---------------------------------------------------------------------------*/

namespace Attributes {

/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/

} // namespace Attributes

/*---------------------------------------------------------------------------*/

} // namespace Xml

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Serializator
} // namespace Ample
