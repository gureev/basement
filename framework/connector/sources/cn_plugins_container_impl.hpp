// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_CONTAINER_HPP__
#define __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_CONTAINER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"
#include "framework/connector/api/cn_plugins_container.hpp"
#include "framework/connector/api/cn_plugin_load_policy.hpp"

#include "framework/connector/sources/utils/cn_plugin_identifier_hasher.hpp"
#include "framework/connector/sources/utils/cn_plugin_identifier_comparator.hpp"

#include "framework/tools/common_headers/containers/hash_map.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PluginInfo;

/*---------------------------------------------------------------------------*/

class PluginsContainer
	:	public Connector::PluginsContainer
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	static PluginsContainer & getInstance();

/*---------------------------------------------------------------------------*/

	void addPlugin(
			const QString & _name
		,	const PluginIdentifier & _identifier
		,	PluginLoadPolicy::Enum _loadPolicy
	);

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Connector::PluginInfo >
		findPlugin( const PluginIdentifier & _pluginIdentifier ) const;

/*---------------------------------------------------------------------------*/

	int getPluginsCount() const;

/*---------------------------------------------------------------------------*/

	void loadInstantPlugins();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	typedef
		CommonHeaders::Containers::HashMap<
				PluginIdentifier
			,	boost::shared_ptr< Implementation::PluginInfo >
			,	Utils::PluginIdentifierHasher
			,	Utils::PluginIdentifierComparator
		>
		Plugins;
	Plugins m_plugins;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_CONTAINER_HPP__
