// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_INFO_RESTORER_HPP__
#define __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_INFO_RESTORER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"
#include "framework/connector/api/cn_plugin_load_policy.hpp"

#include "framework/serializator/api/sr_xml_restorer.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PluginsContainer;

/*---------------------------------------------------------------------------*/

class PluginsInfoRestorer
	:	public Serializator::XmlRestorer
{

/*---------------------------------------------------------------------------*/

	typedef
		Serializator::XmlRestorer
		BaseRestorer;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PluginsInfoRestorer(
			Implementation::PluginsContainer & _pluginsContainer
		,	QIODevice & _pluginsInfoFile
	);

	/*virtual*/ ~PluginsInfoRestorer();

/*---------------------------------------------------------------------------*/

	void run();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void resolvePluginsPath();

/*---------------------------------------------------------------------------*/

	const QString restoreName( const QDomElement & _tag );

	PluginLoadPolicy::Enum restoreLoadPolicy( const QDomElement & _tag );

	const PluginIdentifier restoreIdentifier( const QDomElement & _tag );

/*---------------------------------------------------------------------------*/

	Implementation::PluginsContainer & m_pluginsContainer;

	QString m_pluginsPath;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_IMPLEMENTATION_PLUGINS_INFO_RESTORER_HPP__
