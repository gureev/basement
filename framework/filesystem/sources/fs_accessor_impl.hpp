// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_IMPLEMENTATION_ACCESSOR_HPP__
#define __AMPLE_FILESYSTEM_IMPLEMENTATION_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_accessor.hpp"

#include <QtCore/qfile.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class Accessor
	:	public FileSystem::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Accessor();

	/*virtual*/ ~Accessor();

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Path > createPath( const std::string & _path ) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void setCurrentPath( const Path & _path );

	/*virtual*/ const Path & getCurrentPath() const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ bool exists( const Path & _path ) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Path > getAbsolutePath( const Path & _path ) const;

	/*virtual*/ boost::shared_ptr< Path > getAbsolutePath(
			const Path & _path
		,	const Path & _pathFrom
	) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< Path > getRelativePath( const Path & _path ) const;

	/*virtual*/ boost::shared_ptr< Path > getRelativePath(
			const Path & _path
		,	const Path & _pathTo
	) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ bool canConvertToRelativePath(
			const Path & _path
		,	const Path & _pathTo
	) const;

	/*virtual*/ bool canRead( const Path & _path ) const;

	/*virtual*/ bool canWrite( const Path & _path ) const;

	/*virtual*/ bool canDelete( const Path & _path ) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ bool isFile( const Path & _path ) const;

	/*virtual*/ bool isFolder( const Path & _path ) const;

	/*virtual*/ bool isLink( const Path & _path ) const;

/*---------------------------------------------------------------------------*/

	/*virtual*/ void remove( const Path & _path );

	/*virtual*/ void rename(
			const Path & _path
		,	const std::string & _newName
	);

	/*virtual*/ void move(
			const Path & _path
		,	const Path & _targetFolderPath
	);

	/*virtual*/ void copy(
			const Path & _path
		,	const Path & _targetFolderPath
	);

/*---------------------------------------------------------------------------*/

	/*virtual*/ void makeReadable( const Path & _path );

	/*virtual*/ void makeUnreadable( const Path & _path );

	/*virtual*/ void makeWritable( const Path & _path );

	/*virtual*/ void makeUnwritable( const Path & _path );

/*---------------------------------------------------------------------------*/

	/*virtual*/ boost::shared_ptr< FileSystem::FileHandle > openFile(
			boost::shared_ptr< FileSystem::Path > _path
		,	OpenMode::Enum _openMode
	);

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	void setPermissions(
			const QString & _path
		,	QFile::Permissions _permissions
	);

/*---------------------------------------------------------------------------*/

	int readablePermissionMask() const;

	int writablePermissionMask() const;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_IMPLEMENTATION_ACCESSOR_HPP__
