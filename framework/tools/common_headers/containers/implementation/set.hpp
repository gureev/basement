// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_CONTAINERS_IMPLEMENTATION_SET_HPP__
#define __AMPLE_COMMON_HEADERS_CONTAINERS_IMPLEMENTATION_SET_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/optional.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {
namespace Containers {
namespace Implementation {

/*---------------------------------------------------------------------------*/

template< typename _BaseContainer >
class Set
	:	public _BaseContainer
{

/*---------------------------------------------------------------------------*/

	typedef
		typename _BaseContainer::key_type
		KeyType;

	typedef
		typename _BaseContainer::value_type
		ValueType;

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	Set()
		:	_BaseContainer()
	{}

/*---------------------------------------------------------------------------*/

	int size() const
	{
		return static_cast< int >( _BaseContainer::size() );
	}

/*---------------------------------------------------------------------------*/

	bool contains( const KeyType & _key ) const
	{
		return this->find( _key ) != this->end();
	}

/*---------------------------------------------------------------------------*/

	boost::optional< const ValueType & > getValue( const KeyType & _key ) const
	{
		typedef
			boost::optional< const ValueType & >
			ReturnType;

		typename _BaseContainer::const_iterator it = this->find( _key );

		return	it != this->end()
			?	ReturnType( * it )
			:	ReturnType()
		;
	}

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Containers
} // namespace CommonHeaders
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_CONTAINERS_IMPLEMENTATION_SET_HPP__
