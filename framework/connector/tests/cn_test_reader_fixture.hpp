// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_TEST_RESTORER_FIXTURE_HPP__
#define __AMPLE_CONNECTOR_TEST_RESTORER_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_plugin_identifier.hpp"

#include "framework/connector/sources/cn_plugin_editor.hpp"
#include "framework/connector/sources/cn_plugins_container_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Tests {

/*---------------------------------------------------------------------------*/

class RestorerFixture
	:	public Implementation::PluginEditor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	RestorerFixture();

/*---------------------------------------------------------------------------*/

	PluginIdentifier generatePluginIdentifier() const;

/*---------------------------------------------------------------------------*/

	const Implementation::PluginsContainer & runTest();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::scoped_ptr< Implementation::PluginsContainer > m_plugins;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_TEST_RESTORER_FIXTURE_HPP__
