// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_properties_restorer.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

#include <QtXml/qdom.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PropertiesRestorer::PropertiesRestorer(
		PropertySectionsContainer & _sectionsContainer
	,	QIODevice & _stream
)
	:	BaseRestorer( _stream )
	,	m_sectionsContainer( _sectionsContainer )
{
} // PropertiesRestorer::PropertiesRestorer

/*---------------------------------------------------------------------------*/

PropertiesRestorer::~PropertiesRestorer()
{
} // PropertiesRestorer::~PropertiesRestorer

/*---------------------------------------------------------------------------*/

void
PropertiesRestorer::restoreProperty(
		PropertySection & _section
	,	QDomElement & _propertyTag
)
{
	QString restoredName = _propertyTag.attribute( Resources::Xml::Attributes::PropertyName );
	QString value = _propertyTag.attribute( Resources::Xml::Attributes::PropertyValue );

	if (	restoredName.isEmpty()
		||	value.isEmpty()
	)
		EX_THROW_1_ARGUMENT(
				Exceptions::BrokenAttribute
			,	_propertyTag.lineNumber()
		);

	std::string propertyName = restoredName.toAscii().data();

	if ( m_restoredProperties.getValue( propertyName ) )
		EX_THROW_1_ARGUMENT(
				Exceptions::DuplicateResource
			,	propertyName
		);

	m_restoredProperties.insert( propertyName );

	_section.addResource(
			propertyName
		,	value.toAscii().data()
	);

} // PropertiesRestorer::restoreProperty

/*---------------------------------------------------------------------------*/

void
PropertiesRestorer::restoreSection(
		PropertySection & _section
	,	QDomElement & _sectionTag
)
{
	m_restoredProperties.clear();

	QDomElement propertyTag = _sectionTag.firstChildElement( Resources::Xml::Tags::Property );

	while ( ! propertyTag.isNull() )
	{
		restoreProperty(
				_section
			,	propertyTag
		);

		propertyTag = propertyTag.nextSiblingElement( propertyTag.tagName() );
	}

	restoreNestedSections(
			_section
		,	_sectionTag
	);

} // PropertiesRestorer::restoreSection

/*---------------------------------------------------------------------------*/

template<
		typename _SectionsContainer
	,	typename _SectionTag
>
void
PropertiesRestorer::restoreNestedSections(
		_SectionsContainer & _section
	,	_SectionTag & _sectionTag
)
{
	RestoredResources restoredSections;

	QDomElement sectionTag = _sectionTag.firstChildElement( Resources::Xml::Tags::Section );

	while ( ! sectionTag.isNull() )
	{
		QString restoredSectionName = sectionTag.attribute( Resources::Xml::Attributes::SectionName );

		if ( restoredSectionName.isEmpty() )
			EX_THROW_1_ARGUMENT(
					Exceptions::BrokenAttribute
				,	sectionTag.lineNumber()
			);

		std::string sectionName = restoredSectionName.toAscii().data();

		if ( restoredSections.getValue( sectionName ) )
			EX_THROW_1_ARGUMENT(
					Exceptions::DuplicateSection
				,	sectionName
			);

		restoredSections.insert( sectionName );

		PropertySection & section = _section.addSection( sectionName );

		restoreSection(
				section
			,	sectionTag
		);

		sectionTag = sectionTag.nextSiblingElement( sectionTag.tagName() );
	}

} // PropertiesRestorer::restoreNestedSections

/*---------------------------------------------------------------------------*/

void
PropertiesRestorer::run()
{
	restoreNestedSections(
			m_sectionsContainer
		,	m_document
	);

} // PropertiesRestorer::run

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
