// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_TEST_PROPERTY_VALIDATOR_FIXTURE_HPP__
#define __AMPLE_RESOURCES_TEST_PROPERTY_VALIDATOR_FIXTURE_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/resources/sources/rs_property_validator_impl.hpp"

#include "framework/resources/tests/rs_test_property_fixture.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

class PropertyValidatorFixture
	:	public PropertyFixture

{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	PropertyValidatorFixture();

/*---------------------------------------------------------------------------*/

	Resources::PropertyValidator & getValidator();

/*---------------------------------------------------------------------------*/

	void validateProperty();

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	boost::scoped_ptr< Implementation::PropertyValidator > m_validator;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_TEST_PROPERTY_VALIDATOR_FIXTURE_HPP__
