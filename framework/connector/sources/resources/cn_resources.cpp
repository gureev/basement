// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/sources/resources/cn_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Constants {

/*---------------------------------------------------------------------------*/

	const int MajorFormatVersion = 1;

	const int MinorFormatVersion = 0;

	const char * PluginFactoryFunction = "createPlugin";

/*---------------------------------------------------------------------------*/

} // namespace Constants

/*---------------------------------------------------------------------------*/

namespace Paths {

/*---------------------------------------------------------------------------*/

	const QString PluginsInfo = ":/configs/plugins";

/*---------------------------------------------------------------------------*/

} // namespace Paths

/*---------------------------------------------------------------------------*/

namespace Xml {

/*---------------------------------------------------------------------------*/

namespace Tags {

/*---------------------------------------------------------------------------*/

	const QString Plugins = "plugins";
	const QString Plugin = "plugin";

/*---------------------------------------------------------------------------*/

} // namespace Tags

/*---------------------------------------------------------------------------*/

namespace Attributes {

/*---------------------------------------------------------------------------*/

	const QString Version = "version";

	const QString PluginName = "name";
	const QString PluginIdentifier = "identifier";
	const QString PluginLazyLoad = "lazyload";

/*---------------------------------------------------------------------------*/

} // namespace Attributes

/*---------------------------------------------------------------------------*/

} // namespace Xml

/*---------------------------------------------------------------------------*/

namespace Templates {

/*---------------------------------------------------------------------------*/

	const QString Plugin =
		QString( "<%1 %2=\"%5\" %3=\"%6\" %4=\"%7\"/>" ).arg(
				Xml::Tags::Plugin
			,	Xml::Attributes::PluginName
			,	Xml::Attributes::PluginIdentifier
			,	Xml::Attributes::PluginLazyLoad
		);

	const QString PluginsInfoFile =
		QString( "<%1>\n%2</%1>\n" ).arg( Xml::Tags::Plugins );

/*---------------------------------------------------------------------------*/

} // namespace Templates

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Connector
} // namespace Ample
