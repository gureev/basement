// (C) 2013 Ample FileSystem

#ifndef __AMPLE_FILESYSTEM_ACCESSOR_HPP__
#define __AMPLE_FILESYSTEM_ACCESSOR_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/filesystem/api/fs_open_mode.hpp"

#include "framework/connector/api/cn_accessor.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {

/*---------------------------------------------------------------------------*/

class Path;
class FileHandle;

/*---------------------------------------------------------------------------*/

class Accessor
	:	public Connector::Accessor
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< Path > createPath( const std::string & _path ) const = 0;

/*---------------------------------------------------------------------------*/

	virtual void setCurrentPath( const Path & _path ) = 0;

	virtual const Path & getCurrentPath() const = 0;

/*---------------------------------------------------------------------------*/

	virtual bool exists( const Path & _path ) const = 0;

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< Path > getAbsolutePath( const Path & _path ) const = 0;

	virtual boost::shared_ptr< Path > getAbsolutePath(
			const Path & _path
		,	const Path & _pathFrom
	) const = 0;

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< Path > getRelativePath( const Path & _path ) const = 0;

	virtual boost::shared_ptr< Path > getRelativePath(
			const Path & _path
		,	const Path & _pathTo
	) const = 0;

/*---------------------------------------------------------------------------*/

	virtual bool canConvertToRelativePath(
			const Path & _path
		,	const Path & _pathTo
	) const = 0;

	virtual bool canRead( const Path & _path ) const = 0;

	virtual bool canWrite( const Path & _path ) const = 0;

	virtual bool canDelete( const Path & _path ) const = 0;

/*---------------------------------------------------------------------------*/

	virtual bool isFile( const Path & _path ) const = 0;

	virtual bool isFolder( const Path & _path ) const = 0;

	virtual bool isLink( const Path & _path ) const = 0;

/*---------------------------------------------------------------------------*/

	virtual void remove( const Path & _path ) = 0;

	virtual void rename(
			const Path & _path
		,	const std::string & _newName
	) = 0;

	virtual void move(
			const Path & _path
		,	const Path & _targetFolderPath
	) = 0;

	virtual void copy(
			const Path & _path
		,	const Path & _targetFolderPath
	) = 0;

/*---------------------------------------------------------------------------*/

	virtual void makeReadable( const Path & _path ) = 0;

	virtual void makeUnreadable( const Path & _path ) = 0;

	virtual void makeWritable( const Path & _path ) = 0;

	virtual void makeUnwritable( const Path & _path ) = 0;

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< FileHandle > openFile(
			boost::shared_ptr< Path > _path
		,	OpenMode::Enum _openMode
	) = 0;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace FileSystem
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_FILESYSTEM_ACCESSOR_HPP__
