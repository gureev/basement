// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/tests/fs_test_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:

*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_FileSystem, Fixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( FileSystem_TEST1 )
{
} // FileSystem_TEST1

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_FileSystem

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace FileSystem
} // namespace Ample
