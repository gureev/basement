// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

#include "framework/serializator/api/sr_binary_restorer.hpp"

#include <QtCore/qdatastream.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {

/*---------------------------------------------------------------------------*/

BinaryRestorer::BinaryRestorer( QDataStream & _inputStream )
	:	m_inputStream( _inputStream )
{
} // BinaryRestorer::BinaryRestorer

/*---------------------------------------------------------------------------*/

BinaryRestorer::~BinaryRestorer()
{
} // BinaryRestorer::~BinaryRestorer

/*---------------------------------------------------------------------------*/

} // namespace Serializator
} // namespace Ample
