// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_INTERNAL_MACROS_HPP__
#define __AMPLE_EXCEPTIONS_INTERNAL_MACROS_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/preprocessor.hpp>
#include <boost/static_assert.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_TYPE
	#error
#else
#define EX__INTERNAL_ARGUMENT_TYPE( _ARGUMENT )								\
	BOOST_PP_SEQ_ELEM( 0, _ARGUMENT )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_VALUE
	#error
#else
#define EX__INTERNAL_ARGUMENT_VALUE( _ARGUMENT )							\
	BOOST_PP_SEQ_ELEM( 1, _ARGUMENT )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_INPUT
	#error
#else
#define EX__INTERNAL_ARGUMENT_INPUT( _ARGUMENT )							\
	BOOST_PP_CAT( _ , EX__INTERNAL_ARGUMENT_VALUE( _ARGUMENT ) )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_MEMBER
	#error
#else
#define EX__INTERNAL_ARGUMENT_MEMBER( _ARGUMENT )							\
	BOOST_PP_CAT( m , EX__INTERNAL_ARGUMENT_INPUT( _ARGUMENT ) )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_FUNCTION
	#error
#else
#define EX__INTERNAL_ARGUMENT_FUNCTION( _ARGUMENT )							\
	BOOST_PP_CAT( EX__INTERNAL_ARGUMENT_VALUE( _ARGUMENT ) , () )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_INPUT_CONSTRUCTION
	#error
#else
#define EX__INTERNAL_ARGUMENT_INPUT_CONSTRUCTION( _R, _D, _N, _ARGUMENT )	\
	BOOST_PP_COMMA_IF( _N ) const EX__INTERNAL_ARGUMENT_TYPE( _ARGUMENT ) & EX__INTERNAL_ARGUMENT_INPUT( _ARGUMENT )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_INITIALIZATION_LIST_CONSTRUCTION
	#error
#else
#define EX__INTERNAL_ARGUMENT_INITIALIZATION_LIST_CONSTRUCTION( _R, _D, _N, _ARGUMENT ) \
	BOOST_PP_COMMA_IF( _N ) EX__INTERNAL_ARGUMENT_MEMBER( _ARGUMENT )( EX__INTERNAL_ARGUMENT_INPUT( _ARGUMENT ) )
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_CHEKER_CONSTRUCTION
	#error
#else
#define EX__INTERNAL_ARGUMENT_CHEKER_CONSTRUCTION( _R, _D, _N, _ARGUMENT )	\
	BOOST_STATIC_ASSERT_MSG(												\
			BOOST_PP_EQUAL(													\
					2														\
				,	BOOST_PP_SEQ_SIZE( _ARGUMENT )							\
			)																\
		,	"Poorly formed argument of exception. "							\
			"Expected syntax: (type)(argument)"								\
	);
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_FUNCTION_CONSTRUCTION
	#error
#else
#define EX__INTERNAL_ARGUMENT_FUNCTION_CONSTRUCTION( _R, _D, _N, _ARGUMENT ) \
	const EX__INTERNAL_ARGUMENT_TYPE( _ARGUMENT ) & EX__INTERNAL_ARGUMENT_FUNCTION( _ARGUMENT ) const \
	{																		\
		return EX__INTERNAL_ARGUMENT_MEMBER( _ARGUMENT );					\
	}
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_ARGUMENT_MEMBER_CONSTRUCTION
	#error
#else
#define EX__INTERNAL_ARGUMENT_MEMBER_CONSTRUCTION( _R, _D, _N, _ARGUMENT )	\
	EX__INTERNAL_ARGUMENT_TYPE( _ARGUMENT ) EX__INTERNAL_ARGUMENT_MEMBER( _ARGUMENT );
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_FOR_EACH
	#error
#else
#define EX__INTERNAL_FOR_EACH(												\
		_ARGUMENTS															\
	,	_OPERATION															\
)																			\
	BOOST_PP_SEQ_FOR_EACH_I(												\
			_OPERATION														\
		,	AMPLE															\
		,	_ARGUMENTS														\
	)
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_EXCEPTION_CLASS_TEMPLATE
	#error
#else
#define EX__INTERNAL_EXCEPTION_CLASS_TEMPLATE(								\
		_BASE_EXCEPTION														\
	,	_EXCEPTION															\
	,	_ARGUMENTS															\
)																			\
class _EXCEPTION															\
	:	public _BASE_EXCEPTION												\
{																			\
																			\
	typedef																	\
		_BASE_EXCEPTION														\
		BaseException;														\
																			\
public:																		\
																			\
	_EXCEPTION(																\
			EX__INTERNAL_FOR_EACH(											\
					_ARGUMENTS												\
				,	EX__INTERNAL_ARGUMENT_INPUT_CONSTRUCTION				\
			)																\
	)																		\
		:	EX__INTERNAL_FOR_EACH(											\
					_ARGUMENTS												\
				,	EX__INTERNAL_ARGUMENT_INITIALIZATION_LIST_CONSTRUCTION	\
			)																\
	{																		\
		EX__INTERNAL_FOR_EACH(												\
				_ARGUMENTS													\
			,	EX__INTERNAL_ARGUMENT_CHEKER_CONSTRUCTION					\
		)																	\
	}																		\
																			\
	EX__INTERNAL_FOR_EACH(													\
			_ARGUMENTS														\
		,	EX__INTERNAL_ARGUMENT_FUNCTION_CONSTRUCTION						\
	)																		\
																			\
	/*virtual*/ void accept( BaseException::VisitorType & _visitor ) const	\
	{																		\
		_visitor.visit( * this );											\
	}																		\
																			\
private:																	\
																			\
	EX__INTERNAL_FOR_EACH(													\
			_ARGUMENTS														\
		,	EX__INTERNAL_ARGUMENT_MEMBER_CONSTRUCTION						\
	)																		\
																			\
};
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_BASE_EXCEPTION_CLASS_TEMPLATE
	#error
#else
#define EX__INTERNAL_BASE_EXCEPTION_CLASS_TEMPLATE(							\
		_EXCEPTION															\
	,	_VISITOR															\
)																			\
class _EXCEPTION															\
	:	public ::Ample::Exceptions::BaseException				\
{																			\
																			\
public:																		\
																			\
	typedef																	\
		_VISITOR															\
		VisitorType;														\
																			\
	virtual void accept( _VISITOR & _visitor ) const = 0;					\
																			\
};
#endif

/*---------------------------------------------------------------------------*/

#ifdef EX__INTERNAL_DECLARE_EXCEPTION
	#error
#else
#define EX__INTERNAL_DECLARE_EXCEPTION(										\
		_BASE_EXCEPTION														\
	,	_EXCEPTION															\
	,	_ARGUMENTS															\
)																			\
	EX__INTERNAL_EXCEPTION_CLASS_TEMPLATE(									\
			_BASE_EXCEPTION													\
		,	_EXCEPTION														\
		,	( ( std::string )( throwFileName ) )							\
			( ( int )( throwLineNumber ) )									\
			_ARGUMENTS														\
	)
#endif

/*---------------------------------------------------------------------------*/

} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_INTERNAL_MACROS_HPP__
