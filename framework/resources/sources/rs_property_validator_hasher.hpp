// (C) 2013 Ample Resources

#ifndef __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_HASHER_HPP__
#define __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_HASHER_HPP__

/*---------------------------------------------------------------------------*/

#include <boost/functional/hash.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class PropertyValidator;

/*---------------------------------------------------------------------------*/

class PropertyValidatorHasher
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	typedef
		size_t
		HashType;

/*---------------------------------------------------------------------------*/

	HashType operator() ( boost::shared_ptr< Implementation::PropertyValidator > _validator ) const
	{
		return ( * this )( _validator.get() );

	} // operator()

/*---------------------------------------------------------------------------*/

	HashType operator() ( const Implementation::PropertyValidator * _validator ) const
	{
		return ( * this )( static_cast< const Ample::Resources::PropertyValidator * >( _validator ) );

	} // operator()

/*---------------------------------------------------------------------------*/

	HashType operator() ( const Ample::Resources::PropertyValidator * _validator ) const
	{
		return boost::hash< const Ample::Resources::PropertyValidator * >()( _validator );

	} // operator()

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_RESOURCES_IMPLEMENTATION_PROPERTY_VALIDATOR_HASHER_HPP__
