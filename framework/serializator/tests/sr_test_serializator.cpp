// (C) 2013 Ample Serializator

#include "framework/serializator/sources/ph/sr_ph.hpp"

#include "framework/serializator/api/sr_binary_saver.hpp"
#include "framework/serializator/api/sr_binary_restorer.hpp"

#include "framework/serializator/tests/sr_test_fixture.hpp"

#include <boost/test/unit_test.hpp>

#include <QtCore/qdatastream.h>
#include <QtCore/qbuffer.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Serializator {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		1) Binary
			1.1) Saving to closed buffer
			1.2) Saving to open buffer
			1.3) Base type
				1.3.1) bool
				1.3.2) char
				1.3.3) unsigned char
				1.3.4) short
				1.3.5) unsigned short
				1.3.6) int
				1.3.7) unsigned int
				1.3.8) long
				1.3.9) unsigned long
				1.3.10) long long
				1.3.11) unsigned long long
				1.3.12) float
				1.3.13) double
				1.3.14) std::string

*/

/*---------------------------------------------------------------------------*/

#define BINARY_SERIALIZATOR_TEST( _CASE, _NAME, _TYPE, _VALUE )				\
BOOST_AUTO_TEST_CASE( Serializator_##_CASE##_Binary_BaseType_##_NAME )		\
{																			\
	/*---------- Setup ----------*/											\
																			\
	_TYPE testValue = _VALUE;												\
																			\
	QByteArray bufferData;													\
																			\
	QBuffer buffer( & bufferData );											\
	buffer.open( QIODevice::ReadWrite );									\
																			\
	BOOST_REQUIRE( buffer.isOpen() );										\
																			\
	QDataStream stream( & buffer );											\
																			\
	/*-------- Test actions -----*/											\
																			\
	BOOST_REQUIRE_MESSAGE( stream.atEnd(), "BEFORE SAVE: stream.atEnd() == false" );\
																			\
	BinarySaver saver( stream );											\
	saver.save( testValue );												\
																			\
	buffer.seek( 0 );														\
																			\
	BOOST_REQUIRE_MESSAGE( !stream.atEnd(), "BEFORE RESTORE: stream.atEnd() != false" );\
																			\
	BinaryRestorer restorer( stream );										\
	_TYPE restoredValue = restorer.restore< _TYPE >();						\
																			\
	BOOST_REQUIRE_MESSAGE( stream.atEnd(), "AFTER RESTORE: stream.atEnd() == false" );\
																			\
	BOOST_REQUIRE_EQUAL( testValue, restoredValue );						\
																			\
}

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Serializator, Fixture )

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Serializator_Binary, Fixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Serializator_1_1_Binary_SavingToClosedBuffer )
{
	/*---------- Setup ----------*/

	QByteArray bufferData;

	QBuffer buffer( & bufferData );

	BOOST_REQUIRE( ! buffer.isOpen() );

	QDataStream stream( & buffer );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( stream.atEnd() );

	BinarySaver saver( stream );
	saver.save( 12345 );

	buffer.seek( 0 );

	BOOST_REQUIRE( stream.atEnd() );

} // Serializator_1_1_Binary_SavingToClosedBuffer

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Serializator_1_2_Binary_SavingToOpenBuffer )
{
	/*---------- Setup ----------*/

	QByteArray bufferData;

	QBuffer buffer( & bufferData );
	buffer.open( QIODevice::ReadWrite );

	BOOST_REQUIRE( buffer.isOpen() );

	QDataStream stream( & buffer );

	/*-------- Test actions -----*/

	BOOST_REQUIRE( stream.atEnd() );

	BinarySaver saver( stream );
	saver.save( 12345 );

	buffer.seek( 0 );

	BOOST_REQUIRE( ! stream.atEnd() );

} // Serializator_1_2_Binary_SavingToOpenBuffer

/*---------------------------------------------------------------------------*/

BINARY_SERIALIZATOR_TEST( 1_3_1,	Bool,				bool,					true	);
BINARY_SERIALIZATOR_TEST( 1_3_2,	Char,				char,					'A'		);
BINARY_SERIALIZATOR_TEST( 1_3_3,	UnsignedChar,		unsigned char,			'A'		);
BINARY_SERIALIZATOR_TEST( 1_3_4,	Short,				short,					12345	);
BINARY_SERIALIZATOR_TEST( 1_3_5,	UnsignedShort,		unsigned short,			12345	);
BINARY_SERIALIZATOR_TEST( 1_3_6,	Int,				int,					12345	);
BINARY_SERIALIZATOR_TEST( 1_3_7,	UnsignedInt,		unsigned int,			12345	);
BINARY_SERIALIZATOR_TEST( 1_3_8,	Long,				long,					12345	);
BINARY_SERIALIZATOR_TEST( 1_3_9,	UnsignedLong,		unsigned long,			12345	);
BINARY_SERIALIZATOR_TEST( 1_3_10,	LongLong,			long long,				12345	);
BINARY_SERIALIZATOR_TEST( 1_3_11,	UnsignedLongLong,	unsigned long long,		12345	);
BINARY_SERIALIZATOR_TEST( 1_3_12,	Float,				float,					12345	);
BINARY_SERIALIZATOR_TEST( 1_3_13,	Double,				double,					12345	);
BINARY_SERIALIZATOR_TEST( 1_3_14,	StdString,			std::string,			"test"	);

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Serializator_Binary

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Serializator

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Serializator
} // namespace Ample
