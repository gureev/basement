// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/con_message_color.hpp"

#include "framework/console/sources/resources/con_resources.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

int
MessageColor::getColor( MessageLevel::Enum _messageLevel )
{
	switch ( _messageLevel )
	{
	case MessageLevel::Command:
	case MessageLevel::Info:
		return Resources::MessageColors::Default;

	case MessageLevel::Error:
		return Resources::MessageColors::Error;

	case MessageLevel::Warning:
		return Resources::MessageColors::Warning;

	case MessageLevel::Success:
		return Resources::MessageColors::Success;
	}

	return Resources::MessageColors::Default;

} // MessageColor::getColor

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample
