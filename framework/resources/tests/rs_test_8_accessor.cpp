// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_accessor_impl.hpp"

#include "framework/resources/tests/rs_test_property_fixture.hpp"
#include "framework/resources/tests/rs_test_string_fixture.hpp"
#include "framework/resources/tests/rs_test_property_validator_fixture.hpp"

#include <boost/test/unit_test.hpp>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Accessor:
			1) Properties
				1.1) One property
					1.1.1) Two components
					1.1.2) Three components
					1.1.3) Four components
			2) Strings
				2.1) One property
					2.1.1) Two components
					2.1.2) Three components
					2.1.3) Four components
			3) Property Validator
				3.1) Simple validator

*/

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE( Suite_Accessor )

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Accessor_Properties, PropertyFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_Properties_1_1_1_OneProperty_TwoComponents )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section" )
		.property( "prop", "value" )
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreProperties( file );

		const Property & fProperty = accessor.getProperty(
				"section/prop"
			,	AccessType::FirstAvailable
		);

		const Property & tProperty = accessor.getProperty(
				"section/prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( fProperty.asString(), "value" );
		BOOST_REQUIRE_EQUAL( tProperty.asString(), "value" );

		BOOST_REQUIRE_EQUAL( & tProperty, & fProperty );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_Properties_1_1_1_OneProperty_TwoComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_Properties_1_1_2_OneProperty_ThreeComponents )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section1" )
		.section( "section2" )
			.property( "prop", "value" )
		.end()
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreProperties( file );

		const Property & fProperty = accessor.getProperty(
				"section1/section2/prop"
			,	AccessType::FirstAvailable
		);

		const Property & tProperty = accessor.getProperty(
				"section1/section2/prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( fProperty.asString(), "value" );
		BOOST_REQUIRE_EQUAL( tProperty.asString(), "value" );

		BOOST_REQUIRE_EQUAL( & tProperty, & fProperty );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_Properties_1_1_2_OneProperty_ThreeComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_Properties_1_1_3_OneProperty_FourComponents )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section1" )
		.section( "section2" )
			.section( "section3" )
				.property( "prop", "value" )
			.end()
		.end()
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreProperties( file );

		const Property & fProperty = accessor.getProperty(
				"section1/section2/section3/prop"
			,	AccessType::FirstAvailable
		);

		const Property & tProperty = accessor.getProperty(
				"section1/section2/section3/prop"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( fProperty.asString(), "value" );
		BOOST_REQUIRE_EQUAL( tProperty.asString(), "value" );

		BOOST_REQUIRE_EQUAL( & tProperty, & fProperty );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_Properties_1_1_3_OneProperty_FourComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Accessor_Properties

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Accessor_Strings, StringFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_Strings_2_1_1_OneString_TwoComponents )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section", "test" )
		.string( "string", "id", "value" )
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreStrings( file );

		const String & fString = accessor.getString(
				"section/string"
			,	AccessType::FirstAvailable
		);

		const String & tString = accessor.getString(
				"section/string"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( fString.getValue(), "value" );
		BOOST_REQUIRE_EQUAL( fString.getIdentifier(), "id" );
		BOOST_REQUIRE_EQUAL( fString.getSectionName(), "test" );

		BOOST_REQUIRE_EQUAL( tString.getValue(), "value" );
		BOOST_REQUIRE_EQUAL( tString.getIdentifier(), "id" );
		BOOST_REQUIRE_EQUAL( tString.getSectionName(), "test" );

		BOOST_REQUIRE_EQUAL( & tString, & fString );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_Strings_2_1_1_OneString_TwoComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_Strings_2_1_2_OneString_ThreeComponents )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section1", "test" )
		.section( "section2", "test" )
			.string( "string", "id", "value" )
		.end()
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreStrings( file );

		const String & fString = accessor.getString(
				"section1/section2/string"
			,	AccessType::FirstAvailable
		);

		const String & tString = accessor.getString(
				"section1/section2/string"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( fString.getValue(), "value" );
		BOOST_REQUIRE_EQUAL( fString.getIdentifier(), "id" );
		BOOST_REQUIRE_EQUAL( fString.getSectionName(), "test" );

		BOOST_REQUIRE_EQUAL( tString.getValue(), "value" );
		BOOST_REQUIRE_EQUAL( tString.getIdentifier(), "id" );
		BOOST_REQUIRE_EQUAL( tString.getSectionName(), "test" );

		BOOST_REQUIRE_EQUAL( & tString, & fString );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_Strings_2_1_2_OneString_ThreeComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_Strings_2_1_3_OneString_FourComponents )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section1", "test" )
		.section( "section2", "test" )
			.section( "section3", "test" )
				.string( "string", "id", "value" )
			.end()
		.end()
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreStrings( file );

		const String & fString = accessor.getString(
				"section1/section2/section3/string"
			,	AccessType::FirstAvailable
		);

		const String & tString = accessor.getString(
				"section1/section2/section3/string"
			,	AccessType::TopAvailable
		);

		BOOST_REQUIRE_EQUAL( fString.getValue(), "value" );
		BOOST_REQUIRE_EQUAL( fString.getIdentifier(), "id" );
		BOOST_REQUIRE_EQUAL( fString.getSectionName(), "test" );

		BOOST_REQUIRE_EQUAL( tString.getValue(), "value" );
		BOOST_REQUIRE_EQUAL( tString.getIdentifier(), "id" );
		BOOST_REQUIRE_EQUAL( tString.getSectionName(), "test" );

		BOOST_REQUIRE_EQUAL( & tString, & fString );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_Strings_2_1_3_OneString_FourComponents

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Accessor_Strings

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Accessor_PropertyValidator, PropertyValidatorFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Accessor_PropertyValidator_1_1_SimpleValidator )
{
	/*---------- Setup ----------*/

	Accessor & accessor = getAccessor();

	section( "section" )
		.property( "prop", "value" )
	.end();

	QIODevice & file = openStream();

	/*-------- Test actions -----*/

	try
	{
		accessor.restoreProperties( file );

		PropertyValidator & validator = accessor.createValidator();

		validator.expectType( PropertyType::String );

		accessor.validateProperty(
				"section/prop"
			,	validator
			,	AccessType::FirstAvailable
		);

		accessor.validateProperty(
				"section/prop"
			,	validator
			,	AccessType::TopAvailable
		);
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Accessor_PropertyValidator_1_1_SimpleValidator

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Accessor_PropertyValidator

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Accessor

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
