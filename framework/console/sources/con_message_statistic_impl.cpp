// (C) 2013 Ample Console

#include "framework/console/sources/ph/con_ph.hpp"

#include "framework/console/sources/con_message_statistic_impl.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

MessageStatistic::MessageStatistic()
	:	m_errors( 0 )
	,	m_warnings( 0 )
{
} // MessageStatistic::MessageStatistic

/*---------------------------------------------------------------------------*/

MessageStatistic::~MessageStatistic()
{
} // MessageStatistic::~MessageStatistic

/*---------------------------------------------------------------------------*/

void
MessageStatistic::reset()
{
	m_errors = 0;
	m_warnings = 0;

} // MessageStatistic::reset

/*---------------------------------------------------------------------------*/

int
MessageStatistic::getErrorsCount() const
{
	return m_errors;

} // MessageStatistic::getErrorsCount

/*---------------------------------------------------------------------------*/

int
MessageStatistic::getWarningsCount() const
{
	return m_warnings;

} // MessageStatistic::getWarningsCount

/*---------------------------------------------------------------------------*/

void
MessageStatistic::updateMessagesCounter( MessageLevel::Enum _messageLevel )
{
	switch ( _messageLevel )
	{
	case MessageLevel::Error:
		++ m_errors;
		break;

	case MessageLevel::Warning:
		++ m_warnings;
		break;
	};

} // MessageStatistic::updateMessagesCounter

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample
