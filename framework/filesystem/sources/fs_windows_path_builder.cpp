// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_windows_path_builder.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

bool
WindowsPathBuilder::isAbsolute( const std::string & _path )
{
	bool absolute =
			_path.length() >= 3
		&&	isDriveLetter( _path[ 0 ] )
		&&	_path[ 1 ] == ':'
		&&	_path[ 2 ] == '/'
	;

	return absolute
		?	true
		:	_path[ 0 ] == '/'
	;

} // WindowsPathBuilder::isAbsolute

/*---------------------------------------------------------------------------*/

bool
WindowsPathBuilder::isDriveLetter( const char _symbol )
{
	return
		(		_symbol >= 'a'
			&&	_symbol <= 'z' )
		||
		(		_symbol >= 'A'
			&&	_symbol <= 'Z' )
		||	_symbol == '/'
	;

} // WindowsPathBuilder::isDriveLetter

/*---------------------------------------------------------------------------*/

bool
WindowsPathBuilder::isSlash( const char _symbol )
{
	return _symbol == '/';

} // WindowsPathBuilder::isSlash

/*---------------------------------------------------------------------------*/

const char
WindowsPathBuilder::extractDriveLetter( const std::string & _path )
{
	if ( ! isAbsolute( _path ) )
		return 0;

	const char driveLetter = _path[ 0 ];

	return isDriveLetter( driveLetter )
		?	driveLetter
		:	0
	;

} // WindowsPathBuilder::extractDriveLetter

/*---------------------------------------------------------------------------*/

void
WindowsPathBuilder::optimize( std::string & _path )
{
	std::replace(
			_path.begin()
		,	_path.end()
		,	'\\'
		,	'/'
	);

} // WindowsPathBuilder::optimize

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
