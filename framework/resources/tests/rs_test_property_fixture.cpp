// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/sources/rs_properties_restorer.hpp"

#include "framework/resources/sources/resources/rs_resources.hpp"

#include "framework/resources/tests/rs_test_property_fixture.hpp"

#include <QtCore/qbuffer.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Tests {

/*---------------------------------------------------------------------------*/

PropertyFixture::PropertyFixture()
	:	Implementation::PropertySectionEditor(
				* this
			,	"XML_TEST_FILE"
			,	0
		)
{
} // PropertyFixture::PropertyFixture

/*---------------------------------------------------------------------------*/

Accessor &
PropertyFixture::getAccessor()
{
	return m_accessor;

} // PropertyFixture::getAccessor

/*---------------------------------------------------------------------------*/

QIODevice &
PropertyFixture::openStream()
{
	m_byteData = m_body.toAscii();

	m_buffer.setBuffer( & m_byteData );
	m_buffer.open( QIODevice::ReadOnly );

	return m_buffer;

} // PropertyFixture::openStream

/*---------------------------------------------------------------------------*/

bool
PropertyFixture::compare(
		double _left
	,	double _right
) const
{
	_left = std::fabs( _left );
	_right = std::fabs( _right );

	double epsilon = std::min( _left, _right ) * std::numeric_limits< double >::epsilon();

	return ( _left - _right ) < epsilon;

} // PropertyFixture::compare

/*---------------------------------------------------------------------------*/

void
PropertyFixture::clear()
{
	m_body = "";

} // PropertyFixture::clear

/*---------------------------------------------------------------------------*/

const Implementation::PropertySectionsContainer &
PropertyFixture::runTest()
{
	QByteArray data = m_body.toAscii();

	QBuffer buffer( & data );
	buffer.open( QIODevice::ReadOnly );

	Implementation::PropertiesRestorer restorer(
			m_sections
		,	buffer
	);
	restorer.run();

	return m_sections;

} // PropertyFixture::runTest

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Resources
} // namespace Ample
