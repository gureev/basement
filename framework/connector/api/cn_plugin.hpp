// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_PLUGIN_HPP__
#define __AMPLE_CONNECTOR_PLUGIN_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/connector/api/cn_export.hpp"
#include "framework/connector/api/cn_plugins_container.hpp"
#include "framework/connector/api/cn_plugin_info.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

class PluginIdentifier;
class Accessor;

/*---------------------------------------------------------------------------*/

class CONNECTOR_API Plugin
{

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	virtual ~Plugin() {}

/*---------------------------------------------------------------------------*/

	virtual boost::shared_ptr< Accessor > getAccessor() const = 0;

	virtual const PluginIdentifier & getIdentifier() const = 0;

/*---------------------------------------------------------------------------*/

	virtual void onPluginLoad() = 0;

/*---------------------------------------------------------------------------*/

	template< typename _PluginAccessor >
	boost::shared_ptr< _PluginAccessor >
	getPluginAccessor( const PluginIdentifier & _pluginIdentifier ) const;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

template< typename _PluginAccessor >
boost::shared_ptr< _PluginAccessor >
Plugin::getPluginAccessor( const PluginIdentifier & _pluginIdentifier ) const
{
	static boost::shared_ptr< _PluginAccessor > pluginAccessor;

	if ( pluginAccessor )
		return pluginAccessor;

	auto pluginInfo = PluginsContainer::getInstance().findPlugin( _pluginIdentifier );
	EX_DEBUG_ASSERT( pluginInfo );

	boost::shared_ptr< Accessor > accessor = pluginInfo->getPlugin().getAccessor();

	pluginAccessor = boost::static_pointer_cast< _PluginAccessor >( accessor );

	return pluginAccessor;
}

/*---------------------------------------------------------------------------*/

#ifdef CONNECTOR_DECLARE_PLUGIN_FACTORY
	#error
#else
#define CONNECTOR_DECLARE_PLUGIN_FACTORY()									\
extern "C" __declspec( dllexport )											\
Ample::Connector::Plugin & createPlugin()									\
{																			\
	static Plugin::Instance plugin;											\
	return plugin;															\
}
#endif

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_PLUGIN_HPP__
