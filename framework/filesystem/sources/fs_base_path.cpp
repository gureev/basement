// (C) 2013 Ample FileSystem

#include "framework/filesystem/sources/ph/fs_ph.hpp"

#include "framework/filesystem/sources/fs_base_path.hpp"

#include "framework/exceptions/api/ex_exceptions.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace FileSystem {
namespace Implementation {

/*---------------------------------------------------------------------------*/

BasePath::BasePath( int _componentsCount )
{
	m_components.reserve( _componentsCount );

} // BasePath::BasePath

/*---------------------------------------------------------------------------*/

const std::string &
BasePath::getComponent( int _index ) const
{
	EX_DEBUG_ASSERT(
			_index >= 0
		&&	_index < getComponentsCount()
	);
	return m_components[ _index ];

} // BasePath::getComponent

/*---------------------------------------------------------------------------*/

int
BasePath::getComponentsCount() const
{
	return m_components.size();

} // BasePath::getComponentsCount

/*---------------------------------------------------------------------------*/

FileSystem::Path &
BasePath::stepUp()
{
	EX_DEBUG_ASSERT( ! m_components.empty() );
	m_components.pop_back();

	return * this;

} // BasePath::stepUp

/*---------------------------------------------------------------------------*/

FileSystem::Path &
BasePath::operator /= ( const std::string & _component )
{
	// TODO: validate _component
	m_components.push_back( _component );

	return * this;

} // BasePath::operator /=

/*---------------------------------------------------------------------------*/

std::string
BasePath::toString() const
{
	return buildString< std::string >();

} // BasePath::toString

/*---------------------------------------------------------------------------*/

QString
BasePath::toQString() const
{
	return buildString< QString >();

} // BasePath::toQString

/*---------------------------------------------------------------------------*/

template< typename _String >
_String
BasePath::buildString() const
{
	_String asString;
	asString.reserve( calculatePathLength() );

	if ( isAbsolute() )
	{
		char driveLetter = getDriveLetter();
		asString += driveLetter;
		if ( driveLetter != '/' )
			asString += ":/";
	}

	fillPathString( asString );

	return asString;

} // BasePath::buildString

/*---------------------------------------------------------------------------*/

int
BasePath::calculatePathLength() const
{
	int resultLength = 0;

	int nComponents = getComponentsCount();
	for ( int i = 0; i < nComponents; ++i )
	{
		resultLength += getComponent( i ).length();
		resultLength += 1;
	}

	if ( resultLength )
		--resultLength;

	return resultLength;

} // BasePath::calculatePathLength

/*---------------------------------------------------------------------------*/

template< typename _String >
void
BasePath::fillPathString( _String & _string ) const
{
	int workTo = getComponentsCount() - 1;
	for ( int i = 0; i < workTo; ++i )
	{
		_string += getComponent( i ).data();
		_string += '/';
	}
	_string += getComponent( workTo ).data();

} // BasePath::fillPathString

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace FileSystem
} // namespace Ample
