// (C) 2013 Ample Exceptions

#ifndef __AMPLE_EXCEPTIONS_RESOURCES_HPP__
#define __AMPLE_EXCEPTIONS_RESOURCES_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Exceptions {
namespace Resources {

/*---------------------------------------------------------------------------*/

namespace Messages {

/*---------------------------------------------------------------------------*/

	extern const std::string InternalError;

	extern const std::string UnhandledException;

/*---------------------------------------------------------------------------*/

} // namespace Messages

/*---------------------------------------------------------------------------*/

} // namespace Resources
} // namespace Exceptions
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_EXCEPTIONS_RESOURCES_HPP__
