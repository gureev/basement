// (C) 2013 Ample Connector

#include "framework/connector/sources/ph/cn_ph.hpp"

#include "framework/connector/api/cn_plugin_info.hpp"
#include "framework/connector/api/cn_plugins_loader.hpp"
#include "framework/connector/api/cn_exceptions.hpp"

#include "framework/connector/sources/cn_plugins_info_restorer.hpp"

#include "framework/connector/sources/resources/cn_resources.hpp"

#include "framework/connector/tests/cn_test_reader_fixture.hpp"

#include <boost/test/unit_test.hpp>

#include <QtCore/qfile.h>
#include <QtCore/qbuffer.h>

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {
namespace Tests {

/*---------------------------------------------------------------------------*/

/*

	TEST PLAN:
		Restorer:
			1) Broken data
				1.1) Empty load policy attribute
				1.2) Empty plug-in name attribute
				1.3) Empty plug-in identifier attribute
				1.4) Missing load policy attribute (Not implemented)
				1.5) Missing plug-in name attribute (Not implemented)
				1.6) Missing plug-in identifier attribute (Not implemented)
				1.7) Bad root node name (Not implemented)
			2) Plug-ins info
				2.1) Without plug-ins
				2.2) One plug-in
				2.3) Two plug-ins
				2.4) Two plug-ins with equal names (Not implemented)
				2.5) Two plug-ins with equal identifiers
				2.6) Two plug-ins with equal load policies
				2.7) Not valid plug-in name (Not implemented)
					2.7.1) More than 255 symbols
					2.7.2) Invalid characters
				2.8) Not valid plug-in identifier
					2.8.1) Too few symbols
					2.8.2) Too many symbols
					2.8.3) Major block (Not implemented)
						2.8.3.1) Too few symbols
						2.8.3.2) Too many symbols
					2.8.4) Minor 1 block (Not implemented)
						2.8.4.1) Too few symbols
						2.8.4.2) Too many symbols
					2.8.5) Minor 1 block (Not implemented)
						2.8.5.1) Too few symbols
						2.8.5.2) Too many symbols
					2.8.6) SubMinor 1 block (Not implemented)
						2.8.6.1) Too few symbols
						2.8.6.2) Too many symbols
					2.8.7) SubMinor 2 block (Not implemented)
						2.8.7.1) Too few symbols
						2.8.7.2) Too many symbols
					2.8.8) Not a hex number
				2.9) Not valid load policy
					2.9.1) Value more than 1
					2.9.2) Value less than 0
					2.9.3) Value is not a number
					2.9.4) Length more than 1
			3) Read internal plug-ins info file

*/

/*---------------------------------------------------------------------------*/

BOOST_FIXTURE_TEST_SUITE( Suite_Restorer, RestorerFixture )

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_1_1_BrokenData_EmptyLoadPolicyAttribute )
{
	/*---------- Setup ----------*/

	begin()
		.plugin( "plugin_name", "plugin_identifier", "" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_1_1_BrokenData_EmptyLoadPolicyAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_1_2_BrokenData_EmptyPluginNameAttribute )
{
	/*---------- Setup ----------*/

	begin()
		.plugin( "", "plugin_identifier", "plugin_loadpolicy" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_1_2_BrokenData_EmptyPluginNameAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_1_3_BrokenData_EmptyPluginIdentifierAttribute )
{
	/*---------- Setup ----------*/

	begin()
		.plugin( "plugin_name", "", "plugin_loadpolicy" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_1_3_BrokenData_EmptyPluginIdentifierAttribute

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_1_PluginInfo_WithoutPlugins )
{
	/*---------- Setup ----------*/

	begin()
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & plugins = runTest();

		BOOST_REQUIRE_EQUAL( plugins.getPluginsCount(), 0 );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Restorer_2_1_PluginInfo_WithoutPlugins

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_2_PluginInfo_OnePlugin )
{
	/*---------- Setup ----------*/

	PluginIdentifier id = generatePluginIdentifier();

	begin()
		.plugin( "test_name", id.toString().data(), "1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		auto & plugins = runTest();

		BOOST_REQUIRE_EQUAL( plugins.getPluginsCount(), 1 );

		auto plugin = plugins.findPlugin( id );

		BOOST_REQUIRE( plugin );

		BOOST_REQUIRE_EQUAL( plugin->getLoadPolicy(), PluginLoadPolicy::fromString( "1" ) );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Restorer_2_2_PluginInfo_OnePlugin

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_3_PluginInfo_TwoPlugins )
{
	/*---------- Setup ----------*/

	PluginIdentifier id1 = generatePluginIdentifier();
	PluginIdentifier id2 = generatePluginIdentifier();

	begin()
		.plugin( "test_name_1", id1.toString().data(), "0" )
		.plugin( "test_name_2", id2.toString().data(), "1" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		BOOST_REQUIRE( ! id1.equals( id2 ) );

		auto & plugins = runTest();

		BOOST_REQUIRE_EQUAL( plugins.getPluginsCount(), 2 );

		auto plugin1 = plugins.findPlugin( id1 );
		auto plugin2 = plugins.findPlugin( id2 );

		BOOST_REQUIRE( plugin1 );
		BOOST_REQUIRE( plugin2 );

		BOOST_REQUIRE_EQUAL( plugin1->getLoadPolicy(), PluginLoadPolicy::fromString( "0" ) );
		BOOST_REQUIRE_EQUAL( plugin2->getLoadPolicy(), PluginLoadPolicy::fromString( "1" ) );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Restorer_2_3_PluginInfo_TwoPlugins

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_4_PluginInfo_TwoPluginsWithEqualNames )
{
	/*---------- Setup ----------*/

	PluginIdentifier id1 = generatePluginIdentifier();
	PluginIdentifier id2 = generatePluginIdentifier();

	begin()
		.plugin( "test_name", id1.toString().data(), "0" )
		.plugin( "test_name", id2.toString().data(), "1" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE( ! id1.equals( id2 ) );

	/*
	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicatePluginName
	);
	*/

} // Restorer_2_4_PluginInfo_TwoPluginsWithEqualNames

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_5_PluginInfo_TwoPluginsWithEqualIdentifiers )
{
	/*---------- Setup ----------*/

	PluginIdentifier id = generatePluginIdentifier();

	begin()
		.plugin( "test_name_1", id.toString().data(), "0" )
		.plugin( "test_name_2", id.toString().data(), "1" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::DuplicatePluginIdentifier
	);

} // Restorer_2_5_PluginInfo_TwoPluginsWithEqualIdentifiers

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_6_PluginInfo_TwoPluginsWithEqualLoadPolicies )
{
	/*---------- Setup ----------*/

	PluginIdentifier id1 = generatePluginIdentifier();
	PluginIdentifier id2 = generatePluginIdentifier();

	begin()
		.plugin( "test_name_1", id1.toString().data(), "0" )
		.plugin( "test_name_2", id2.toString().data(), "0" )
	.end();

	/*-------- Test actions -----*/

	try
	{
		BOOST_REQUIRE( ! id1.equals( id2 ) );

		auto & plugins = runTest();

		BOOST_REQUIRE_EQUAL( plugins.getPluginsCount(), 2 );

		auto plugin1 = plugins.findPlugin( id1 );
		auto plugin2 = plugins.findPlugin( id2 );

		BOOST_REQUIRE( plugin1 );
		BOOST_REQUIRE( plugin2 );

		BOOST_REQUIRE_EQUAL( plugin1->getLoadPolicy(), PluginLoadPolicy::fromString( "0" ) );
		BOOST_REQUIRE_EQUAL( plugin2->getLoadPolicy(), PluginLoadPolicy::fromString( "0" ) );
	}
	catch ( ... )
	{
		BOOST_REQUIRE( false );
	}

} // Restorer_2_6_PluginInfo_TwoPluginsWithEqualLoadPolicies

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_8_1_PluginInfo_NotValidPluginIdentifier_TooFewSymbols )
{
	/*---------- Setup ----------*/

	std::string brokenPluginIdentifier = generatePluginIdentifier().toString();
	brokenPluginIdentifier.pop_back();

	begin()
		.plugin( "test_name", brokenPluginIdentifier.data(), "0" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_8_1_PluginInfo_NotValidPluginIdentifier_TooFewSymbols

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_8_2_PluginInfo_NotValidPluginIdentifier_TooManySymbols )
{
	/*---------- Setup ----------*/

	std::string brokenPluginIdentifier = generatePluginIdentifier().toString() + 'A';

	begin()
		.plugin( "test_name", brokenPluginIdentifier.data(), "0" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_8_2_PluginInfo_NotValidPluginIdentifier_TooManySymbols

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_8_8_PluginInfo_NotValidPluginIdentifier_NotAHexNumber )
{
	/*---------- Setup ----------*/

	begin()
		.plugin( "test_name", "XXX", "LENGTH" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_8_8_PluginInfo_NotValidPluginIdentifier_NotAHexNumber

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_9_1_PluginInfo_NotValidLoadPolicy_ValueMoreThan1 )
{
	/*---------- Setup ----------*/

	PluginIdentifier id = generatePluginIdentifier();

	begin()
		.plugin( "test_name", id.toString().data(), "2" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_9_1_PluginInfo_NotValidLoadPolicy_ValueMoreThan1

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_9_2_PluginInfo_NotValidLoadPolicy_ValueMoreLessThen0 )
{
	/*---------- Setup ----------*/

	PluginIdentifier id = generatePluginIdentifier();

	begin()
		.plugin( "test_name", id.toString().data(), "-1" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_9_2_PluginInfo_NotValidLoadPolicy_ValueMoreLessThen0

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_9_3_PluginInfo_NotValidLoadPolicy_ValueIsNotANumber )
{
	/*---------- Setup ----------*/

	PluginIdentifier id = generatePluginIdentifier();

	begin()
		.plugin( "test_name", id.toString().data(), "X" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_9_3_PluginInfo_NotValidLoadPolicy_ValueIsNotANumber

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_2_9_4_PluginInfo_NotValidLoadPolicy_LengthMoreThan1 )
{
	/*---------- Setup ----------*/

	PluginIdentifier id = generatePluginIdentifier();

	begin()
		.plugin( "test_name", id.toString().data(), "LENGTH" )
	.end();

	/*-------- Test actions -----*/

	BOOST_REQUIRE_THROW(
			runTest()
		,	Exceptions::BrokenAttribute
	);

} // Restorer_2_9_4_PluginInfo_NotValidLoadPolicy_LengthMoreThan1

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE( Restorer_3_ReadInternalPluginsInfoFile )
{
	/*---------- Setup ----------*/

	Implementation::PluginsContainer m_plugins;

	QFile file( Resources::Paths::PluginsInfo );
	file.open( QIODevice::ReadOnly );

	Implementation::PluginsInfoRestorer restorer(
			m_plugins
		,	file
	);

	/*-------- Test actions -----*/

	BOOST_REQUIRE_NO_THROW( restorer.run() );

} // Restorer_3_ReadInternalPluginsInfoFile

/*---------------------------------------------------------------------------*/

BOOST_AUTO_TEST_SUITE_END() // Suite_Restorer

/*---------------------------------------------------------------------------*/

} // namespace Tests
} // namespace Connector
} // namespace Ample
