// (C) 2013 Ample Console

#ifndef __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_MANAGER_HPP__
#define __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_MANAGER_HPP__

/*---------------------------------------------------------------------------*/

#include "framework/console/api/con_message_level.hpp"

#include "framework/console/sources/con_console_writer.hpp"
#include "framework/console/sources/con_console_reader.hpp"

#include <QtCore/qobject.h>

/*---------------------------------------------------------------------------*/

namespace Ample
{
	namespace Resources
	{
		class String;
	}
}

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Console {
namespace Implementation {

/*---------------------------------------------------------------------------*/

class ConsoleManager
	:	public QObject
{

/*---------------------------------------------------------------------------*/

	Q_OBJECT

/*---------------------------------------------------------------------------*/

public:

/*---------------------------------------------------------------------------*/

	ConsoleManager( const std::string & _applicationName );

	~ConsoleManager();

/*---------------------------------------------------------------------------*/

	void print(
			MessageLevel::Enum _messageLevel
		,	const Resources::String & _string
	);

	void print(
			MessageLevel::Enum _messageLevel
		,	const std::string & _message
	);

/*---------------------------------------------------------------------------*/

signals:

/*---------------------------------------------------------------------------*/

	void printEvent(
			MessageLevel::Enum _messageLevel
		,	const std::string & _moduleShortName
		,	const std::string & _messageIdentifier
		,	const std::string & _message
	);

	void printCommandEvent( const std::string & _command );

/*---------------------------------------------------------------------------*/

public slots:

/*---------------------------------------------------------------------------*/

	void onEnterKey( std::string _command );

/*---------------------------------------------------------------------------*/

private:

/*---------------------------------------------------------------------------*/

	ConsoleWriter m_consoleWriter;

	ConsoleReader m_consoleReader;

/*---------------------------------------------------------------------------*/

};

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Console
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONSOLE_IMPLEMENTATION_CONSOLE_MANAGER_HPP__
