// (C) 2013 Ample Framework

#ifndef __AMPLE_COMMON_HEADERS_VERSION_HPP__
#define __AMPLE_COMMON_HEADERS_VERSION_HPP__

/*----------------------------------------------------------------------------*/

#include <string>

#include <QtCore/qstring.h>

#include <boost/format.hpp>

/*----------------------------------------------------------------------------*/

namespace Ample {
namespace CommonHeaders {

/*----------------------------------------------------------------------------*/

class Version
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	Version()
		:	m_major( 0 )
		,	m_minor( 0 )
	{}

/*----------------------------------------------------------------------------*/

	Version(
			int _major
		,	int _minor
	)
		:	m_major( _major )
		,	m_minor( _minor )
	{}

/*----------------------------------------------------------------------------*/

	int getMajorValue() const;

	int getMinorValue() const;

/*----------------------------------------------------------------------------*/

	static Version fromString( const std::string & _versionStr );

	static Version fromString( const QString & _versionStr );

	static Version fromString( const char * _versionStr );

/*----------------------------------------------------------------------------*/

	bool operator == ( const Version & _version ) const;

	bool operator != ( const Version & _version ) const;

	bool operator > ( const Version & _version ) const;

	bool operator < ( const Version & _version ) const;

/*----------------------------------------------------------------------------*/

	bool isValid() const;

	bool isSatisfies( const Version & _actualVersion ) const;

/*----------------------------------------------------------------------------*/

	std::string toString() const;

	QString toQString() const;

/*----------------------------------------------------------------------------*/

private:

/*----------------------------------------------------------------------------*/

	int m_major;

	int m_minor;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

inline
int
Version::getMajorValue() const
{
	return m_major;
}

/*----------------------------------------------------------------------------*/

inline
int
Version::getMinorValue() const
{
	return m_minor;
}

/*----------------------------------------------------------------------------*/

inline
Version
Version::fromString( const std::string & _versionStr )
{
	return fromString( _versionStr.data() );
}

/*----------------------------------------------------------------------------*/

inline
Version
Version::fromString( const QString & _versionStr )
{
	return fromString( _versionStr.toAscii().data() );
}

/*----------------------------------------------------------------------------*/

inline
Version
Version::fromString( const char * _versionStr )
{
	int major = 0;
	int minor = 0;

	bool ok = 2 == sscanf(
			_versionStr
		,	"%d.%d"
		,	& major
		,	& minor
	);

	return	ok
		?	Version( major, minor )
		:	Version()
	;
}

/*----------------------------------------------------------------------------*/

inline
bool
Version::operator == ( const Version & _version ) const
{
	return	( m_major == _version.m_major )
		&&	( m_minor == _version.m_minor )
	;
}

/*----------------------------------------------------------------------------*/

inline
bool
Version::operator != ( const Version & _version ) const
{
	return !( * this == _version );
}

/*----------------------------------------------------------------------------*/

inline
bool
Version::operator > ( const Version & _version ) const
{
	return	m_major > _version.m_major
		||	(
					m_major == _version.m_major
				&&	m_minor > _version.m_minor
			)
	;
}

/*----------------------------------------------------------------------------*/

inline
bool
Version::operator < ( const Version & _version ) const
{
	return	!( * this > _version )
		&&	( * this != _version )
	;
}

/*----------------------------------------------------------------------------*/

inline
bool
Version::isValid() const
{
	return m_major || m_minor;
}

/*----------------------------------------------------------------------------*/

inline
bool
Version::isSatisfies( const Version & _actualVersion ) const
{
	return	_actualVersion.m_major == m_major
		&&	_actualVersion.m_minor >= m_minor
	;
}

/*----------------------------------------------------------------------------*/

inline
std::string
Version::toString() const
{
	static const char DefaultTemplate[] = "%1%.%2%";
	return boost::str(
		boost::format( DefaultTemplate )
		%	m_major
		%	m_minor
	);
}

/*----------------------------------------------------------------------------*/

inline
QString
Version::toQString() const
{
	return QString::fromAscii( toString().data() );

}

/*----------------------------------------------------------------------------*/

} // namespace CommonHeaders
} // namespace Ample

/*----------------------------------------------------------------------------*/

#endif // __AMPLE_COMMON_HEADERS_VERSION_HPP__
