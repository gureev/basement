// (C) 2013 Ample Resources

#include "framework/resources/sources/ph/rs_ph.hpp"

#include "framework/resources/api/rs_exceptions.hpp"

#include "framework/resources/sources/rs_property_section.hpp"
#include "framework/resources/sources/rs_property_list.hpp"

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Resources {
namespace Implementation {

/*---------------------------------------------------------------------------*/

PropertySection::PropertySection()
{
} // PropertySection::PropertySection

/*---------------------------------------------------------------------------*/

PropertySection::~PropertySection()
{
} // PropertySection::~PropertySection

/*---------------------------------------------------------------------------*/

void
PropertySection::addResource(
		const std::string & _name
	,	const std::string & _value
)
{
	Properties::iterator it = m_properties.find( _name );

	if ( it == m_properties.end() )
	{
		boost::shared_ptr< Implementation::PropertyList > list( new PropertyList );
		m_properties[ _name ] = list;

		it = m_properties.find( _name );
	}

	it->second->add( _value );

} // PropertySection::addResource

/*---------------------------------------------------------------------------*/

PropertySection &
PropertySection::addSection( const std::string & _name )
{
	if ( m_properties.contains( _name ) )
		EX_THROW_1_ARGUMENT(
				Exceptions::DuplicateResource
			,	_name
		);

	return PropertySectionsContainer::addSection( _name );

} // PropertySection::addSection

/*---------------------------------------------------------------------------*/

const Resources::Property &
PropertySection::getResource(
		const std::string & _name
	,	AccessType::Enum _accessType
) const
{
	auto resource = m_properties.getValue( _name );

	if ( !resource )
		EX_THROW_1_ARGUMENT(
				Exceptions::MissingResource
			,	_name
		);

	EX_DEBUG_ASSERT( * resource );
	return ( * resource )->get( _accessType );

} // PropertySection::getResource

/*---------------------------------------------------------------------------*/

int
PropertySection::getResourcesCount() const
{
	return m_properties.size();

} // PropertySection::getResourcesCount

/*---------------------------------------------------------------------------*/

} // namespace Implementation
} // namespace Resources
} // namespace Ample
