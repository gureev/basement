// (C) 2013 Ample Connector

#ifndef __AMPLE_CONNECTOR_EXPORT_HPP__
#define __AMPLE_CONNECTOR_EXPORT_HPP__

/*---------------------------------------------------------------------------*/

namespace Ample {
namespace Connector {

/*---------------------------------------------------------------------------*/

#ifdef _PLUGIN_CONNECTOR_
	#define CONNECTOR_API __declspec( dllexport )
#else
	#define CONNECTOR_API __declspec( dllimport )
#endif

/*---------------------------------------------------------------------------*/

} // namespace Connector
} // namespace Ample

/*---------------------------------------------------------------------------*/

#endif // __AMPLE_CONNECTOR_EXPORT_HPP__
